import React, { Component } from 'react'
import { StyleSheet, View, Image, FlatList, TouchableOpacity, Modal, ImageBackground, ActivityIndicator } from 'react-native'
import { Text, Button, SearchBar, Header, HeaderTitle, HeaderButton, TextInputForm } from '../components'
import { ListItem, Icon } from 'react-native-elements'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'
import moment from 'moment'
import { EventRegister } from 'react-native-event-listeners'

import DateTimePicker from 'react-native-modal-datetime-picker'
import * as pickupService from '../services/pickup.service'
import * as orderHelper from '../helpers/order-helper'
import images from '../components/images'

export default class SelectPickupModal extends Component {

    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            pickups: [],
            pickupsFilter: [],
            activity: this.props.navigation.getParam('activity'),
            page: 1,
            pageNext: null,
            refresing: false,
            textSearch: '',
            isSearching: false
        }
    }

    componentDidMount() {
        this.searchPickups("init")
    }

    onChangeText = (text) => {
        const pickups = [...this.state.pickups]
        const textFilter = text.toUpperCase()
        const pickupsFilter = pickups.filter((pickup) => pickup.address !== null && (pickup.address).toUpperCase().search(textFilter) > -1)
        this.setState({ textSearch: text, pickupsFilter: pickupsFilter, isSearching: true })
    }

    onClearText() {

    }

    searchPickups(option) {
        const page = (option == "more") ? this.state.page + 1 : this.state.page
        const params = {
            activity_id: this.state.activity.id,
            page_size: 20,
            page: page
        }

        if (this.state.pageNext !== null || option == "init" && this.state.refresing == false) {
            this.setState({ refresing: true }, () => {
                pickupService.getAll(params).then((response) => {
                    const pageNext = response.data.data.next
                    const pickUps = orderHelper.parsePickUps(response.data.data.results, this.state.activity.pick_up_date)
                    const pickupList = [...this.state.pickups, ...pickUps]

                    this.setState({ pickups: pickupList, pickupsFilter: pickupList, refresing: false, pageNext: pageNext, page: page })
                }).catch((error) => {
                    this.setState({ refresing: false })
                })
            })
        }
    }

    onSelectOption = (pickup) => {
        this.handleClose()
        EventRegister.emit('order:selectPickup', pickup)
    }

    handleClose() {
        this.props.navigation.goBack()
    }

    handleMorePickUp = () => {
        if (this.state.isSearching == false && this.state.refresing == false) {
            this.searchPickups("more")
        }
    }

    handleRefresh() {
        this.setState({ page: 1, refresing: true }, () => {
            this.searchPickups("init")
        })
    }

    renderSearchBar = () => {
        return <SearchBar
            round
            onChangeText={this.onChangeText.bind(this)}
            onClearText={this.onClearText}
            value={this.state.textSearch}
            placeholder='Buscar...'
        />
    }

    renderFooter = () => {
        if (!this.state.refresing) return null

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        )
    }

    render() {
        return (
            <View style={styles.container}>

                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>
                    <Header
                        transparent
                        leftComponent={(
                            <HeaderButton icon="md-arrow-back" color="white" typeicon="ionicon" onPress={this.handleClose.bind(this)} />
                        )}
                        centerComponent={<HeaderTitle>{this.state.activity ? this.state.activity.name : 'Pickups'}</HeaderTitle>}
                        rightComponent={(
                            <View style={{ width: 48 }} />
                        )}
                    />
                </ImageBackground>

                <FlatList
                    data={this.state.pickupsFilter}
                    ListHeaderComponent={this.renderSearchBar}
                    ListFooterComponent={this.renderFooter}
                    renderItem={({ item, index }) => <ListItem
                        title={item.address}
                        subtitle={item.time}
                        onPress={() => this.onSelectOption(item)}
                        titleStyle={{ fontFamily: 'RobotoCondensed-Bold' }}
                        subtitleStyle={{ fontFamily: 'RobotoCondensed-Bold' }}

                    />}
                    keyExtractor={(item, index) => (item.id).toString()}
                    //refresing={this.state.refresing}
                    onEndReached={this.handleMorePickUp}
                    onEndReachedThreshold={0.1}
                //onRefresh={this.handleRefresh.bind(this)}

                />

            </View>

        )
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    styleInput: {
        height: 39,
        color: '#4F4F4F',
        fontSize: 14,
        textAlign: 'center',
        textDecorationLine: "none",
    }
});


