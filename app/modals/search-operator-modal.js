import React, { Component } from 'react'
import { StyleSheet, View, Image, FlatList, TouchableOpacity, Modal } from 'react-native'
import { Text, Button, SearchBar, Header, HeaderTitle, HeaderButton } from '../components'
import { ListItem, Icon } from 'react-native-elements'
import * as operatorService from '../services/operator-service'


const dataPromotors = [
    {
        id: 1,
        name: 'Julian',
        email: 'julian@gmail.com'
    },
    {
        id: 2,
        name: 'Maria Median',
        email: 'mariam@gmail.com'
    },
    {
        id: 3,
        name: 'Eduardo Irrasabal',
        email: 'eduardoi@gmail.com'
    },
    {
        id: 4,
        name: 'Barroto Padilla',
        email: 'barretop@gmail.com'
    },
    {
        id: 5,
        name: 'Andrez Ramoz',
        email: 'andrezramoz@gmail.com'
    },
];
export default class SearchOperatorModal extends Component {

    constructor(props) {
        super(props)
        this.state = {
            textSearch: '',
            operators: []
        }
    }

    searchOperators(params) {
        operatorService.getAll(params).then((response) => {
            this.setState({ operators: response.data.data.results })
        }).catch((error) => {

        })
    }

    componentWillReceiveProps(old) {
        if (this.props.visible !== old.visible && this.props.visible === false) {

        }
    }

    onChangeText = (text) => {
        this.setState({
            textSearch: text
        })

        this.searchOperators({ name: text })
    }

    onClearText = () => {

    }

    onClose = () => {
        if (this.props.onRequestClose) {
            this.props.onRequestClose()
        }
    }


    onPressOperator = (item) => {
        if (this.props.onPressOperator) {
            this.onClose()
            this.props.onPressOperator(item)
        }
    }

    render() {

        return (

            <Modal
                animationType={this.props.animationType}
                transparent={true}
                visible={this.props.visible}
                onRequestClose={this.onClose}
            >
                <View style={styles.container}>

                    {/*<View style={{ alignItems: 'flex-end', padding: 16 }}>
                        <TouchableOpacity onPress={this.onClose}>
                            <Icon name='md-close' type='ionicon' size={24} />
                        </TouchableOpacity>
                    </View>*/}


                    <Header
                        leftComponent={<HeaderTitle>Buscar Operador</HeaderTitle>}
                        //leftComponent={<View style={{ width: 24 }} />}
                        //centerComponent={<HeaderTitle>Buscar Operador</HeaderTitle>}
                        rightComponent={<HeaderButton icon='md-close' typeicon='ionicon' size={24} onPress={this.onClose} />}
                    />
                    <SearchBar
                        round
                        ref={search => this._search = search}
                        onChangeText={this.onChangeText}
                        onClearText={this.onClearText}
                        value={this.state.textSearch}
                        placeholder='Buscar...'
                    />

                    <FlatList
                        data={this.state.operators}
                        renderItem={({ item, index }) => <ListItem
                            title={item.name.toUpperCase()}
                            subtitle={item.email_one}
                            onPress={() => this.onPressOperator(item)}
                            titleStyle={{ fontFamily: 'RobotoCondensed-Bold' }}
                            subtitleStyle={{ fontFamily: 'RobotoCondensed-Bold' }}
                        />}
                        keyExtractor={(item, index) => (item.id).toString()}
                    />

                    <View>
                        {/*<Button square nomargin title={'SALIR'} onPress={this.onClose} />*/}
                    </View>
                </View>
            </Modal>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF'
    },
    styleInput: {
        height: 39,
        color: '#4F4F4F',
        fontSize: 14,
        textAlign: 'center',
        textDecorationLine: "none",
    }
});


