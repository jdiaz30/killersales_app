/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react'
import { View, StatusBar, ImageBackground, FlatList, Image } from 'react-native'

import Config from "../../app.settings"
import images from '../../components/images'

import { Header, HeaderTitle, HeaderButton, Text } from '../../components'
import SVGImage from 'react-native-svg-image'
import * as sessionHelper from '../../helpers/session-helper'



export default class Battle extends Component {

    static navigationOptions = {
        drawerLabel: 'Home',
        header: null //(<HeaderComponent title="Killer Sales" option="partial" />)
    }

    constructor(props) {
        super(props)
        this.state = {
            userSession: null,
            killers: []
        };
    }


    componentDidMount() {
        this.getUserSession();
    }

    getUserSession() {
        sessionHelper.getUser().then((user) => {
            if (user !== null) {
                this.setState({ userSession: user })
            }
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                <StatusBar backgroundColor="#000036" barStyle="light-content" />

                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>

                    <Header
                        transparent menu
                        centerComponent={<HeaderTitle>BATALLA</HeaderTitle>}
                        rightComponent={(<HeaderButton icon="md-share" color="white" typeicon="ionicon" />)}
                    />

                    {this.state.userSession && <View style={{ alignItems: 'center', height: 80, paddingBottom: 8 }}>
                        <View style={{ width: 56, flex: 1 }}  >
                            <SVGImage
                                style={{ width: 56, height: 56, backgroundColor: "transparent" }}
                                source={{ uri: Config.API_MEDIA_URL + this.state.userSession.avatar.url }}
                            />
                        </View>
                        <View><Text bold color='#FFF'>N° 8</Text></View>

                    </View>}

                </ImageBackground>


            </View>
        )
    }
}

