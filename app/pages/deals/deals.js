/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { View, TouchableOpacity, Image, FlatList, ImageBackground, ScrollView, StyleSheet, Dimensions, Alert, StatusBar, Picker } from 'react-native'
import { Icon, Avatar, Card, Button, FormInput, Text, ButtonGroup, FormLabel, CheckBox } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'
import DateTimePicker from 'react-native-modal-datetime-picker'
import { Dropdown } from 'react-native-material-dropdown'
import NumericInput, { calcSize } from 'react-native-numeric-input'
import moment from 'moment'
import AnimatedHideView from 'react-native-animated-hide-view'

import Config from "../../app.settings"
import images from '../../components/images'
import { Header, HeaderButton, HeaderTitle } from '../../components'

import * as sessionHelper from '../../helpers/session-helper'
import * as dateHelper from '../../helpers/date-helper'
import * as orderHelper from '../../helpers/order-helper'

import * as promotorService from '../../services/promotor-service'

import serviceStyle from '../../styles/service.style'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'

export default class Deals extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            userSession: this.props.navigation.getParam('userSession'),
            deals: [],
            isLoading: false
        }
    }

    componentDidMount() {
        this.listDeals()
    }

    listDeals() {
        this.setState({ isLoading: true }, () => {
            promotorService.getAllDeals(this.state.userSession.id, { page_size: 30 }).then((response) => {
                this.setState({ deals: response.data.data.results, isLoading: false })
            }).catch(() => {
                this.setState({ isLoading: false })
            })
        })

    }

    //Destroy component
    componentWillUnmount() {

    }

    handleClose() {
        this.props.navigation.pop()
    }

    getDateOrder(dateOrder) {
        return (dateHelper.dateTranscurred(dateOrder))
    }

    getStatusOrderText(status) {
        return (orderHelper.getStatusText(parseInt(status)))
    }

    getStatusOrderBackgroundText(status) {
        return (orderHelper.getStatusBackground(parseInt(status)))
    }

    handleSelectDeal(deal) {
        this.props.navigation.push("DealsOrderService", { pagePrev: "deals", orderId: deal.id, userSession: this.state.userSession, catalog: { operator: deal.operator } })
    }

    render() {
        return (

            <View style={{ flex: 1, backgroundColor: "#f8f8fc" }}>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />

                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>
                    <Header
                        transparent menu
                        centerComponent={<HeaderTitle>Mis Deals</HeaderTitle>}
                        rightComponent={(<View style={{ width: 48 }} />)}
                    />
                </ImageBackground>


                <View style={{ flex: 1 }}>

                    <FlatList
                        data={this.state.deals}
                        renderItem={({ item, index }) =>
                            <TouchableOpacity onPress={this.handleSelectDeal.bind(this, item)}>
                                <View style={{
                                    borderRadius: 6,
                                    marginBottom: 8,
                                    marginHorizontal: 8,
                                    padding: 13,
                                    borderColor: '#CCC',
                                    borderWidth: 1,
                                    backgroundColor: '#FFF',
                                    shadowOpacity: 0.75,
                                    shadowRadius: 5,
                                    shadowColor: '#081257',
                                    //shadowOffset: { height: 5, width: 5 },
                                    elevation: 4,
                                }}>
                                    <View>
                                        <Text style={{ color: '#121111', fontFamily: "RobotoCondensed-Bold", fontSize: 16 }} ellipsizeMode="tail" numberOfLines={1}>{item.operator.name} </Text>
                                    </View>
                                    <View style={{ alignContent: 'space-between', flex: 1, flexDirection: 'row' }}>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', width: '10%' }}>
                                            <Icon name="md-person" type="ionicon" color="#000040" size={15} />
                                            <Text style={{ color: '#121111', fontFamily: "RobotoCondensed-Bold", fontSize: 15 }}> {item.nro_adults}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', width: '10%' }}>
                                            <Icon name="md-happy" type="ionicon" color="#000040" size={15} />
                                            <Text style={{ color: '#121111', fontFamily: "RobotoCondensed-Bold", fontSize: 15 }}> {item.nro_childs}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', width: '25%' }}>
                                            <Text style={{ color: '#121111', fontFamily: "RobotoCondensed-Bold", fontSize: 14 }}> {(orderHelper.getTotalText(item))}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', width: '25%' }}>
                                            <Text style={{ color: '#121111', fontFamily: "RobotoCondensed-Bold", fontSize: 14 }}> {(orderHelper.getTotalBalanceText(item))}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', justifyContent: 'flex-end', alignItems: 'center', width: '32%' }}>
                                            <View style={{ backgroundColor: this.getStatusOrderBackgroundText(item.status), borderRadius: 5, padding: 8 }}>
                                                <Text style={{ color: 'white', fontFamily: "RobotoCondensed-Bold", fontSize: 14 }}>{this.getStatusOrderText(item.status)}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View>
                                        <Text style={{ color: '#CCC', fontFamily: "RobotoCondensed-Bold", fontSize: 15 }}>Hace {this.getDateOrder(item.date_order)}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        }
                        keyExtractor={(item, index) => (item.id).toString()}
                        showsHorizontalScrollIndicator={false}
                        style={{ paddingTop: 8 }}
                    />
                </View>

                <OrientationLoadingOverlay
                    visible={this.state.isLoading}
                    color="white"
                    indicatorSize="large"
                    messageFontSize={18}
                    message="Cargando ..."
                />

            </View >
        )
    }
}

