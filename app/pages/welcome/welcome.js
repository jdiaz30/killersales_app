import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, StatusBar, AsyncStorage, Image, ImageBackground } from 'react-native'

import Ionicons from 'react-native-vector-icons/Ionicons'
import AppIntroSlider from 'react-native-app-intro-slider'

import * as sessionHelper from '../../helpers/session-helper'
import images from '../../components/images'

const styles = StyleSheet.create({
    buttonCircle: {
        width: 40,
        height: 40,
        backgroundColor: 'rgba(0, 0, 0, .2)',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
    },
    mainContentB: {
        flex: 1,
        alignItems: 'center',
        //justifyContent: 'space-around',
    },
    mainContent: {
        backgroundColor: '#484ea2',
        flex: 1,
        width: '100%',
        height: '100%',
        padding: 20
    },
    mainText: {
        padding: 25
    },
    image: {
        width: 320,
        height: 320,
    },
    text: {
        color: 'white',
        backgroundColor: 'transparent',
        textAlign: 'center',
        paddingHorizontal: 16,
        fontSize: 18,
        fontFamily: "RobotoCondensed-Light"
    },
    title: {
        fontSize: 26,
        color: '#fff',
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginBottom: 16,
        fontFamily: "RobotoCondensed-Bold"
    }
})



const slides = [
    {
        key: 'somethun',
        title: 'Unete',
        text: 'Cuéntanos sobre ti y sube tu documentación',
        image: images.WelcomeI1,
        imageBackground: images.WelcomeBackground1,
        backgroundColor: '#59b2ab',
        imageBackgroundWidth: 149,
        imageBackgroundHeight: 149
    },
    {
        key: 'somethun-dos',
        title: 'Recomienda Reserva Vende',
        text: 'Gana dinero en los horarios que prefieras,tenemos las mejores comisiones para ti',
        image: images.WelcomeI2,
        imageBackground: images.WelcomeBackground2,
        backgroundColor: '#febe29',
        imageBackgroundWidth: 132,
        imageBackgroundHeight: 147
    },
    {
        key: 'somethun1',
        title: 'Ingresos netos Dépositos rápidos',
        text: 'Mira cuanto ganaste en cada venta y genera puntos intecambiables por premios', //'I\'m already out of descriptions\n\nLorem ipsum bla bla bla',
        image: images.WelcomeI3,
        imageBackground: images.WelcomeBackground3,
        backgroundColor: '#22bcb5',
        imageBackgroundWidth: 241,
        imageBackgroundHeight: 153
    }
];

export default class Welcome extends Component {

    static navigationOptions = {
        drawerLabel: 'Welcome',
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            userSession: null
        }
    }

    renderItem = props => (
        <ImageBackground source={images.WelcomeBackground1} style={[styles.mainContentB, {
            paddingTop: 0,
            paddingBottom: 0,
            width: props.width,
            height: props.height,
        }]} resizeMode='stretch'>
            <View style={styles.mainContent}>
                <View style={{ alignItems: 'center' }}>
                    <Image source={images.LogoWShite} style={{ width: 180, height: 172 }} />
                </View>
                <View style={styles.mainText}>
                    <Text style={styles.title}>{props.title}</Text>
                    <Text style={styles.text}>{props.text}</Text>
                </View>
                <View style={{ alignItems: 'center' }}>
                    <Image source={props.image} style={{ width: props.imageBackgroundWidth, height: props.imageBackgroundHeight }} />
                </View>
            </View>

        </ImageBackground>
    )

    componentDidMount() {
        this.getUserSession()
    }

    getUserSession() {
        setTimeout(() => {
            sessionHelper.getUser().then((user) => {
                if (user !== null) {
                    this.setState({
                        userSession: { ...user },
                    })
                }
            })
        }, 300)
    }

    _renderNextButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Ionicons
                    name="md-arrow-round-forward"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                    style={{ backgroundColor: 'transparent' }}
                />
            </View>
        )
    }

    _renderDoneButton = () => {
        return (
            <View style={styles.buttonCircle}>
                <Ionicons
                    name="md-checkmark"
                    color="rgba(255, 255, 255, .9)"
                    size={24}
                    style={{ backgroundColor: 'transparent' }}
                />
            </View>
        )
    }

    welcomeCheck() {
        sessionHelper.saveWelcome()
        this.props.navigation.navigate('Catalog')
    }

    render() {

        return (
            <AppIntroSlider
                slides={slides}
                renderDoneButton={this._renderDoneButton}
                renderNextButton={this._renderNextButton}
                renderItem={this.renderItem}
                onDone={this.welcomeCheck.bind(this)}
            />
        )
    }
}

