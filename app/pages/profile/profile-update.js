
import React, { Component } from 'react'
import { View, TouchableOpacity, Image, Text, FlatList, ImageBackground, ScrollView, StatusBar } from 'react-native'
import { Avatar, Icon, CheckBox } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'

import Config from "../../app.settings"
import images from '../../components/images'

import * as sessionHelper from '../../helpers/session-helper'
import * as promotorService from '../../services/promotor-service'
import { Header, HeaderButton, Card, CardHeader, AvatarCircle, TextInputForm, LabelInputForm, Button } from '../../components'
import { SearchOperatorModal } from '../../modals'

export default class ProfileUpdate extends Component {
    static navigationOptions = {
        headerTitle: "Perfil",
        header: null
    }

    constructor(props) {
        super(props)
        this.userSession = { ...this.props.navigation.getParam('userSession') }
        this.state = {
            showModalSearchOp: false,
            clan: [],
            isLoading: false,
            email: this.userSession.email,
            address: (this.userSession.address == "-----") ? "" : this.userSession.address,
            phone: this.userSession.phone,
            isFreelancer: (this.userSession.is_freelancer == null || this.userSession.is_freelancer == "") ? false : ((this.userSession.is_freelancer == false) ? false : true),
            operator: this.userSession.operator
        }
    }

    handleClose() {
        this.props.navigation.pop()
    }

    handleUpdateProfile() {
        this.props.navigation.pop()
        const data = {
            email: this.state.email,
            address: (this.state.address == "-----") ? "" : this.state.address,
            phone: this.state.phone,
            is_freelancer: this.state.isFreelancer,
            operator_id: (this.state.operator == null) ? "" : this.state.operator.id
        }

        this.setState({ isLoading: true }, () => {
            promotorService.update(this.userSession.id, data).then((response) => {
                this.setState({ isLoading: false })
                EventRegister.emit('updateProfile', response.data.data)
            }).catch((error) => {
                this.setState({ isLoading: false })
            })
        })
    }

    render() {
        return (

            <ScrollView>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />
                <ImageBackground source={images.ProfileBackTwo} style={{ width: '100%', flex: 1, height: '100%' }} resizeMode='cover'>

                    <View style={{ flex: 1, height: '100%' }}>
                        <View style={{ height: 240 }}>
                            <ImageBackground source={images.ProfileBack} style={{ width: '100%', flex: 1, justifyContent: 'center', height: '100%' }} resizeMode='cover'>

                                <View style={{ width: '100%', height: '100%' }}>
                                    <Header transparent
                                        leftComponent={(
                                            <HeaderButton icon="md-arrow-back" color="white" typeicon="ionicon" onPress={this.handleClose.bind(this)} />
                                        )}
                                        centerComponent={(
                                            <Text style={{ color: '#FFF', fontFamily: "RobotoCondensed-Bold", fontSize: 20 }}>
                                                {this.userSession.name.toUpperCase()}
                                            </Text>
                                        )}
                                        rightComponent={(<View style={{ width: 56 }} />)}
                                    />

                                    <View style={{ alignItems: 'center', flex: 1 }}>
                                        {this.userSession && <View style={{ flex: 1 }} >
                                            <View style={{ width: 100, flex: 1 }} >
                                                <SVGImage
                                                    style={{ width: 100, height: 100, backgroundColor: "transparent" }}
                                                    source={{ uri: this.userSession.avatar }}
                                                />
                                            </View>
                                        </View>}
                                        <View style={{ top: -33, position: 'relative', alignItems: 'center' }}>
                                            <Image style={{ height: 30, width: 30, marginTop: 5 }} source={images.CoupTree} height={30} />
                                        </View>
                                    </View>
                                </View>
                            </ImageBackground>
                        </View>


                        <View style={{ position: 'relative', top: -40, flex: 1, paddingLeft: 10, paddingRight: 10 }}>

                            <Card title='Datos Personales' >

                                <View style={{ paddingHorizontal: 16 }}>

                                    <CheckBox
                                        checked={this.state.isFreelancer}
                                        onPress={() => {
                                            const is_freelancer = (this.state.isFreelancer == 1 ? false : true)
                                            this.setState({
                                                isFreelancer: is_freelancer
                                            })
                                        }}
                                        checkedIcon='dot-circle-o'
                                        uncheckedIcon='circle-o'
                                        uncheckedColor='#E5E5EA'
                                        checkedColor='#081257'
                                        center
                                        title='SOY FREELANCER'
                                        textStyle={{ color: '#4B4B4B', fontFamily: 'RobotoCondensed-Regular' }}
                                        containerStyle={{ borderWidth: 0, marginLeft: 0, marginRight: 0, margin: 0, backgroundColor: 'transparent' }}
                                    />

                                    {this.state.isFreelancer == false && <View>
                                        <LabelInputForm>Operador</LabelInputForm>
                                        <TouchableOpacity onPress={() => { this.setState({ showModalSearchOp: true }) }}>
                                            <TextInputForm
                                                editable={false}
                                                value={this.state.operator ? this.state.operator.name : 'Ninguno'}
                                            />
                                        </TouchableOpacity>
                                    </View>}

                                    <SearchOperatorModal
                                        animationType="fade"
                                        transparent={false}
                                        visible={this.state.showModalSearchOp}
                                        onRequestClose={() => { this.setState({ showModalSearchOp: false }) }}
                                        onPressOperator={(operator) => { this.setState({ operator: operator }) }}
                                    />

                                    <View>
                                        <LabelInputForm>Email</LabelInputForm>
                                        <TextInputForm
                                            editable={false}
                                            value={this.state.email}
                                        />
                                    </View>

                                    <View>
                                        <LabelInputForm>Dirección</LabelInputForm>
                                        <TextInputForm
                                            onChangeText={(address) => this.setState({ address })}
                                            value={this.state.address}
                                        />
                                    </View>
                                    <View>
                                        <LabelInputForm>Teléfono</LabelInputForm>
                                        <TextInputForm
                                            onChangeText={(phone) => this.setState({ phone })}
                                            dataDetectorTypes='phoneNumber'
                                            value={this.state.phone}
                                        />
                                    </View>

                                    <Button title="ACTUALIZAR" onPress={this.handleUpdateProfile.bind(this)} />

                                </View>
                            </Card>
                        </View>
                    </View>

                </ImageBackground>

                <OrientationLoadingOverlay
                    visible={this.state.isLoading}
                    color="white"
                    indicatorSize="large"
                    messageFontSize={18}
                    message="Actualizando ..."
                />
            </ScrollView>

        )
    }
}

