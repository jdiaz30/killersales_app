
import React, { Component } from 'react'
import { View, TouchableOpacity, Image, Text, FlatList, ImageBackground, ScrollView, StatusBar } from 'react-native'
import { Avatar, Icon, ListItem, Button } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'

import Config from "../../app.settings"

import images from '../../components/images'

import * as sessionHelper from '../../helpers/session-helper'
import * as promotorService from '../../services/promotor-service'

import { Header, HeaderButton, Card, CardHeader, AvatarCircle, HeaderLeft, HeaderCenter, HeaderRight } from '../../components';


const ItemProfile = props => {
    return (
        <ListItem
            leftIcon={(<View style={{ width: 24, alignItems: 'flex-start' }} ><Icon name={props.icon} type="font-awesome" size={15} color="#06003a" /></View>)}
            subtitle={(<Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#b6b6b6", paddingLeft: 8 }}>{props.value}</Text>)}
            rightIcon={(<View />)}
            containerStyle={[{ paddingTop: 8, paddingBottom: 8, borderBottomWidth: 0 }]}
        />
    )
}

const ItemClan = props => {
    return (
        <View style={{ justifyContent: 'center', alignItems: 'center', padding: 4, flex: 1 }}>
            <AvatarCircle
                source={{ uri: Config.API_MEDIA_URL + props.avatar }} type='svg' size={44}
            />
            <Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#3d3f4b", fontSize: 12 }} ellipsizeMode="tail" numberOfLines={1}>{props.name}</Text>
        </View>
    )
}

const ChartProfile = props => {
    return (
        <View style={{ flexDirection: 'row', padding: 8 }} >
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: "RobotoCondensed-Bold", fontSize: 24, color: "#081257" }}>{props.deals}</Text>
                <Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#3d3f4b", fontSize: 14 }}>Deals</Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: "RobotoCondensed-Bold", fontSize: 24, color: "#081257" }}>{props.top}</Text>
                <Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#3d3f4b", fontSize: 14 }}>Top</Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text style={{ fontFamily: "RobotoCondensed-Bold", fontSize: 24, color: "#081257" }}>{props.kcoins}</Text>
                <Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#3d3f4b", fontSize: 14 }}>KCoins</Text>
            </View>
        </View>
    )
}

export default class Profile extends Component {
    static navigationOptions = {
        headerTitle: "Perfil",
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            userSession: this.props.navigation.getParam('userSession'),
            clan: [],
            userStats: { top: 0, deals: 0, kcoins: 0 }
        }
    }

    componentDidMount() {
        this.listenEvents()
        this.getUserSession()
        this.listClan()
    }

    onMenu() {
        NavigationService.drawer()
    }



    listClan() {
        if (this.state.userSession.clan !== null) {
            const promotorData = {
                clan_id: this.state.userSession.clan.id,
                page_size: 50,
                exclude_id: this.state.userSession.id
            }
            promotorService.search(promotorData)
                .then((response) => {
                    this.setState({ clan: response.data.results })
                })
        }
    }

    listenEvents() {
        this.listenerUpdateAvatar = EventRegister.addEventListener('updateAvatar', (data) => {
            if (data !== undefined && data !== null) {
                this.state.userSession.avatar = Config.API_MEDIA_URL + data.avatar.url
                this.setState({ userSession: this.state.userSession })
            }
        })

        this.listenerUpdateProfile = EventRegister.addEventListener('updateProfile', (data) => {
            const dataUpdate = { ...data }
            dataUpdate.avatar = Config.API_MEDIA_URL + data.avatar.url
            this.setState({ userSession: dataUpdate })
        })
    }

    getUserSession() {
        const userData = { ...this.state.userSession }
        userData.avatar = (userData.avatar !== null) ? Config.API_MEDIA_URL + userData.avatar.url : ""
        userData.phone = (userData.phone !== null && userData.phone !== "") ? userData.phone : "00-0000"
        userData.address = (userData.address !== null && userData.address !== "") ? userData.address : "-----"
        this.setState({ userSession: userData }, () => {
            this.getStats(userData)
        })
    }

    getStats(user) {
        promotorService.stats(user.id)
            .then((response) => {
                this.setState({ userStats: response.data.data })
            })
            .catch((error) => {

            })
    }

    handleClose() {
        this.props.navigation.pop()
    }

    selectedAvatar() {
        this.props.navigation.push("ProfileAvatar")
    }

    render() {
        const { navigation } = this.props

        return (

            <View style={{ flex: 1 }}>

                <ScrollView>
                    <ImageBackground source={images.ProfileBackTwo} style={{ width: '100%', flex: 1, height: '100%' }} resizeMode='cover'>

                        <View style={{ flex: 1, height: '100%' }}>
                            <View style={{ height: 240 }}>
                                <ImageBackground source={images.ProfileBack} style={{ width: '100%', flex: 1, justifyContent: 'center', height: '100%' }} resizeMode='cover'>

                                    <View style={{ width: '100%', height: '100%' }}>

                                        <Header transparent menu
                                            //leftComponent={(<HeaderButton icon="md-menu" color="white" typeicon="ionicon" onPress={this.onMenu} />)}
                                            centerComponent={(
                                                <Text style={{ color: '#FFF', fontFamily: "RobotoCondensed-Bold", fontSize: 20 }}>
                                                    {this.state.userSession.name.toUpperCase()}
                                                </Text>
                                            )}
                                            rightComponent={(<View style={{ width: 56 }} />)}
                                        />

                                        {/**<View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', marginTop: 10, marginLeft: 10 }}>
                                            <TouchableOpacity style={{ width: 28 }} onPress={this.handleClose.bind(this)}  >
                                                <Icon name="md-arrow-back" color="white" type="ionicon" />
                                            </TouchableOpacity>
                                        </View>*/}

                                        <View style={{ alignItems: 'center', flex: 1 }}>

                                            <View style={{ flex: 1 }} >
                                                <TouchableOpacity style={{ width: 100, flex: 1 }} onPress={this.selectedAvatar.bind(this)} >
                                                    <SVGImage
                                                        style={{ width: 100, height: 100, backgroundColor: "transparent" }}
                                                        source={{ uri: this.state.userSession.avatar }}
                                                    />
                                                </TouchableOpacity>
                                            </View>

                                            <View style={{ top: -33, position: 'relative' }} >
                                                <Icon name="camera" type="font-awesome" raised size={10} />
                                            </View>

                                            <View style={{ top: -33, position: 'relative', alignItems: 'center' }}>
                                                <Image style={{ height: 30, width: 30, marginTop: 5 }} source={images.CoupTree} height={30} />
                                                {/**<Text style={{ color: "white", fontFamily: "RobotoCondensed-Bold", fontSize: 18, zIndex: 10 }}>{this.state.userSession.name}</Text>*/}
                                            </View>

                                        </View>
                                    </View>
                                </ImageBackground>
                            </View>


                            <View style={{ position: 'relative', top: -40, flex: 1, paddingLeft: 10, paddingRight: 10 }}>

                                <Card containerStyle={{ borderRadius: 8 }} >
                                    {/**<Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#3d3f4b", fontSize: 17, textAlign: 'center' }}>{this.state.userSession.name}</Text>*/}
                                    <ChartProfile
                                        deals={this.state.userStats.deals}
                                        top={this.state.userStats.top}
                                        kcoins={this.state.userStats.kcoins}
                                    />
                                </Card>

                                <Card title='Mi Clan'>
                                    <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, marginBottom: 16 }}>
                                        <FlatList
                                            data={this.state.clan}
                                            renderItem={({ item, index }) => <ItemClan
                                                avatar={item.avatar.url}
                                                name={item.name}
                                            />}
                                            keyExtractor={(item, index) => (item.id).toString()}
                                            showsHorizontalScrollIndicator={false}
                                            horizontal={true}
                                        />
                                    </View>
                                </Card>


                                <Card>
                                    <CardHeader
                                        leftComponent={<View style={{ width: 47 }} />}
                                        centerComponent={<Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#06003a", fontSize: 17 }}>Datos Personales</Text>}
                                        rightComponent={(
                                            <HeaderButton icon='pencil' typeicon="font-awesome" size={18} color="#06003a" onPress={() => { navigation.push('ProfileUpdate', { userSession: this.state.userSession }) }} />
                                        )}
                                    />

                                    <ItemProfile icon='envelope' value={this.state.userSession.email} />
                                    <ItemProfile icon='map-marker' value={this.state.userSession.address} />
                                    <ItemProfile icon='phone' value={this.state.userSession.phone} />
                                </Card>

                            </View>


                        </View>

                    </ImageBackground>
                </ScrollView>
            </View>

        )
    }
}

