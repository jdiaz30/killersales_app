/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, StatusBar, FlatList, TouchableOpacity, Alert, ImageBackground } from 'react-native'
import { Button, FormLabel, FormInput, FormValidationMessage, Header, Avatar, Icon, ButtonGroup } from 'react-native-elements'
import { EventRegister } from 'react-native-event-listeners'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'

import images from '../../components/images'
import SVGImage from 'react-native-svg-image'

import Config from "../../app.settings"

import * as avatarService from '../../services/avatar-service'
import * as promotorService from '../../services/promotor-service'
import * as sessionHelper from '../../helpers/session-helper'
import * as globalHelper from '../../helpers/global-helper'

export default class ProfileAvatar extends Component {

    static navigationOptions = {
        title: "Elegir Avatar",
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            avatars: [],
            avatarsFilter: [],
            userSession: {},
            buttonIndex: 0,
            isLoading: false
        }
    }

    componentDidMount() {
        avatarService.getAll({ page_size: 30 }).then(
            (response) => {
                this.setState({ avatars: response.data.data.results }, () => {
                    this.setAvatarsFilter()
                })
            }
        )

        this.getUserSession()
    }

    setAvatarsFilter() {
        const avatars = [... this.state.avatars]
        const isFree = (this.state.buttonIndex == 0) ? true : false
        const avatarsFilter = avatars.filter((avatar) => (avatar.is_free == isFree))

        this.setState({ avatarsFilter: avatarsFilter })
    }

    updateButtonIndex(buttonIndex) {
        this.setState({ buttonIndex: buttonIndex }, () => {
            this.setAvatarsFilter()
        })
    }

    getUserSession() {
        sessionHelper.getUser().then((user) => {
            this.setState({ userSession: user })
        })
    }

    selectAvatar(avatar) {
        Alert.alert(
            'Confirmación',
            '¿Deseas usar este avatar?',
            [
                { text: 'No', onPress: () => console.log('Ask me later pressed') },
                { text: 'Si', onPress: () => this.saveSelectAvatar(avatar) },
            ],
            { cancelable: false }
        )
    }

    saveSelectAvatar(avatar) {
        const data = {
            avatar_id: avatar.id
        }

        this.setState({ isLoading: true }, () => {
            promotorService.update(this.state.userSession.id, data)
                .then(
                    (response) => {
                        globalHelper.showMessageAlert("Exito", "Avatar actualizado", null)
                        this.setState({ isLoading: false })
                        EventRegister.emit('updateAvatar', response.data.data)
                    })
                .catch((error) => {
                    globalHelper.showMessageAlert("Error", "Avatar no actualizado", null)
                    this.setState({ isLoading: false })
                })
        })
    }

    handleClose() {
        this.props.navigation.pop()
    }

    render() {
        const buttons = ["FREE", "PREMIUM"]
        const { buttonIndex } = this.state
        return (
            <View style={{ flex: 1 }}>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />

                <View style={{ height: 50 }}>
                    <ImageBackground source={images.ProfileBack} style={{
                        width: '100%',
                        flex: 1,
                        height: '100%',

                    }} resizeMode='cover'>


                        <View style={{ flexDirection: "row" }}>
                            <View style={{ justifyContent: 'flex-start', alignItems: 'center', marginTop: 10, marginLeft: 10 }}>
                                <TouchableOpacity style={{ width: 28 }} onPress={this.handleClose.bind(this)}  >
                                    <Icon name="md-arrow-back" type="ionicon" color="white" />
                                </TouchableOpacity>

                            </View>
                            <View style={{ alignItems: "center", marginTop: 10, marginLeft: 20 }}>
                                <Text style={{ color: 'white', fontFamily: "RobotoCondensed-Bold", fontSize: 20 }}>Elige tu avatar</Text>
                            </View>
                        </View>

                    </ImageBackground>
                </View>

                <View style={{ flex: 1 }}>
                    <View >
                        <ButtonGroup
                            onPress={this.updateButtonIndex.bind(this)}
                            selectedIndex={buttonIndex}
                            buttons={buttons}
                            containerStyle={{ height: 40 }}
                        />
                    </View>
                    <View style={{ padding: 10, flex: 1 }}>

                        <FlatList
                            data={this.state.avatarsFilter}
                            renderItem={({ item, index }) =>
                                <TouchableOpacity onPress={this.selectAvatar.bind(this, item)}>
                                    <View style={{ padding: 15, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: 'white', borderRadius: 4, marginBottom: 8 }}>
                                        <View style={{ alignItems: 'flex-start', flexDirection: 'row' }}>
                                            <View>

                                                <TouchableOpacity style={{ width: 100, flex: 1 }} >
                                                    <SVGImage
                                                        style={{ width: 100, height: 100, backgroundColor: "transparent" }}
                                                        source={{ uri: Config.API_MEDIA_URL + item.url }}
                                                    />
                                                </TouchableOpacity>

                                            </View>
                                            <View style={{ marginTop: 8, marginLeft: 5 }}>
                                                <Text style={{ color: '#120416', fontFamily: "RobotoCondensed-Bold", fontSize: 20 }}>{item.name}</Text>
                                                {item.is_free ? (
                                                    <Text style={{ color: '#fdec5b', fontFamily: "RobotoCondensed-Bold", fontSize: 15 }}>FREE</Text>
                                                ) : (
                                                        <Text style={{ color: '#fdec5b', fontFamily: "RobotoCondensed-Bold", fontSize: 15 }}>{item.kcoins} KCOINS</Text>
                                                    )}


                                                {item.status == 1 ? (
                                                    <Text style={{ color: '#120416', fontFamily: "RobotoCondensed-Bold", fontSize: 11 }}>DISPONIBLE</Text>
                                                ) : (
                                                        <Text style={{ color: '#120416', fontFamily: "RobotoCondensed-Bold", fontSize: 11 }}> NO DISPONIBLE </Text>
                                                    )}

                                            </View>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            }
                            keyExtractor={(item, index) => (item.id).toString()}
                            showsHorizontalScrollIndicator={false}
                            showsVerticalScrollIndicator={true}
                        />
                    </View>
                </View>


                <OrientationLoadingOverlay
                    visible={this.state.isLoading}
                    color="white"
                    indicatorSize="large"
                    messageFontSize={18}
                    message="Actualizando ..."
                />
            </View>
        );
    }
}

