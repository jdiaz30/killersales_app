import React, { Component } from 'react'
import { StyleSheet, View, Image, FlatList, TouchableOpacity, Modal, StatusBar } from 'react-native'
import { ListItem, Icon } from 'react-native-elements'

import { Text, Button, SearchBar, Header, HeaderTitle, HeaderButton } from '../../components'
import * as operatorService from '../../services/operator-service'
import * as appService from '../../services/app-service'

export default class SearchCountryModal extends Component {

    constructor(props) {
        super(props)
        this.state = {
            textSearch: '',
            countries: []
        }
    }

    searchCountries(params) {

        appService.getAllCountries(params).then((response) => {
            console.log("joper30 getAllCountries", response)
            this.setState({ countries: response.data.data.results })
        }).catch((error) => {
            console.log("joper30 getAllCountries err", error)
        })
    }

    componentWillReceiveProps(old) {
        if (this.props.visible !== old.visible && this.props.visible === false) {

        }
    }

    onChangeText = (text) => {
        this.setState({
            textSearch: text
        })

        this.searchCountries({ name: text, status: 1 })
    }

    onClearText = () => {

    }

    onClose = () => {
        if (this.props.onRequestClose) {
            this.props.onRequestClose()
        }
    }


    onPressCountry = (item) => {
        if (this.props.onPressCountry) {
            this.onClose()
            this.props.onPressCountry(item)
        }
    }

    render() {

        return (

            <Modal
                animationType={this.props.animationType}
                transparent={true}
                visible={this.props.visible}
                onRequestClose={this.onClose}
            >
                <View style={styles.container}>
                    <View style={{ borderRadius: 15 }}>
                        <Header
                            leftComponent={<HeaderTitle>Buscar Paises</HeaderTitle>}
                            rightComponent={<HeaderButton icon='md-close' typeicon='ionicon' size={24} onPress={this.onClose} />}
                        />
                        <SearchBar
                            round
                            /* ref={search => this._search = search} */
                            onChangeText={this.onChangeText}
                            onClearText={this.onClearText}
                            value={this.state.textSearch}
                            placeholder='Buscar...'
                        />
                        <View style={styles.containterModal}>
                            <FlatList
                                data={this.state.countries}
                                renderItem={({ item, index }) => <ListItem
                                    title={item.name.toUpperCase()}
                                    subtitle={item.email_one}
                                    onPress={() => this.onPressCountry(item)}
                                    titleStyle={{ fontFamily: 'RobotoCondensed-Bold' }}
                                    subtitleStyle={{ fontFamily: 'RobotoCondensed-Bold' }}
                                />}
                                keyExtractor={(item, index) => (item.id).toString()}

                            />
                        </View>

                    </View>


                </View>
            </Modal >
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "rgba(0,0,0,0.6)",
        padding: 10
    },
    styleInput: {
        height: 39,
        color: '#4F4F4F',
        fontSize: 14,
        textAlign: 'center',
        textDecorationLine: "none",
    },
    containterModal: {
        backgroundColor: 'white',
        height: 300,
    }
})