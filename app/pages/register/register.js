/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, ScrollView, StatusBar, ImageBackground, Image, Picker, Alert, TouchableOpacity, TextInput } from 'react-native'
import { Button, FormLabel, FormInput, FormValidationMessage, Card, Header, CheckBox } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome'
import { Dropdown } from 'react-native-material-dropdown'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'

import images from '../../components/images'
import registerStyle from '../../styles/register.style'

import * as registerService from '../../services/promotor-service'
import * as authService from '../../services/auth-service'
import * as operatorService from '../../services/operator-service'
import * as appService from '../../services/app-service'

import * as notificationHelper from '../../helpers/notification-helper'
import * as sessionHelper from '../../helpers/session-helper'
import * as globalHelper from '../../helpers/global-helper'
import * as httpService from '../../services/http-service'

import SearchCountryModal from './search-country-modal'
import { TextInputForm, LabelInputForm } from '../../components'
import SearchOperatorModal from './search-operator-modal'

export default class Register extends Component {
    static navigationOptions = {
        title: 'Crear cuenta',
        header: null
    }

    constructor(props) {
        super(props)

        this.state = {
            name: "",
            lastName: "",
            phone: "",
            email: "",
            password: "",
            rePassword: "",
            country: null,
            operator: null,
            isLoading: false,
            isFreelancer: false,
            errorName: false,
            errorLastName: false,
            errorPhone: false,
            errorEmail: false,
            errorPassword: false,
            errorRePassword: false,
            errorCountry: false,
            errorOperator: false,

            showModalSearchCountry: false,
            showModalSearchOperator: false,
            showActivateCode: false,
            activateCode: null
        }
    }

    componentDidMount() {
        this.getActivateCode()
    }

    getActivateCode() {
        sessionHelper.getDataList("validateCode").then((data) => {
            console.log("joper30 getActivateCode", data)
            if (data !== null) {
                let showActivateCode = (data.validate) ? false : true
                this.setState({ showActivateCode: showActivateCode, activateCode: data })
            }
        }).catch((error) => {

        })
    }

    handleRegister() {
        let errorName = (this.state.name == "") ? true : false
        let errorLastName = (this.state.lastName == "") ? true : false
        let errorEmail = (this.state.email == "" || globalHelper.validateEmail(this.state.email) == false) ? true : false
        let errorPhone = (this.state.phone == "") ? true : false
        let errorPassword = (this.state.password == "") ? true : false
        let errorRePassword = (this.state.password !== this.state.rePassword || this.state.rePassword == "") ? true : false
        let errorCountry = (this.state.country == null) ? true : false

        this.setState({
            errorName: errorName,
            errorLastName: errorLastName,
            errorEmail: errorEmail,
            errorPhone: errorPhone,
            errorPassword: errorPassword,
            errorRePassword: errorRePassword,
            errorCountry: errorCountry
        }, () => {
            if (errorName || errorLastName || errorEmail || errorPhone || errorPassword || errorCountry) {
                globalHelper.showMessageAlert('Error!', 'Datos incorrectos', null)
            } else {
                this.register()
            }
        })
    }

    validateInput(name) {
        switch (name) {
            case "name":
                let errorName = (this.state.name == "") ? true : false
                this.setState({ errorName: errorName })
                break
            case "lastName":
                let errorLastName = (this.state.lastName == "") ? true : false
                this.setState({ errorLastName: errorLastName })
                break
            case "email":
                let errorEmail = (this.state.email == "" || globalHelper.validateEmail(this.state.email) == false) ? true : false
                this.setState({ errorEmail: errorEmail })
                break
            case "phone":
                let errorPhone = (this.state.phone == "") ? true : false
                this.setState({ errorPhone: errorPhone })
                break
            case "password":
                let errorPassword = (this.state.password == "") ? true : false
                this.setState({ errorPassword: errorPassword })
                break
            case "rePassword":
                let errorRePassword = (this.state.password !== this.state.rePassword) ? true : false
                this.setState({ errorRePassword: errorRePassword })
                break
        }
    }

    showActivateCodePage() {
        let { activateCode } = this.state
        this.props.navigation.navigate('ConfirmAcount', { phone: activateCode.phone, numberArea: activateCode.numberArea, userTemp: activateCode.user })
    }

    register() {
        const data = {
            promotor: {
                name: this.state.name,
                last_name: this.state.lastName,
                phone: this.state.phone,
                email: this.state.email,
                status: 0,
                avatar_id: 23,
                is_freelancer: this.state.isFreelancer,
                type_promotor: "promotor",
                country_id: this.state.country.id,
                operator_id: this.state.operator.id
            },
            user: {
                user_name: this.state.email,
                password: this.state.password,
                type_user: "promotor",
                status: 0,
                related_id: 0
            },
            country: this.state.country
        }

        if (this.state.operator == null) {
            delete data.promotor.operator_id
        }

        this.setState({ isLoading: true }, () => {
            appService.registerPromotor(data)
                .then((response) => {
                    this.setState({ isLoading: false })
                    const user = response.data.data
                    notificationHelper.init(user)

                    globalHelper.showMessageAlert('Éxito!', 'Su perfil se ha creado,necesitamos validar la cuenta', () => {
                        sessionHelper.saveTempValidateCodeUser({ user: user, validate: false, phone: this.state.phone, numberArea: this.state.country.number_area })
                        this.props.navigation.navigate('ConfirmAcount', { phone: this.state.phone, numberArea: this.state.country.number_area, userTemp: user })
                    })
                })
                .catch((error) => {
                    this.setState({ isLoading: false })
                    httpService.showError(error)
                    console.warn("joper30 register error", error)
                })
        })
    }

    /* sendCodePhone(data) {
        const promotor = data.promotor
        const user = data.userapp
        const self = this
        const params = {
            userapp: user,
            promotor: promotor
        }

        appService.sendValidatePhoneAccount(params).then((response) => {
            this.setState({ isLoading: false })
            globalHelper.showMessageAlert('Éxito!', 'Su perfil se ha creado,necesitamos validar la cuenta', () => {
                self.props.navigation.navigate('ConfirmAcount', { phone: promotor.phone, numberArea: promotor.country.number_area, userTemp: user })
            })

        }).catch((error) => {
            console.warn("sendCodePhone", error)
            this.setState({ isLoading: false })
        })
    } */g

    render() {
        return (

            <ImageBackground source={images.Menu} style={{ flex: 1 }} resizeMode='cover'>
                <View style={registerStyle.boxRegister}>
                    <StatusBar
                        backgroundColor="#000036"
                        barStyle="light-content"
                    />

                    <ScrollView>
                        <View style={{ marginTop: 25, marginBottom: 0, paddingHorizontal: 8 }}>
                            <View style={{ paddingLeft: 8, paddingRight: 8, marginBottom: 15 }}>
                                <CheckBox
                                    title="Soy Freelancer"
                                    uncheckedColor="#333"
                                    checked={this.state.isFreelancer}
                                    textStyle={{ color: "#333", fontSize: 13 }}
                                    containerStyle={{ backgroundColor: 'transparent', borderRadius: 0, borderWidth: 0, marginTop: 10, padding: 0, margin: 0 }}
                                    onPress={() => { this.setState({ isFreelancer: !this.state.isFreelancer }) }}
                                />
                            </View>

                            <TextInputForm style={(this.state.errorName == true) ? registerStyle.input_error : {}} placeholder="Nombre" value={this.state.name} onChangeText={(name) => this.setState({ name })} onEndEditing={this.validateInput.bind(this, "name")} />

                            <TextInputForm style={(this.state.errorLastName == true) ? registerStyle.input_error : {}} placeholder="Apellidos"
                                value={this.state.lastName} onChangeText={(lastName) => this.setState({ lastName })} onEndEditing={this.validateInput.bind(this, "lastName")} />

                            <TouchableOpacity onPress={() => { this.setState({ showModalSearchCountry: true }) }}>
                                <View style={registerStyle.inputButton}>
                                    <Text> {(this.state.country == null) ? "País" : this.state.country.name} </Text>
                                </View>
                            </TouchableOpacity>

                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                <View style={{ flex: 1 }}>
                                    <TextInputForm value={(this.state.country !== null) ? this.state.country.number_area : "+"} editable={false} placeholder="+" />
                                </View>
                                <View style={{ flex: 4 }}>
                                    <TextInputForm style={[(this.state.errorPhone == true) ? registerStyle.input_error : {}]}
                                        value={this.state.phone} onChangeText={(phone) => this.setState({ phone })} keyboardType="phone-pad" onEndEditing={this.validateInput.bind(this, "phone")} placeholder="Teléfono" />
                                </View>
                            </View>

                            <TouchableOpacity onPress={() => { this.setState({ showModalSearchOperator: true }) }}>
                                <View style={registerStyle.inputButton}>
                                    <Text> {(this.state.operator == null) ? "Operador" : this.state.operator.name} </Text>
                                </View>
                            </TouchableOpacity>

                            <TextInputForm style={(this.state.errorEmail == true) ? registerStyle.input_error : {}} placeholder="Correo"
                                value={this.state.email} onChangeText={(email) => this.setState({ email })} keyboardType="email-address" onEndEditing={this.validateInput.bind(this, "email")} />

                            <TextInputForm secureTextEntry={true} style={(this.state.errorPassword == true) ? registerStyle.input_error : {}} placeholder="Contraseña"
                                value={this.state.password} onChangeText={(password) => this.setState({ password })} onEndEditing={this.validateInput.bind(this, "password")} textContentType='password' secureTextEntry={true} />

                            <TextInputForm secureTextEntry={true} style={(this.state.errorRePassword == true) ? registerStyle.input_error : {}} placeholder="Confirmar contraseña"
                                value={this.state.rePassword} onChangeText={(rePassword) => this.setState({ rePassword })} onEndEditing={this.validateInput.bind(this, "rePassword")} textContentType='password' secureTextEntry={true} />

                        </View>

                        <View style={{ marginBottom: 16, paddingHorizontal: 8 }}>

                            {this.state.showActivateCode && <Button title='Activar cuenta' onPress={() => this.showActivateCodePage()} transparent={true} color="#fff" fontFamily="RobotoCondensed-Bold" fontSize={19} style={{ marginBottom: 16 }} />}
                            <Button title='REGISTRARSE' rounded={true} backgroundColor='#fdec5b' color="#2b2e34" fontFamily="RobotoCondensed-Bold" loading={this.state.isLoading} onPress={this.handleRegister.bind(this)} style={{ marginBottom: 16 }} containerViewStyle={{ marginLeft: 0, marginRight: 0 }} buttonStyle={{ borderRadius: 4 }} />
                            <Button title='Salir' onPress={() => this.props.navigation.goBack()} transparent={true} color="#fff" fontFamily="RobotoCondensed-Bold" fontSize={19} style={{ marginBottom: 16 }} />
                        </View>


                        {/*  <OrientationLoadingOverlay
                        visible={this.state.isLoading}
                        color="white"
                        indicatorSize="large"
                        messageFontSize={18}
                        message="Registrando ..."
                    /> */}

                        <SearchCountryModal
                            animationType="fade"
                            transparent={false}
                            visible={this.state.showModalSearchCountry}
                            onRequestClose={() => { this.setState({ showModalSearchCountry: false }) }}
                            onPressCountry={(country) => {
                                this.setState({ country: country, errorCountry: false })
                            }}
                        />

                        <SearchOperatorModal
                            animationType="fade"
                            transparent={false}
                            visible={this.state.showModalSearchOperator}
                            onRequestClose={() => { this.setState({ showModalSearchOperator: false }) }}
                            onPressOperator={(operator) => {
                                this.setState({ operator: operator, errorOperator: false })
                            }}
                        />
                    </ScrollView>
                </View>

            </ImageBackground>


        )
    }
}
