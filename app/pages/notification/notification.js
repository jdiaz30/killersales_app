/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { View, TouchableOpacity, Image, Text, FlatList, ImageBackground, ScrollView, StyleSheet, Dimensions, Alert, TextInput, StatusBar } from 'react-native'
import { Icon, Avatar, Card, Button, FormInput, SearchBar } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'

import Config from "../../app.settings"
import images from '../../components/images'
import { Header, HeaderTitle } from '../../components'
import * as sessionHelper from '../../helpers/session-helper'
import * as dateHelper from '../../helpers/date-helper'
import * as socketHelper from '../../helpers/socket-helper'

import * as notifyService from '../../services/notification.service'

export default class Notifications extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            userSession: this.props.navigation.getParam('userSession'),
            notifications: [],
            isLoading: false,
            isRefresh: false
        }
    }

    componentDidMount() {
        this.listNotifications()
        this.socket = socketHelper.getSocket()

        this.socket.on('killer_sales:new-notification', (data) => {
            this.listNotifications()
        })
    }

    listNotifications() {
        const params = {
            to_promotor_id: this.state.userSession.id,
            page_size: 50
        }
        this.setState({ isLoading: true }, () => {
            notifyService.getAll(params)
                .then((response) => {
                    let notifyList = response.data.data.results
                    notifyList.map((notify) => {
                        notify.time_text = dateHelper.dateTranscurred(notify.date_add)
                    })
                    this.setState({ notifications: notifyList, isLoading: false, isRefresh: false })
                })
                .catch((error) => {
                    this.setState({ isLoading: false })
                })
        })
    }

    handleActionNotify(notify, action) {
        notify.status = (action == "acept") ? 1 : 0
        this.updateNotify(notify.id, notify)
    }

    handleActionNotifyDefault(notify) {
        if (notify.status == 0) {
            notify.status = 1
            let notifications = [... this.state.notifications]
            notifications.filter((item) => item.id == notify.id).map((item) => item = notify)

            this.setState({ notifications: notifications })
            this.updateNotify(notify.id, notify)
        }
        EventRegister.emit('seeNotify', notify.id)
        switch (notify.type_notify) {
            case 'cobrate-success':
                this.props.navigation.navigate("ServiceDetail", { orderId: notify.action_id })
                break
            case 'create-order-cobrate':
                this.props.navigation.navigate("ServiceDetail", { orderId: notify.action_id })
                break
            case 'add-kcoins':
                this.props.navigation.navigate("ServiceDetail", { orderId: notify.action_id })
                break
            default:
                break
        }
    }

    updateNotify(notifyId, data) {
        notifyService.update(notifyId, data)
            .then((response) => {

            })
            .catch((error) => {

            })
    }

    handleRefreshNotify = () => {
        this.setState({ isRefresh: true }, () => {
            this.listNotifications()
        })
    }

    render() {
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />
                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>
                    <Header
                        transparent menu
                        centerComponent={<HeaderTitle>Notificaciones</HeaderTitle>}
                        rightComponent={(<View style={{ width: 48 }} />)}
                    />
                </ImageBackground>

                <View style={{ flex: 1 }}>
                    {(this.state.isLoading == false && this.state.notifications.length == 0) &&
                        <View style={{ padding: 25, alignItems: 'center' }}>
                            <Text style={{ color: '#000036', fontFamily: "RobotoCondensed-Bold", fontSize: 16 }}>No se encontraron notificaciones</Text>
                        </View>
                    }


                    <FlatList
                        data={this.state.notifications}
                        renderItem={({ item, index }) =>

                            <TouchableOpacity onPress={this.handleActionNotifyDefault.bind(this, item)}>
                                <View style={{ marginBottom: 8, borderBottomColor: "#f7f7f7", borderBottomWidth: 1 }}>

                                    <View style={{ flexDirection: "row" }}>
                                        <View style={{ padding: 15, flex: 1 }}>
                                            <Text style={{ color: '#000036', fontFamily: "RobotoCondensed-Bold", fontSize: 16 }}>{item.message}</Text>
                                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                                <Text style={{ color: '#000036', fontFamily: "RobotoCondensed-Light", fontSize: 13 }}>Hace {item.time_text}</Text>
                                                {item.status == 1 && <Icon rounded name='md-checkmark' type='ionicon' size={8} containerStyle={{ marginLeft: 5 }} />}
                                            </View>

                                        </View>

                                        {item.type_notify == "fdfdf" &&
                                            <View style={{ padding: 15, flex: 1, flexDirection: 'row' }}>
                                                <Icon rounded name='md-checkmark' type='ionicon' raised onPress={this.handleActionNotify.bind(this, item, "acept")} />
                                                <Icon rounded name='md-close-circle' type='ionicon' raised onPress={this.handleActionNotify.bind(this, item, "cancel")} />
                                            </View>}
                                    </View>

                                </View>
                            </TouchableOpacity>

                        }
                        keyExtractor={(item, index) => (item.id).toString()}
                        showsHorizontalScrollIndicator={false}
                        onRefresh={() => this.handleRefreshNotify()}
                        refreshing={this.state.isRefresh}
                    />
                </View>
                <OrientationLoadingOverlay
                    visible={this.state.isLoading}
                    color="white"
                    indicatorSize="large"
                    messageFontSize={18}
                    message="Cargando ..."
                />
            </View>

        )
    }
}

