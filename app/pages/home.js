
import React, { Component } from 'react'
import { View, StatusBar, ImageBackground, FlatList, Image, PermissionsAndroid, Platform } from 'react-native'

import { ListItem } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'

import Config from "../app.settings"
import images from '../components/images'

import * as notificationHelper from '../helpers/notification-helper'

import * as promotorService from '../services/promotor-service'
import { Header, HeaderTitle, HeaderButton, HeaderLeft, HeaderRight, Text } from '../components'

import * as sessionHelper from '../helpers/session-helper'
import * as socketHelper from '../helpers/socket-helper'

const ItemKiller = props => {

    const backgroundColor = props.order == 1 ? '#ECECEC' : '#FFFFFF'
    const colorText = props.order == 1 ? '#FFF' : '#081257'

    //const containerStyleFirst = { height: 64, backgroundColor: '#081257', paddingTop: 8, paddingBottom: 8, marginBottom: 4 }

    const containerStyleFirst = { height: 60, backgroundColor: '#081257', borderRadius: 6, paddingTop: 8, paddingBottom: 8, marginBottom: 4, marginHorizontal: 4 }
    const containerStyleLast = { height: 60, backgroundColor: '#FFFFFF', borderRadius: 6, paddingTop: 8, paddingBottom: 8, marginBottom: 4, marginHorizontal: 4 }
    const containerStyle = props.order == 1 ? containerStyleFirst : containerStyleLast

    return (
        <ListItem
            leftIcon={(
                <HeaderLeft>
                    <Text color={colorText} >{props.order}</Text>
                    <View style={{ width: 32, height: 32, marginHorizontal: 4 }}>
                        <SVGImage
                            style={{ width: 32, height: 32, backgroundColor: "transparent" }}
                            source={{ uri: Config.API_MEDIA_URL + props.image_url }}
                        />
                    </View>
                </HeaderLeft>
            )}
            subtitle={(
                <HeaderLeft>
                    <Text color={colorText} >{props.name}</Text>
                </HeaderLeft>
            )}
            rightIcon={(
                <HeaderRight>
                    <Text bold color={colorText} size={20} style={{ marginHorizontal: 4 }} >{props.kcoins}</Text>
                    <Image style={{ height: 22, width: 22 }} source={images.Coin} />
                </HeaderRight>
            )}
            onPress={props.onPress}
            containerStyle={containerStyle}
        />
    )
}

export default class Home extends Component {

    static navigationOptions = {
        drawerLabel: 'Home',
        header: null //(<HeaderComponent title="Killer Sales" option="partial" />)
    }

    constructor(props) {
        super(props)
        notificationHelper.init(null)
        this.state = {
            userSession: null,
            killers: []
        }
    }

    componentDidMount() {
        this.getUserSession()
        this.handleSearch()
    }

    validateGeolocation() {
        if (Platform.OS === 'android') {
            PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
                .then(granted => {
                    if (granted) this.watchGeolocation()
                })
        } else {
            this.watchGeolocation()
        }
    }

    watchGeolocation() {
        this.watchId = navigator.geolocation.watchPosition(
            (position) => {
                let params = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                }

                sessionHelper.getDataList('meGeolocation').then((geo) => {
                    if (geo !== null) {
                        if (params.lat !== geo.latitude && params.lng !== geo.longitude) {
                            this.updateGeolocation(params)
                        }
                    } else {
                        this.updateGeolocation(params)
                    }
                })
            },
            (error) => {
                console.warn("error", error)
            },
            { enableHighAccuracy: false, timeout: 30000, maximumAge: 1000, distanceFilter: 1 }
        )
    }

    updateGeolocation(params) {
        promotorService.update(this.state.userSession.id, params)
            .then((response) => {
                let region = {
                    latitude: params.lat,
                    longitude: params.lng,
                    latitudeDelta: 2.71,
                    longitudeDelta: 0.77
                }
                sessionHelper.saveDataList(region, 'meGeolocation')
            })
            .catch((error) => {

            })
    }

    componentWillUnmount() {
        navigator.geolocation.clearWatch(this.watchId)
    }

    getUserSession() {
        sessionHelper.getUser().then((user) => {
            if (user !== null) {
                this.setState({ userSession: user }, () => {
                    this.validateGeolocation()
                })
            }
        })
    }

    handleSearch() {
        const params = {
            name: '',
            page_size: 20
        }

        promotorService.search(params).then((response) => {
            this.setState({ killers: response.data.data.results })
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>

                <StatusBar backgroundColor="#000036" barStyle="light-content" />

                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>

                    <Header
                        transparent menu
                        centerComponent={<HeaderTitle>RANKING</HeaderTitle>}
                        rightComponent={(<View style={{ width: 48 }} />)}
                    />

                    {this.state.userSession && <View style={{ alignItems: 'center', height: 80, paddingBottom: 8 }}>
                        <View style={{ width: 56, flex: 1 }}  >
                            <SVGImage
                                style={{ width: 56, height: 56, backgroundColor: "transparent" }}
                                source={{ uri: Config.API_MEDIA_URL + this.state.userSession.avatar.url }}
                            />
                        </View>
                        <View><Text bold color='#FFF'>N° 8</Text></View>

                    </View>}
                </ImageBackground>

                <FlatList
                    data={this.state.killers}
                    renderItem={({ item, index }) => <ItemKiller
                        order={index + 1}
                        name={item.name}
                        kcoins={item.kcoins}
                        image_url={item.avatar ? item.avatar.url : null}
                        onPress={() => { alert("KILLER: " + item.name) }}
                    />}
                    keyExtractor={(item, index) => (item.id).toString()}
                    style={{ paddingTop: 4 }}
                />

            </View>
        )
    }
}

