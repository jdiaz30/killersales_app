/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, StatusBar, ImageBackground, Image, Alert, AsyncStorage } from 'react-native'
import { Button, FormLabel, FormInput, FormValidationMessage, Card } from 'react-native-elements'
import { requestOneTimePayment } from 'react-native-paypal'

import images from '../components/images'
import loginStyle from '../styles/login.style'

import * as authService from '../services/auth-service'
import * as orderService from '../services/order-service'
import * as httpService from '../services/http-service'
import * as appService from '../services/app-service'

import * as sessionHelper from '../helpers/session-helper'
import * as globalHelper from '../helpers/global-helper'

import RecoveryPasswordModal from './recovery-password'
import { TextInputForm, LabelInputForm } from '../components'


export default class Login extends Component {

    constructor(props) {
        super(props)

        this.state = {
            userName: "",
            password: "",
            isLoading: false,
            showRecoverPasswordModal: false
        }
    }

    static navigationOptions = {
        title: 'Iniciar sesión',
        header: null
    }

    async paypalDemo(token) {
        const {
            nonce,
            payerId,
            email,
            firstName,
            lastName,
            phone
        } = await requestOneTimePayment(token.trim(),
            {
                amount: '5', // required
                // any PayPal supported currency (see here: https://developer.paypal.com/docs/integration/direct/rest/currency-codes/#paypal-account-payments)
                currency: 'USD',
                // any PayPal supported locale (see here: https://braintree.github.io/braintree_ios/Classes/BTPayPalRequest.html#/c:objc(cs)BTPayPalRequest(py)localeCode)
                localeCode: 'en_US',
                shippingAddressRequired: false,
                userAction: 'commit', // display 'Pay Now' on the PayPal review page
                // one of 'authorize', 'sale', 'order'. defaults to 'authorize'. see details here: https://developer.paypal.com/docs/api/payments/v1/#payment-create-request-body
                intent: 'authorize',
            }
        )
    }

    handleLogin() {

        this.setState({ isLoading: true })

        const data = {
            user_name: this.state.userName,
            password: this.state.password
        }

        authService.login(data, { source: 'app' })
            .then((response) => {
                this.setState({ isLoading: false })
                this._authSuccess(response.data.data.token)

                globalHelper.showMessageAlert('Éxito!', 'Bienvenido a Behlaak Tours', () => {
                    this.definePage()
                })
            })
            .catch((error) => {
                const errorResponse = error.response.data

                globalHelper.showMessageAlert('Error!', errorResponse.user_msg, () => {
                    if ((JSON.stringify(errorResponse.data)).indexOf('message_code') > 0 && errorResponse.data.message_code == 2) {
                        this.sendCodePhone(errorResponse.data)
                    } else {
                        this.setState({ isLoading: false })
                    }
                })
            })
    }

    sendCodePhone(data) {
        const promotor = data.promotor
        const user = data.userapp
        const self = this
        const params = {
            userapp: user,
            promotor: promotor
        }

        appService.sendValidatePhoneAccount(params).then((response) => {
            this.setState({ isLoading: false })
            self.props.navigation.navigate('ConfirmAcount', { phone: promotor.phone, numberArea: promotor.country.number_area, userTemp: user })
        }).catch((error) => {
            console.warn("sendCodePhone", error)
            this.setState({ isLoading: false })
        })
    }

    definePage() {
        sessionHelper.saveWelcome()
        this.props.navigation.navigate('Welcome')
    }

    _authSuccess(token) {
        sessionHelper.saveToken(token)
        httpService.initAuth()
    }

    render() {
        return (

            <ImageBackground source={images.Playa} style={{ flex: 1 }} resizeMode='cover'>

                <View style={loginStyle.boxLogin}>

                    <StatusBar backgroundColor="#000036" barStyle="light-content" />

                    <View style={loginStyle.logo}>
                        <Image source={images.LogoWShite} style={{ width: 180, height: 172 }} />
                    </View>

                    <View style={{ maxWidth: 300, width: '100%', paddingHorizontal: 16 }}>

                        <TextInputForm placeholder="Usuario" value={this.state.userName} onChangeText={(userName) => this.setState({ userName })} selectionColor={'yellow'} keyboardType='email-address' />
                        <TextInputForm placeholder="Contraseña" value={this.state.password} onChangeText={(password) => this.setState({ password })} selectionColor={'yellow'} textContentType='password' secureTextEntry={true} />

                        <View style={{ marginVertical: 1 }}>
                            <Button title='INICIAR SESIÓN' onPress={this.handleLogin.bind(this)} rounded={true} backgroundColor='#00add2' color="#fff" fontFamily="RobotoCondensed-Bold" containerViewStyle={{ marginLeft: 0, marginRight: 0 }} buttonStyle={{ borderRadius: 4 }} loading={this.state.isLoading} />
                        </View>

                        <View style={{ marginVertical: 1 }}>
                            <Button title='Crear una cuenta' onPress={() => this.props.navigation.navigate('Register')} transparent={true} color="#fff" fontFamily="RobotoCondensed-Bold" fontSize={17} />
                        </View>

                        <View style={{ marginVertical: 1 }}>
                            <Button title='¿Olvido su contraseña?' onPress={() => { this.setState({ showRecoverPasswordModal: true }) }} transparent={true} color="#fff" fontFamily="RobotoCondensed-Bold" fontSize={14} textStyle={{ textDecorationLine: "underline" }} />
                        </View>

                    </View>

                </View >

                <RecoveryPasswordModal
                    animationType="fade"
                    transparent={false}
                    visible={this.state.showRecoverPasswordModal}
                    onClose={() => { this.setState({ showRecoverPasswordModal: false }) }}

                />
            </ImageBackground>

        );
    }
}

