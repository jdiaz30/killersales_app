/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { View, TouchableOpacity, Image, FlatList, ImageBackground, ScrollView, StyleSheet, Dimensions, Alert, StatusBar, Picker } from 'react-native'
import { Icon, Avatar, Card, Button, FormInput, Text, ButtonGroup, FormLabel, CheckBox, SearchBar } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'
import DateTimePicker from 'react-native-modal-datetime-picker'
import { Dropdown } from 'react-native-material-dropdown'
import NumericInput, { calcSize } from 'react-native-numeric-input'
import moment from 'moment'
import AnimatedHideView from 'react-native-animated-hide-view'
import { Header, HeaderButton, HeaderTitle } from '../../components'
import HeaderComponent from '../../components/header/header'

import Config from "../../app.settings"
import images from '../../components/images'

import * as sessionHelper from '../../helpers/session-helper'

import * as clanService from '../../services/clan-service'
import * as promotorService from '../../services/promotor-service'
import * as hotelService from '../../services/hotel-service'

export default class ServiceHotel extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            searchTxt: "",
            hotels: []
        }
        this.orderId = this.props.navigation.getParam("orderId")
    }

    componentDidMount() {
        this.handleSearch()
        this.listenEvents()
    }

    componentWillUnmount() {
        EventRegister.removeEventListener(this.listener)
    }

    listenEvents() {
        const self = this
        this.listener = EventRegister.addEventListener('order:registerHotel', (data) => {
            self.handleSelectHotel(data)
        })
    }

    handleClose() {
        this.props.navigation.pop()
    }

    handleSearch() {
        const params = {
            name: this.state.searchTxt,
            page_size: 20,
            status: 1
        }

        hotelService.getAll(params).then((response) => {
            this.setState({ hotels: response.data.data.results })
        })
    }

    handleSelectHotel(hotel) {
        EventRegister.emit("selectedHotel", hotel)
        this.props.navigation.pop()
    }

    handleAddHotel() {
        let navigate = (this.orderId == null) ? "ServiceHotelAdd" : "DealsServiceHotelAdd"
        this.props.navigation.push(navigate)
    }

    render() {
        return (

            <View style={{ flex: 1, backgroundColor: "white" }}>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />

                <ImageBackground source={images.ProfileBack} style={{
                    width: 'auto',
                    height: 'auto',

                }} resizeMode='cover'>
                    <Header
                        transparent
                        leftComponent={(
                            <HeaderButton icon="md-arrow-back" color="white" typeicon="ionicon" onPress={this.handleClose.bind(this)} />
                        )}
                        centerComponent={<HeaderTitle>Buscar Hotel</HeaderTitle>}
                        rightComponent={(
                            <HeaderButton icon="md-add" color="white" typeicon="ionicon" onPress={this.handleAddHotel.bind(this)} />
                        )}
                    />

                </ImageBackground>
                <View>
                    <SearchBar
                        onSubmitEditing={this.handleSearch.bind(this)}
                        round
                        lightTheme
                        onChangeText={(searchTxt) => this.setState({ searchTxt })}
                        placeholder='Buscar ...' />
                </View>

                <View style={{ marginTop: 8 }}>
                    <FlatList
                        data={this.state.hotels}
                        renderItem={({ item, index }) =>

                            <TouchableOpacity onPress={this.handleSelectHotel.bind(this, item)}>
                                <View style={{ padding: 12, backgroundColor: 'white', borderBottomColor: "#f7f7f7", borderBottomWidth: 1, flex: 1 }}>
                                    <View style={{ flexDirection: 'row', alignContent: "space-between", marginBottom: 5 }}>
                                        <CheckBox
                                            title=''
                                            containerStyle={{ backgroundColor: 'transparent', borderRadius: 0, borderWidth: 0, padding: 0, margin: 0, alignItems: 'flex-start' }}
                                        />
                                        <Text style={{ alignItems: 'flex-start', fontFamily: "RobotoCondensed-Light" }}> {item.name}</Text>

                                    </View>
                                </View>

                            </TouchableOpacity>
                        }
                        keyExtractor={(item, index) => (item.id).toString()}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>

            </View >
        )
    }
}

