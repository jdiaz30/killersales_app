
import React, { Component } from 'react'
import { View, TouchableOpacity, Image, Text, FlatList, ImageBackground, ScrollView, StatusBar, StyleSheet } from 'react-native'
import { Avatar, Icon, ListItem, Button } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'
import Toast, { DURATION } from 'react-native-easy-toast'

import Config from "../../app.settings"
import images from '../../components/images'

import * as sessionHelper from '../../helpers/session-helper'
import * as dateHelper from '../../helpers/date-helper'
import * as globalHelper from '../../helpers/global-helper'

import * as promotorService from '../../services/promotor-service'
import * as orderService from '../../services/order-service'
import * as orderCobrateService from '../../services/order-cobrate-service'

import { Header, HeaderButton, Card, CardHeader, AvatarCircle, HeaderLeft, HeaderCenter, HeaderRight } from '../../components'


export default class ServiceDetail extends Component {
    static navigationOptions = {
        headerTitle: "Orden de servicio",
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            order: {},
            activities: [],
            orderCobrate: null,
            messageLoading: "Cargando ...",
            isLoading: false
        }
    }

    componentDidMount() {
        let orderId = this.props.navigation.getParam('orderId')
        this.getOrder(orderId)
        this.getOrderCobrate(orderId)
    }

    getOrder(orderId) {
        this.setState({ isLoading: true }, () => {
            orderService.get(orderId)
                .then((response) => {
                    let order = response.data.data.order
                    let activities = response.data.data.activities
                    this.setState({ order: order, activities: activities, isLoading: false })
                })
                .catch((error) => {
                    this.setState({ isLoading: false })
                })
        })
    }

    getOrderCobrate(orderId) {
        this.setState({ isLoading: true }, () => {
            orderService.listOrderCobrate(orderId)
                .then((response) => {
                    this.setState({ orderCobrate: response.data.data, isLoading: false })
                })
                .catch((error) => {
                    this.setState({ isLoading: false })
                })
        })
    }

    handleClose() {
        this.props.navigation.pop()
    }

    getPriceTotalActivity(priceAdult, priceChild, order) {
        priceAdult = (order.nro_adults) * parseFloat(priceAdult)
        priceChild = (order.nro_childs) * parseFloat(priceChild)
        let total = priceAdult + priceChild
        return total.toFixed(2)
    }

    handleAcceptCobrate() {
        globalHelper.showMessageConfimrAlert("Confirmar", "¿Acepta pagar el monto de esta reserva?", "Si", "No", () => {
            this.setState({ isLoading: true, messageLoading: "Procesando ..." }, () => {
                orderCobrateService.update(this.state.orderCobrate.id, { status: 2 })
                    .then((response) => {
                        this.setState({ isLoading: false })
                        this.refs.toast.show('Proceso realizado con éxito', 2000)
                        this.handleClose()
                    })
                    .catch((error) => {
                        this.setState({ isLoading: false })
                        this.refs.toast.show('Proceso no se pudo completar', 2000)
                    })
            })

        }, null)
    }

    renderActivities() {
        let { order, activities } = this.state
        return <FlatList
            data={activities}
            renderItem={({ item, index }) => <View style={{ flexDirection: 'row', flex: 1 }}>
                <View style={{ width: '25%', alignItems: 'flex-start' }}>
                    <Image source={{ uri: Config.API_MEDIA_URL + item.activity.poster_500 }} style={styles.activityImage} resizeMode={'cover'} />
                </View>
                <View style={{ width: '25%', alignItems: 'flex-start', marginTop: 15 }}>
                    <Text style={styles.textActivityList}>{item.activity.name}</Text>
                </View>
                <View style={{ width: '25%', alignItems: 'flex-start', marginTop: 15 }}>
                    <Text style={styles.textActivityList}> {order.money.simbol} {this.getPriceTotalActivity(item.price_pub_adult, item.price_pub_child, order)}</Text>
                </View>

            </ View>}

            keyExtractor={(item, index) => (item.id).toString()}
            showsHorizontalScrollIndicator={false}
        />
    }

    renderGuests() {
        let { order } = this.state
        return <View>
            <ListItem
                leftIcon={(
                    <Image style={{ width: 30, height: 36, marginRight: 24 }} resizeMode={'cover'} source={images.Adult} />
                )}
                subtitle={(
                    <Text style={styles.textPaxList}>Adultos: +12 años</Text>
                )}
                rightIcon={(
                    <Text style={styles.textPaxList}>{order.nro_adults}</Text>
                )}
                containerStyle={{ borderBottomWidth: 0, }}
            />

            <ListItem
                leftIcon={(
                    <Image style={{ width: 36, height: 33, marginRight: 18 }} resizeMode={'cover'} source={images.Menor} />
                )}
                subtitle={(
                    <Text style={styles.textPaxList}>Menor : 5 - 11 años</Text>
                )}
                rightIcon={(
                    <Text style={styles.textPaxList}>{order.nro_childs}</Text>
                )}
                containerStyle={{ borderBottomWidth: 0, }}
            />

            <ListItem
                leftIcon={(
                    <Image style={{ width: 27, height: 43, marginRight: 26 }} resizeMode={'cover'} source={images.Infante} />
                )}
                subtitle={(
                    <Text style={styles.textPaxList}>Infante : 0 - 4 años</Text>
                )}
                rightIcon={(
                    <Text style={styles.textPaxList}>{order.nro_babys}</Text>
                )}
                containerStyle={{ borderBottomWidth: 0, }}
            />
        </View>

    }

    render() {
        let { order, activities } = this.state
        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <ImageBackground source={images.ProfileBackTwo} style={{ width: '100%', flex: 1, height: '100%' }} resizeMode='cover'>

                        <View style={{ flex: 1, height: '100%' }}>
                            <View style={{ height: 240 }}>
                                <ImageBackground source={images.ProfileBack} style={{ width: '100%', flex: 1, justifyContent: 'center', height: '100%' }} resizeMode='cover'>

                                    <View style={{ width: '100%', height: '100%' }}>

                                        <Header transparent

                                            leftComponent={(
                                                <HeaderButton icon="md-arrow-back" color="white" typeicon="ionicon" onPress={this.handleClose.bind(this)} />
                                            )}
                                            centerComponent={(
                                                <Text style={{ color: '#FFF', fontFamily: "RobotoCondensed-Bold", fontSize: 20 }}>
                                                    Reserva # {order.id}
                                                </Text>
                                            )}
                                            rightComponent={(<View style={{ width: 56 }} />)}
                                        />

                                        <View style={{ flex: 1, padding: 8 }}>
                                            <View style={{ marginBottom: 28, marginTop: 10 }}>
                                                <Text style={{ color: '#FFF', fontFamily: "RobotoCondensed-Bold", fontSize: 16 }}>
                                                    {dateHelper.utcToLocal(order.date_order, 'LLLL')}
                                                </Text>
                                            </View>
                                            <View style={{ marginBottom: 8 }}>
                                                {order.operator != undefined &&
                                                    <Text style={{ color: '#FFF', fontFamily: "RobotoCondensed-Bold", fontSize: 16 }}>
                                                        {order.operator.name} - ({order.money.name})</Text>
                                                }

                                            </View>

                                            <View style={{ flexDirection: 'row', flex: 1 }}>
                                                <View style={{ width: '25%', alignItems: 'flex-start', alignContent: 'center' }}>
                                                    <View style={{ alignItems: 'center' }}>
                                                        <Text style={{ color: '#4a4989', fontFamily: "RobotoCondensed-Bold", fontSize: 15 }}>Total</Text>
                                                        <Text style={{ color: '#FFF', fontFamily: "RobotoCondensed-Bold", fontSize: 17 }}>
                                                            {(order.money != undefined) ? order.money.simbol : ""}  {order.total}
                                                        </Text>
                                                    </View>
                                                </View>

                                                {order.type_payment == "deposito" &&
                                                    <View style={{ width: '25%', alignItems: 'flex-start', alignContent: 'center' }}>
                                                        <View style={{ alignItems: 'center' }}>
                                                            <Text style={{ color: '#4a4989', fontFamily: "RobotoCondensed-Bold", fontSize: 15 }}>Déposito</Text>
                                                            <Text style={{ color: '#FFF', fontFamily: "RobotoCondensed-Bold", fontSize: 17 }}>
                                                                {(order.money != undefined) ? order.money.simbol : ""}  {order.total_payment}
                                                            </Text>
                                                        </View>
                                                    </View>
                                                }

                                                {order.type_payment == "deposito" &&
                                                    <View style={{ width: '25%', alignItems: 'flex-start', alignContent: 'center' }}>
                                                        <View style={{ alignItems: 'center' }}>
                                                            <Text style={{ color: '#4a4989', fontFamily: "RobotoCondensed-Bold", fontSize: 15 }}>Balance</Text>
                                                            <Text style={{ color: '#FFF', fontFamily: "RobotoCondensed-Bold", fontSize: 17 }}>
                                                                {(order.money != undefined) ? order.money.simbol : ""}  {(order.total - order.total_payment).toFixed(2)}
                                                            </Text>
                                                        </View>
                                                    </View>
                                                }

                                                {order.status == 1 &&
                                                    <View style={{ width: '35%', alignItems: 'flex-start' }}>
                                                        <View style={{ alignItems: 'center' }}>
                                                            <Button title="Pagar" backgroundColor='#fdec5b' color="#2b2e34" buttonStyle={{ borderRadius: 4 }} fontFamily="RobotoCondensed-Bold" onPress={this.handleAcceptCobrate.bind(this)} />
                                                        </View>
                                                    </View>
                                                }

                                            </View>

                                        </View>

                                    </View>
                                </ImageBackground>
                            </View>

                            <View style={{ flex: 1, paddingLeft: 5, paddingRight: 5 }}>
                                <Card containerStyle={styles.card}>
                                    <View style={{ padding: 10 }}>
                                        <Text style={{ color: '#919191', fontFamily: "RobotoCondensed-Bold", fontSize: 13 }}>ACTIVIDADES</Text>
                                    </View>
                                    <View style={{ padding: 10 }}>
                                        {this.renderActivities()}
                                    </View>
                                </Card>

                                <Card containerStyle={styles.card}>
                                    <View style={{ padding: 10 }}>
                                        <Text style={{ color: '#919191', fontFamily: "RobotoCondensed-Bold", fontSize: 13 }}>PAX</Text>
                                    </View>
                                    <View style={{ padding: 10 }}>
                                        {this.renderGuests()}
                                    </View>
                                </Card>
                            </View>
                        </View>

                    </ImageBackground>
                </ScrollView>

                <OrientationLoadingOverlay
                    visible={this.state.isLoading}
                    color="white"
                    indicatorSize="large"
                    messageFontSize={18}
                    message={this.state.messageLoading}
                />
                <Toast ref="toast" position='top' positionValue={200} />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    card: {
        flex: 1,
        margin: 8,
        marginBottom: 8,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowColor: '#081257',
        elevation: 4,
        width: 'auto',
    },
    textActivityList: {
        color: '#919191',
        fontFamily: "RobotoCondensed-Bold",
        fontSize: 13
    },
    activityImage: {
        height: 55,
        width: 55
    },
    textPaxList: {
        color: '#919191',
        fontFamily: "RobotoCondensed-Bold",
        fontSize: 13,
        marginTop: 8
    },
})