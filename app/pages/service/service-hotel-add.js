/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { View, TouchableOpacity, Image, FlatList, ImageBackground, ScrollView, StyleSheet, Dimensions, Alert, StatusBar, Picker } from 'react-native'
import { Icon, Avatar, Card, Button, FormInput, Text, ButtonGroup, FormLabel, CheckBox, SearchBar } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'
import DateTimePicker from 'react-native-modal-datetime-picker'
import { Dropdown } from 'react-native-material-dropdown'
import NumericInput, { calcSize } from 'react-native-numeric-input'
import moment from 'moment'
import AnimatedHideView from 'react-native-animated-hide-view'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'
import Toast, { DURATION } from 'react-native-easy-toast'


import { Header, HeaderButton, HeaderTitle, TextInputForm, LabelInputForm } from '../../components'
import HeaderComponent from '../../components/header/header'

import Config from "../../app.settings"
import images from '../../components/images'

import * as sessionHelper from '../../helpers/session-helper'

import * as clanService from '../../services/clan-service'
import * as promotorService from '../../services/promotor-service'
import * as hotelService from '../../services/hotel-service'

export default class ServiceHotelAdd extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            name: "",
            isLoading: false
        }

    }

    componentDidMount() {

    }

    handleClose() {
        this.props.navigation.pop()
    }

    handleSave() {
        const data = { name: this.state.name, status: 1 }
        if ((this.state.name).trim() !== "") {
            this.setState({ isLoading: true }, () => {
                hotelService.register(data).then((response) => {
                    this.setState({ isLoading: false })
                    EventRegister.emit("order:registerHotel", response.data.data)
                    this.refs.toast.show('Hotel registrado correctamente')
                    this.handleClose()
                }).catch((error) => {
                    this.setState({ isLoading: false })
                    this.refs.toast.show('No se pudo registrar')
                })
            })
        } else {
            this.refs.toast.show('Ingrese el nombre del Hotel')
        }

    }

    render() {

        return (

            <View style={{ flex: 1, backgroundColor: "white" }}>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />

                <View style={{ height: 50 }}>
                    <ImageBackground source={images.ProfileBack} style={{
                        width: 'auto',
                        height: 'auto',

                    }} resizeMode='cover'>
                        <Header
                            transparent
                            leftComponent={(
                                <HeaderButton icon="md-arrow-back" color="white" typeicon="ionicon" onPress={this.handleClose.bind(this)} />
                            )}
                            centerComponent={<HeaderTitle>Registrar Hotel</HeaderTitle>}
                            rightComponent={(
                                <HeaderButton icon="md-checkmark" color="white" typeicon="ionicon" onPress={this.handleSave.bind(this)} />
                            )}
                        />

                    </ImageBackground>
                </View>
                <View style={styles.containerForm}>
                    <LabelInputForm style={{ fontSize: 20 }}>Ingrese nombre del hotel</LabelInputForm>
                    <TextInputForm
                        editable={true}
                        value={this.state.name}
                        onChangeText={(name) => this.setState({ name })}
                    />
                </View>

                <OrientationLoadingOverlay
                    visible={this.state.isLoading}
                    color="white"
                    indicatorSize="large"
                    messageFontSize={18}
                    message="Registrando ..."
                />

                <Toast ref="toast" />

            </View >
        )
    }
}

const styles = StyleSheet.create({
    containerForm: {
        padding: 22
    }
})

