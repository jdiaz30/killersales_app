
import React, { Component } from 'react'
import { View, TouchableOpacity, Image, FlatList, ImageBackground, ScrollView, StyleSheet, Dimensions, Alert, StatusBar, Picker, TextInput, TouchableHighlight, Modal } from 'react-native'
import { Icon, ButtonGroup, CheckBox, ListItem } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'
import DateTimePicker from 'react-native-modal-datetime-picker'
import { Dropdown } from 'react-native-material-dropdown'
import NumericInput, { calcSize } from 'react-native-numeric-input'
import moment from 'moment'
import AnimatedHideView from 'react-native-animated-hide-view'
import ModalDropdown from 'react-native-modal-dropdown'
import Flag from 'react-native-flags'
import Toast, { DURATION } from 'react-native-easy-toast'

import Config from "../../app.settings"
import images from '../../components/images'
import { Header, HeaderButton, HeaderTitle, Card, Text, TextInputForm, Button, InputNumber } from '../../components'
import { SelectPickupModal } from '../../modals'

import * as sessionHelper from '../../helpers/session-helper'

import * as orderService from '../../services/order-service'
import * as catalogService from '../../services/catalog-service'
import * as cityService from '../../services/city-service'
import * as pickupService from '../../services/pickup.service'
import * as appService from '../../services/app-service'
import * as clientService from '../../services/client-service'
import * as activityService from '../../services/activity-service'
import * as moneyService from '../../services/money-service'
import * as divisaService from '../../services/divisa-service'

import * as globalHelper from '../../helpers/global-helper'
import * as httpService from '../../services/http-service'
import * as dateHelper from '../../helpers/date-helper'
import * as orderHelper from '../../helpers/order-helper'

import serviceStyle from '../../styles/service.style'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'
import WebView from 'react-native-webview'

/* const CatItem = props => {
    return (
        <Card containerStyle={{ flex: 1, margin: 8, marginBottom: 8, shadowOpacity: 0.75, shadowRadius: 5, shadowColor: '#081257', elevation: 4, position: 'relative', width: 'auto' }} >
            <View
                onPress={props.onPress}
                style={{ height: 140, width: 'auto', borderTopLeftRadius: 8, borderTopRightRadius: 8, backgroundColor: '#CCC' }}>
                <Image source={{ uri: props.image }} style={{ height: '100%', width: '100%', borderTopLeftRadius: 8, borderTopRightRadius: 8 }} resizeMode={'cover'} />

                <View style={{ position: 'absolute', top: 8, right: 0 }} >
                    <HeaderButton icon='md-trash' typeicon='ionicon' />
                </View>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', padding: 8, height: 80 }}>
                <Text style={{ color: '#000', fontSize: 18 }} numberOfLines={1} fontFamily="RobotoCondensed-Bold">{props.name}</Text>
                <View style={{ flexDirection: 'row' }}>
                    <Icon name="md-person" type="ionicon" size={14} color="#000040" />
                    <Text style={{ color: "#000040" }} fontFamily="RobotoCondensed-Bold"> $ {props.price_rep_adult}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Icon name="md-happy" type="ionicon" size={14} color="#000040" />
                    <Text style={{ color: "#000040" }} fontFamily="RobotoCondensed-Bold">  $ {props.price_rep_child}</Text>
                </View>
                <View>
                    <TouchableOpacity onPress={this.handleAddHotel.bind(this)} >
                        <TextInputForm
                            placeholder={'Definir Fecha'} editable={false}
                            value={this.state.hotel == null ? "" : this.state.hotel.name}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </Card>
    )
} */

export default class OrderService extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        let catalog = this.props.navigation.getParam('catalog')
        this.state = {
            showPaypal: false,
            isDateTimePickerVisible: false,
            selectedIndex: 0,
            selectedCity: this.props.navigation.getParam('city'),
            catalog: catalog,
            date_order: moment().format("YYYY-MM-DD HH:mm:ss"),
            nro_hab: "",
            name_client: "",
            address_client: "",
            phone_client: "",
            email_client: "",
            nro_adults: 0,
            nro_childs: 0,
            nro_babys: 0,
            total: 0,
            total_adults: 0,
            total_childs: 0,
            hotel: null,
            service_selected_date: null,
            services: this.props.navigation.getParam('services'),
            cities: [],
            isLoading: false,
            balance: "0",
            payments: [{ value: "efectivo", label: "Efectivo" }, { value: "tarjeta-debito", label: "Tarjeta Débito" }, { value: "tarjeta-credito", label: "Tarjeta Crédito" }, { value: "paypal", label: "Paypal" }, { value: "mercadopago", label: "Mercado Pago" }],
            type_payment: [{ value: "todo", label: "100%" }, { value: "deposito", label: "Déposito" }],
            selected_type_payments: "",
            selected_method_payments: "",
            total_payment: "0",
            dates_service: {},
            reservationComplete: false,
            pickups: [],
            isModalPickupVisible: false,
            countries: null,
            selectedCountry: null,
            number_area_select: null,
            verifyPhone: false,
            messageLoading: "Cargando ...",
            typeForm: "register",
            money: (catalog !== null) ? catalog.operator.money_id : 0,
            moneyList: [],
            moneySimbol: (catalog !== null) ? catalog.operator.money.simbol : ""
        }

        this.userSession = this.props.navigation.getParam('userSession')
        this.orderId = null
        this.servicesRemove = []
        this.paypalData = { url: "" }
    }

    componentDidMount() {
        this.getUserSession()
    }

    getUserSession() {
        sessionHelper.getUser().then((user) => {
            if (user !== null) {
                this.userSession = user
            }
        })
    }

    getMoneys() {
        sessionHelper.getDataList('moneys').then((moneys) => {
            if (moneys == null) {
                divisaService.getAll({ page_size: 10, money_a_id: this.state.catalog.operator.money_id })
                    .then((response) => {
                        let moneys = []
                        let money = this.state.catalog.operator.money

                        moneys.push({
                            id: money.id,
                            name: money.name,
                            money_a_value: 0,
                            money_b_value: 0
                        })

                        let divisas = response.data.data.results.map((item) => {
                            item.id = item.money_b_id
                            item.name = item.money_b.name
                            return item
                        })

                        let data = moneys.concat(divisas)

                        this.setMoneys(data)
                    })
                    .catch((error) => {

                    })
            } else {
                this.setMoneys(moneys)
            }
        })
    }

    setMoneys(data) {
        this.setState({ moneyList: data })
        sessionHelper.saveDataList(data, 'moneys')
    }

    getCountries() {
        sessionHelper.getDataList('countries').then((countries) => {
            if (countries == null) {
                appService.getAllCountries({ status: 1, page_size: 50 }).then((response) => {
                    this.setCountries(response.data.data.results)
                }).catch((error) => {

                })
            } else {
                this.setCountries(countries)
            }
        })
    }

    setCountries(data) {
        this.setState({ countries: data, selectedCountry: data[0], number_area_select: data[0].number_area })
        sessionHelper.saveDataList(data, 'countries')
    }

    clearStorageByPayment = () => {
        //console.log("clearStorageByPAyment")
        sessionHelper.removeActivities()
        sessionHelper.removeCatalog()
    }

    saveDataStorage = () => {
        const {
            //selectedIndex,
            nro_hab,
            name_client,
            phone_client,
            email_client,
            nro_adults,
            nro_childs,
            nro_babys,
            hotel,
            selected_type_payments,
            selected_method_payments,
            dates_service
        } = this.state

        const data = {
            //selectedIndex,
            nro_hab,
            name_client,
            phone_client,
            email_client,
            nro_adults,
            nro_childs,
            nro_babys,
            hotel,
            selected_type_payments,
            selected_method_payments,
            dates_service
        }
        sessionHelper.saveOrder(data)
    }

    getDataStorage = () => {
        return new Promise((resolve, reject) => {
            sessionHelper.getOrder().then((order) => {
                var data = {}
                if (order) {
                    const {
                        // selectedIndex,
                        nro_hab,
                        name_client,
                        phone_client,
                        email_client,
                        nro_childs,
                        nro_babys,
                        nro_adults,
                        hotel,
                        selected_type_payments,
                        selected_method_payments,
                        dates_service
                    } = order
                    data = {
                        //selectedIndex,
                        nro_hab,
                        name_client,
                        phone_client,
                        email_client,
                        nro_adults,
                        nro_childs,
                        nro_babys,
                        hotel,
                        selected_type_payments,
                        selected_method_payments,
                        dates_service
                    }
                    this.setState(data)
                }
                resolve()
            }).catch((error) => {
                console.log("joper30 error", error)
                reject()
            })
        })
    }

    componentDidMount() {
        let pagePrev = this.props.navigation.getParam('pagePrev')
        this.orderId = this.props.navigation.getParam('orderId')

        this.getCountries()
        this.getMoneys()
        if (pagePrev && pagePrev == "deals") {
            this.getOrder(this.orderId)
        } else {
            this.getDataStorage().then(() => {
                var services = []

                this.state.services.forEach((act, index) => {
                    act.pick_up_date = (this.state.dates_service && this.state.dates_service['service_' + act.id]) ? this.state.dates_service['service_' + act.id]['date'] : ''
                    services.push(act)
                })
                this.setState({ services: services }, () => {
                    this.getTotal()
                })
            })
        }

        this.listenEvents()
    }

    getOrder(orderId) {
        this.setState({ isLoading: true, typeForm: "edit" }, () => {
            orderService.get(orderId)
                .then((response) => {
                    let services = response.data.data.activities
                    let order = response.data.data.order
                    let city = order.city

                    services.map((service) => {
                        service.poster = (service.activity.poster !== null) ? Config.API_MEDIA_URL + service.activity.poster_500 : "https://www.cultura.gal/sites/default/files/images/evento/ameixarock.png"
                        service.id = (service.activity.id).toString()
                        service.pick_up_date = dateHelper.extractDateOnly(service.pick_up_date)
                        service.name = service.activity.name
                        service.description = service.activity.description

                        if (service.pickup !== null) {
                            let pickup = orderHelper.parsePickUps([service.pickup], service.pick_up_date)
                            pickup = pickup[0]
                            service.time = pickup.time
                            service.address = pickup.address
                        }

                        /*   dates_service['service_' + item.id] = {
                              date: item.pick_up_date,
                              time: item.time,
                              address: item.address
                          } */
                        return service
                    })

                    let parsePhone = orderHelper.parsePhone(order.phone_client)
                    let operator = order.operator
                    order.phone_client = parsePhone.phone

                    this.setState({
                        isLoading: false,
                        services: services,
                        name_client: order.name_client,
                        email_client: order.email_client,
                        phone_client: order.phone_client,
                        number_area_select: parsePhone.numberArea,
                        nro_adults: order.nro_adults,
                        nro_childs: order.nro_childs,
                        nro_babys: order.nro_babys,
                        catalog: { operator_id: operator.id, operator: operator },
                        selectedCity: city,
                        nro_hab: (order.nro_hab).toString(),
                        address_client: order.address_client,
                        hotel: order.hotel,
                        total_payment: order.total_payment,
                        selected_method_payments: order.method_payment,
                        selected_type_payments: order.type_payment,
                        total: order.total,
                        selectedCountry: order.client.country,
                    })
                    //console.warn("getOrder", response.data.data.acti)
                })
                .catch((error) => {
                    this.setState({ isLoading: false })
                })
        })
    }

    //Destroy component
    componentWillUnmount() {
        this.saveDataStorage()
        if (this.state.reservationComplete) {
            this.clearStorageByPayment()
        }
        EventRegister.removeEventListener(this.listener)
        EventRegister.removeEventListener(this.listenerPickup)
    }

    listenEvents() {
        const self = this
        this.listener = EventRegister.addEventListener('selectedHotel', (data) => {
            this.setState({ hotel: data })
        })

        this.listenerPickup = EventRegister.addEventListener('order:selectPickup', (pickup) => {
            self._handlePickup(pickup)
        })
    }

    listServices() {
        sessionHelper.getActivities().then((activitiesStorage) => {
            catalogService.getActivities(this.state.catalog.id).then((response) => {
                var services = []
                const data = response.data
                data.forEach((item, index) => {
                    if (activitiesStorage) {
                        var act = activitiesStorage.find(v => v.id == item.id)
                        if (act) {
                            act.pick_up_date = (this.state.dates_service && this.state.dates_service['service_' + item.id]) ? this.state.dates_service['service_' + item.id]['date'] : ''
                            act.time = (this.state.dates_service && this.state.dates_service['service_' + item.id]) ? this.state.dates_service['service_' + item.id]['time'] : ''
                            item.selected = true
                            services.push(act)
                        } else {
                            item.pick_up_date = ''
                            item.selected = false
                            services.push(item)
                        }
                    } else {
                        item.pick_up_date = ''
                        item.selected = false
                        services.push(item)
                    }
                })
                this.setState({ services: services })
            })
        })
    }

    listCities() {
        cityService.getAll({ status: 1, page_size: 20 }).then((response) => {
            const data = [...response.data.data.results]
            const cities = data.map((city) => {
                city.selected = (this.state.selectedCity && this.state.selectedCity.id == city.id)
                return city
            })
            this.setState({ cities: cities })
        })
    }

    handleClose() {
        this.props.navigation.pop()
    }

    handleSave() {
        const validate = 0
        const { nro_adults, nro_hab, hotel, services, selectedCity, selected_method_payments, selected_type_payments } = this.state
        const countReservations = services.filter(v => moment(v.pick_up_date, 'YYYY-MM-DD').isValid())
        const self = this

        if (services.length == 0 || services.length != countReservations.length || selectedCity == null || selected_method_payments == "" || selected_type_payments == "") {
            globalHelper.showMessageAlert('No es posible registrar!', 'Faltan datos!', null)
        } else {
            let total = 0
            this.setState({ isLoading: true, messageLoading: "Registrando ..." }, () => {
                let activities = []

                services.map((service) => {
                    let act = {
                        price_pub_adult: this.parseTotalDivisa(service.price_pub_adult),
                        price_pub_child: this.parseTotalDivisa(service.price_pub_child),
                        price_rep_adult: this.parseTotalDivisa(service.price_rep_adult),
                        price_rep_child: this.parseTotalDivisa(service.price_rep_child),
                        pick_up_date: service.pick_up_date + " 00:00:00",
                        pickup_id: service.pickup_id,
                        activity_id: service.id,
                        order_id: (service.order_id !== undefined && service.order_id !== null) ? service.order_id : ""
                    }
                    activities.push(act)
                    total = total + this.state.nro_adults * service.price_rep_adult
                    total = total + ((this.state.nro_childs > 0) ? this.state.nro_childs * service.price_rep_child : 0)
                    //total = this.parseTotalDivisa(total)
                })

                console.log("joper30 register", this.userSession)

                let data = {
                    order: {
                        city_id: selectedCity.id,
                        hotel_id: (hotel !== null) ? hotel.id : null,
                        operator_id: this.state.catalog.operator_id,
                        name_client: this.state.name_client,
                        email_client: this.state.email_client,
                        phone_client: (this.state.number_area_select).trim() + " " + (this.state.phone_client).trim(),
                        address_client: this.state.address_client,
                        nro_adults: this.state.nro_adults,
                        nro_babys: this.state.nro_babys,
                        nro_childs: this.state.nro_childs,
                        nro_hab: (this.state.nro_hab == "") ? 0 : this.state.nro_hab,
                        total: this.state.total,
                        promotor_id: this.userSession.id,
                        method_payment: this.state.selected_method_payments,
                        total_payment: (this.state.selected_type_payments == "deposito") ? this.state.total_payment : total,
                        type_payment: this.state.selected_type_payments,
                        money_id: this.state.money
                    },
                    activities: activities,
                    activities_deleted: this.servicesRemove,
                    country_client: this.state.selectedCountry.id
                }


                if (this.state.typeForm == "register") {
                    this.registerOrder(data)
                } else {
                    this.updateOrder(data)
                }
            })
        }
    }

    registerOrder(data) {
        orderService.register(data).then((response) => {
            this.setState({ isLoading: false, reservationComplete: true, dates_service: {}, messageLoading: "Cargando ..." })
            this.refs.toast.show('Su orden ha sido enviada, espere su confirmación', 3000)

            console.log("joper30 registerOrder", response.data.data)
            const order = response.data.data
            const dataPaypal = globalHelper.convertParamsUrlByObject({
                nameItem: "Behlaak Nro Operación :" + order.code_internal,
                sku: order.code_internal,
                price: order.total,
                description: "Behlaak Nro Operación :" + order.code_internal,
                currency: "USD"
            })

            const urlPaypal = Config.URL_PAYMENTS + "/paypal?" + dataPaypal

            this.paypalData = { url: urlPaypal }

            if (this.state.selected_method_payments == "paypal") {
                this.setState({ showPaypal: true })
            } else {
                setTimeout(() => {
                    this.props.navigation.navigate('Catalog', { userSession: this.userSession })
                }, 2500)
            }

        }).catch((error) => {
            this.setState({ isLoading: false, messageLoading: "Cargando ..." }, () => {
                console.log("joper30 registerOrder", error)
                httpService.showError(error)
            })
        })
    }

    updateOrder(data) {
        orderService.update(this.orderId, data)
            .then((response) => {
                this.setState({ isLoading: false, reservationComplete: true, dates_service: {}, messageLoading: "Cargando ..." })
                this.refs.toast.show('Su orden ha sido actualizada', 3000)
                setTimeout(() => {
                    this.props.navigation.pop()
                }, 3000)
            })
            .catch((error) => {
                this.setState({ isLoading: false, messageLoading: "Cargando ..." })
                let messageError = httpService.parseMessageError(error)
                this.refs.toast.show(messageError, 3000)
            })
    }

    _removeService = (service) => {
        if (this.state.services.length > 1) {
            globalHelper.showMessageConfimrAlert('Eliminar', 'Seguro de eliminar servicio ' + service.name, "Si", "No", () => {
                let services = [...this.state.services]
                const index = services.findIndex(v => v.id == service.id)
                if (index != -1) {
                    const existRemoveService = this.servicesRemove.indexOf(service.id)
                    if (existRemoveService == -1 && this.orderId !== null) {
                        this.servicesRemove.push(service.id)
                    }

                    services.splice(index, 1)
                    this.setState({ services: services }, () => {
                        this.flatListRef.scrollToIndex({ animated: true, index: 0 })
                    })
                }
            }, null)
        } else {
            globalHelper.showMessageAlert('Eliminar', 'No se puede seguir eliminando', null)
        }
    }

    _showModalPickup(activity) {
        if (activity.pick_up_date == "") {
            globalHelper.showMessageAlert("Aviso", "Necesitas seleccionar una fecha", null)
        } else {
            this.setState({ service_selected_date: activity }, () => {
                let navigate = (this.state.typeForm == "register") ? "PickUp" : "DealsPickUp"
                this.props.navigation.navigate(navigate, { activity: activity })
            })
        }
    }

    _hideModalPickup() {
        this.setState({ isModalPickupVisible: false, service_selected_date: null })
    }

    _showDateTimePicker(service) {
        this.setState({ isDateTimePickerVisible: true, service_selected_date: service })
    }

    _hideDateTimePicker = () => {
        this.setState({ isDateTimePickerVisible: false, service_selected_date: null })
    }

    _handleDatePicked = (date) => {
        const services = [... this.state.services]
        const pick_up = moment(date).format("YYYY-MM-DD")
        const date_now = moment().format("YYYY-MM-DD")

        if (moment(pick_up).isSameOrAfter(date_now)) {
            var dates_service = { ...this.state.dates_service }
            services.filter((item) => this.state.service_selected_date.id === item.id).map((item) => {
                item.pick_up_date = pick_up
                dates_service['service_' + item.id] = {
                    date: item.pick_up_date,
                    time: item.time
                }

                return item
            })

            this.setState({ isLoading: true }, () => {
                activityService.getAvailability(this.state.service_selected_date.id, { date: pick_up }).then(
                    (response) => {
                        const capacity = parseInt(response.data.data.max_capacity_day)
                        const totalOrders = parseInt(response.data.data.total_orders)
                        const available = capacity - totalOrders

                        if (totalOrders < capacity) {
                            this.setState({ services: services, dates_service: dates_service, isLoading: false })
                            this.refs.toast.show('Hay ' + available + " pax disponibles")
                        } else {
                            this.setState({ isLoading: false })
                            this.refs.toast.show('Lo sentimos no hay disponibilidad')
                        }
                    },
                    (error) => {
                        this.setState({ isLoading: false })
                        this.refs.toast.show('Ocurrio un error al conectar con el servidor')
                    }
                )
                this._hideDateTimePicker()
            })
        } else {
            globalHelper.showMessageAlert("Alerta!", "No se puede reservar para una fecha anterior al dia actual", null)
            this._hideDateTimePicker()
        }
    }

    _handlePickup = (pickup) => {
        const services = [... this.state.services]
        var dates_service = { ...this.state.dates_service }
        if (pickup.time == "") {
            globalHelper.showMessageAlert("Alerta!", "No hay atención para esta fecha", null)
        } else {

            services.filter((item) => this.state.service_selected_date.id === item.id).map((item) => {
                item.time = pickup.time
                item.address = pickup.address
                item.pickup_id = pickup.id
                dates_service['service_' + item.id] = {
                    date: item.pick_up_date,
                    time: item.time,
                    address: item.address
                }
                return item
            })
            this.setState({ services: services, dates_service: dates_service, service_selected_date: null })
        }

        //this._hideModalPickup()
    }

    handleSelectButtonsTab(selectedIndex) {
        console.log("joper30 selectedCountry", this.state.selectedCountry, this.state.number_area_select)
        switch (selectedIndex) {
            case 0:
                this.setState({ selectedIndex: selectedIndex })
                break
            case 1:
                const countPickUps = this.state.services.filter(v => moment(v.pick_up_date, 'YYYY-MM-DD').isValid())
                const countPickUpsTime = this.state.services.filter(v => v.pickup_id !== undefined && v.pickup_id !== "")
                const countServices = this.state.services.length

                if (countServices == countPickUps.length && countServices == countPickUpsTime.length) {
                    this.setState({ selectedIndex: selectedIndex })
                } else {
                    globalHelper.showMessageAlert("Alerta!", "Necesita agregar datos de PickUp", null)
                }

                break
            case 2:
                if (this.state.selectedIndex == 1 && ((this.state.name_client == "" || this.state.email_client == "" || this.state.phone_client == "") || (this.state.nro_adults == 0 && this.state.nro_childs == 0))) {
                    globalHelper.showMessageAlert("Alerta!", "Necesita agregar datos del Pax", null)
                } else if (this.state.selectedCountry == null) {

                    globalHelper.showMessageAlert("Alerta!", "Necesita seleccionar el código de área", null)
                } else {
                    this.setState({ selectedIndex: selectedIndex })
                }

                break
            case 3:
                if (this.state.selectedIndex == 2 && (this.state.address_client == "" && this.state.hotel == null)) {
                    globalHelper.showMessageAlert("Alerta!", "Necesita agregar hotel u Dirección del cliente", null)
                } else {
                    this.setState({ selectedIndex: selectedIndex })
                }

                break
        }

        this.getTotal()
    }

    handleAddHotel() {
        let navigate = (this.orderId == null) ? "ServiceHotel" : "DealsServiceHotel"
        this.props.navigation.push(navigate, { orderId: this.orderId })
    }

    handleVerifyPhone(phone_client) {
        this.setState({ phone_client: phone_client })

        let data = {
            phone: this.state.number_area_select + phone_client
        }

        if (this.state.selectedCountry === null) {
            this.refs.toast.show('Necesita seleccionar el código de área', 2000)
        } /* else {
            if ((data.phone).length > 8) {
                clientService.verifyPhone(data)
                    .then((response) => {
                        this.refs.toast.show('El teléfono es válido', 2000)
                        this.setState({ verifyPhone: true })
                    })
                    .catch((error) => {
                        this.refs.toast.show('El teléfono es inválido', 2000)
                        this.setState({ verifyPhone: false })
                    })
            }
        } */
    }

    updatePriceService(service, value, option) {
        let services = [... this.state.services]
        services.filter((item) => item.id == service.id).map((item) => {
            if (option == "adult") {
                item.price_pub_adult = value
            } else {
                item.price_pub_child = value
            }

            return item
        })
        this.setState({ services: services })
    }

    handleSelectService(service) {
        const services = [... this.state.services]
        services.filter((item) => service.id === item.id).map((item) => {
            item.selected = !item.selected
            return item
        })
        this.setState({ services: services }, () => {
            this.getTotal()
        })
    }

    handleSelectCity(city) {
        //this.setState({ isLoading: true })
        this.setState({ selectedCity: city })
        const cities = this.state.cities.map((item) => {
            item.selected = (city.id == item.id) ? true : false
            return item
        })
        this.setState({ cities: cities })
    }

    _dropdown_2_renderSeparator(sectionID, rowID, adjacentRowHighlighted) {
        if (rowID == this.state.countries.length - 1) return
        let key = `spr_${rowID}`
        return (<View style={styles.dropdown_2_separator}
            key={key}
        />)
    }

    _dropdown_2_renderButtonText(rowData) {
        const { number_area } = rowData
        return number_area
    }

    _dropdown_2_renderRow(rowData, rowID, highlighted) {
        let evenRow = rowID % 2
        return (

            <TouchableHighlight underlayColor='cornflowerblue'>
                <View style={[styles.dropdown_2_row, highlighted && { backgroundColor: 'lemonchiffon' }]}>
                    <Flag
                        code={(rowData.iso).toUpperCase()}
                        size={32}
                    />
                    <Text style={[styles.dropdown_2_row_text, highlighted && { color: 'mediumaquamarine' }]}>
                        {rowData.number_area}
                    </Text>
                </View>
            </TouchableHighlight>
        )
    }

    changeMoney(money) {
        let operator = this.state.catalog.operator
        let divisa = this.state.moneyList.filter((item) => item.id == money && item.id !== operator.money_id)
        let simbol = (divisa.length > 0) ? divisa[0].money_b.simbol : operator.money.simbol
        this.setState({ moneySimbol: simbol, money: money }, () => {
            this.getTotal()
        })
    }

    getTotal() {
        const { nro_adults, nro_hab, hotel, services, selectedCity } = this.state
        //const servicesSelected = services

        let total = 0
        let totalAdults = 0
        let totalChilds = 0
        let total_payment = 0
        let balance = 0

        services.map((service) => {
            total = total + this.state.nro_adults * service.price_pub_adult
            total = total + ((this.state.nro_childs > 0) ? this.state.nro_childs * service.price_pub_child : 0)
            totalAdults = totalAdults + ((this.state.nro_adults > 0) ? this.state.nro_adults * service.price_pub_adult : 0)
            totalChilds = totalChilds + ((this.state.nro_childs > 0) ? this.state.nro_childs * service.price_pub_child : 0)
        })

        total = this.parseTotalDivisa(total)

        let totalServices = services.length

        total_payment = (this.state.selected_type_payments == "deposito") ? this.state.total_payment : 0
        balance = (this.state.selected_type_payments == "deposito") ? total - this.state.total_payment : 0
        balance = (this.state.total_payment == "" || this.state.total_payment == 0) ? 0 : balance
        balance = balance.toFixed(2)

        this.setState({ total: total, total_adults: totalAdults, total_childs: totalChilds, total_payment: total_payment, balance: balance })
    }

    parseTotalDivisa(total) {
        let totalParse = total
        if (this.state.catalog.operator.money_id !== this.state.money) {
            let divisa = this.state.moneyList.filter((item) => item.id == this.state.money)
            let moneyA = parseFloat(divisa[0].money_a_value)
            let moneyB = parseFloat(divisa[0].money_b_value)
            totalParse = (moneyA > moneyB) ? total / moneyB : total * moneyB
        }
        totalParse = parseFloat(totalParse).toFixed(2)
        return totalParse
    }

    renderNumberArea() {
        let numberAreas = []

        //numberAreas.push(<Picker.Item label="Cod. área" value="0" key={"area-0001"} />)

        this.state.countries.map((country, index) => {
            let labelArea = country.number_area + " " + country.iso
            numberAreas.push(<Picker.Item label={labelArea} value={country} key={"area-" + index} />)
        })

        return numberAreas
    }

    renderCards() {
        const { height, width } = Dimensions.get('window')
        const widthCardService = (width - 32)

        if (this.state.selectedIndex == 0) {
            return <View style={{ flex: 1, padding: 8 }}>
                <FlatList
                    ref={(ref) => { this.flatListRef = ref; }}
                    data={this.state.services}
                    renderItem={({ item, index }) => <Card containerStyle={{ flex: 1, margin: 8, marginBottom: 8, shadowOpacity: 0.75, shadowRadius: 5, shadowColor: '#081257', elevation: 4, position: 'relative', width: widthCardService }} >
                        <View
                            onPress={item.onPress}
                            style={{ height: 240, width: 'auto', borderTopLeftRadius: 8, borderTopRightRadius: 8, backgroundColor: '#CCC' }}>
                            <Image source={{ uri: item.poster }} style={{ height: '100%', width: '100%', borderTopLeftRadius: 8, borderTopRightRadius: 8 }} resizeMode={'cover'} />
                            <View style={{ position: 'absolute', top: 8, left: 8 }} >
                                <Text bold size={18} color={'#FFF'} style={serviceStyle.textShadowDark} >{index + 1}/{this.state.services.length}</Text>
                            </View>
                            <View style={{ position: 'absolute', top: 8, right: 0 }} >
                                <HeaderButton icon='md-trash' typeicon='ionicon' onPress={() => this._removeService(item)} color="red" />
                            </View>
                            <View style={{ position: 'absolute', bottom: 8, right: 8 }} >
                                <Text bold size={24} color={'#FFF'} style={serviceStyle.textShadowDark}>{'$' + (this.state.nro_adults * item.price_pub_adult + this.state.nro_childs * item.price_pub_child).toFixed(2)}</Text>
                            </View>
                        </View>

                        <View style={{ padding: 8 }}>

                            <Text style={{ color: '#000', fontFamily: "RobotoCondensed-Bold", fontSize: 18, textAlign: 'center' }} numberOfLines={1}>{item.name}</Text>

                            <ListItem
                                leftIcon={(<Icon name="md-person" type="ionicon" size={14} color="#000040" />)}
                                subtitle={(<Text style={{ color: "#000040", fontSize: 11 }}>  ${item.price_rep_adult}</Text>)}
                                rightIcon={(
                                    <View style={{ flexDirection: 'row' }}>
                                        <TextInputForm
                                            placeholder={'Precio venta'}
                                            value={item.price_pub_adult}
                                            onChangeText={(value) => this.updatePriceService(item, value, "adult")}
                                            style={[styles.inputPrice, (parseFloat(item.price_pub_adult) >= parseFloat(item.price_rep_adult)) ? {} : styles.inputPriceError]}
                                            keyboardType="numeric"
                                        />
                                        <Text bold style={{ color: "#000040", width: 20, textAlign: 'right' }}> {this.state.nro_adults} </Text>
                                        <Text bold style={{ color: "#000040", width: 80, textAlign: 'right' }}> {'$' + (this.state.nro_adults * item.price_pub_adult).toFixed(2)} </Text>
                                    </View>
                                )}
                                containerStyle={{ paddingLeft: 0, paddingRight: 0 }}
                            />
                            <ListItem
                                leftIcon={(<Icon name="md-happy" type="ionicon" size={14} color="#000040" />)}
                                subtitle={(<Text style={{ color: "#000040", fontSize: 11 }}>  ${item.price_rep_child}</Text>)}
                                rightIcon={(
                                    <View style={{ flexDirection: 'row' }}>
                                        <TextInputForm
                                            placeholder={'Precio venta'}
                                            value={item.price_pub_child}
                                            onChangeText={(value) => this.updatePriceService(item, value, "child")}
                                            style={[styles.inputPrice, (parseFloat(item.price_pub_child) >= parseFloat(item.price_rep_child)) ? {} : styles.inputPriceError]}
                                            keyboardType="numeric"
                                        />
                                        <Text bold style={{ color: "#000040", width: 20, textAlign: 'right' }}> {this.state.nro_childs} </Text>
                                        <Text bold style={{ color: "#000040", width: 80, textAlign: 'right' }}> {'$' + (this.state.nro_childs * item.price_pub_child).toFixed(2)} </Text>
                                    </View>
                                )}
                                containerStyle={{ paddingLeft: 0, paddingRight: 0 }}
                            />
                        </View>

                        {moment(item.pick_up_date, 'YYYY-MM-DD').isValid() && item.time &&
                            <View style={{ paddingHorizontal: 8, alignItems: 'center', marginBottom: 8 }}>
                                <Text style={{ fontFamily: "RobotoCondensed-Bold" }}>{item.address}</Text>
                            </View>}

                        <View style={{ flexDirection: 'row', padding: 8 }}>
                            <View style={{ flex: 1 }}>
                                <TouchableOpacity onPress={this._showDateTimePicker.bind(this, item)} style={styles.buttonPickUps}>

                                    <Text style={{ color: "#000040", width: 100, fontSize: 12, textAlign: 'center' }} fontFamily="RobotoCondensed-Bold">{item.pick_up_date !== "" ? item.pick_up_date : 'Fecha'}</Text>

                                </TouchableOpacity>
                            </View>

                            <View style={{ flex: 1 }}>
                                <TouchableOpacity onPress={this._showModalPickup.bind(this, item)} >
                                    <View style={styles.buttonPickUps}>
                                        <Text style={{ color: "#000040", width: 100, fontSize: 12, textAlign: 'center' }} fontFamily="RobotoCondensed-Bold">{item.time ? item.time : 'Pickup'}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>

                        </View>
                        <View style={{ flex: 1, paddingHorizontal: 8 }}>
                            <Text style={{ color: "#000040", fontSize: 14 }} fontFamily="RobotoCondensed-Bold">Descripción</Text>
                            <Text style={{ color: "#000040", fontSize: 12 }} fontFamily="RobotoCondensed-Light">{(item.description && item.description !== null) ? item.description : ""}</Text>
                        </View>

                    </Card>}

                    keyExtractor={(item, index) => item.id}
                    horizontal={true}
                    showsHorizontalScrollIndicator={false}
                    //onRefresh={this.handleRefreshCatalog.bind(this)}
                    refreshing={this.state.isRefresh}
                //numColumns={2}

                />

                <View style={{ marginTop: 8, paddingHorizontal: 8 }}>
                    <Button title="CONTINUAR" onPress={this.handleSelectButtonsTab.bind(this, 1)} buttonStyle={{ backgroundColor: "#00add2" }} />
                </View>
            </View>
        } else if (this.state.selectedIndex == 1) {
            return <View style={{ flex: 1 }}>
                <Card containerStyle={{ flex: 1, margin: 8, marginBottom: 8, shadowOpacity: 0.75, shadowRadius: 5, shadowColor: '#081257', elevation: 4, width: 'auto' }} >
                    <View style={{ paddingHorizontal: 16, paddingTop: 16 }}>
                        <View>
                            <Text bold fontFamily="RobotoCondensed-Bold">Nombre</Text>
                            <TextInputForm
                                placeholder={'Escribir nombre'}
                                value={this.state.name_client}
                                onChangeText={(name_client) => this.setState({ name_client })}
                            />
                        </View>

                        <View>
                            <Text bold fontFamily="RobotoCondensed-Bold">Correo</Text>
                            <TextInputForm
                                placeholder={'Escribir correo'} keyboardType='email-address'
                                value={this.state.email_client}
                                onChangeText={(email_client) => this.setState({ email_client })}
                            />
                        </View>
                        <View>
                            <Text bold style={{ fontFamily: "RobotoCondensed-Bold" }} >Teléfono</Text>
                        </View>

                        <View style={{
                            flexDirection: 'row',
                            flex: 1
                        }} >
                            <View style={{ width: '40%' }}>
                                <Picker
                                    selectedValue={this.state.selectedCountry}
                                    style={{ height: 50, width: 110 }}
                                    onValueChange={(value, itemIndex) =>
                                        this.setState({ number_area_select: value.number_area, selectedCountry: value })
                                    }>

                                    {this.renderNumberArea()}

                                </Picker>
                                {/*   <ModalDropdown options={this.state.countries} style={styles.dropdown_2}
                                    ref="dropdown_2"
                                    defaultValue="Código área"
                                    textStyle={styles.dropdown_2_text}
                                    onSelect={(id, value) => this.setState({ number_area_select: value.number_area, selectedCountry: value })}
                                    dropdownStyle={styles.dropdown_2_dropdown}
                                    renderButtonText={(rowData) => this._dropdown_2_renderButtonText(rowData)}
                                    renderRow={this._dropdown_2_renderRow.bind(this)}
                                    renderSeparator={(sectionID, rowID, adjacentRowHighlighted) => this._dropdown_2_renderSeparator(sectionID, rowID, adjacentRowHighlighted)}
                                /> */}
                            </View>
                            <View style={{ width: '60%' }}>
                                <TextInputForm
                                    placeholder={'Teléfono'} keyboardType='phone-pad'
                                    value={this.state.phone_client}
                                    onChangeText={(phone_client) => this.handleVerifyPhone(phone_client)}
                                />
                            </View>
                        </View>
                    </View>

                    <View style={{ marginBottom: 16 }}>
                        <ListItem
                            leftIcon={(
                                <Image style={{ width: 30, marginRight: 16 }} resizeMode={'cover'} source={images.Adult} />
                            )}
                            subtitle={(
                                <Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#000040", marginTop: 8 }}>Adultos: +12 años </Text>
                            )}
                            rightIcon={(
                                <InputNumber value={this.state.nro_adults} onChange={(nro_adults) => { this.setState({ nro_adults }) }} style={{ width: 100 }} />
                            )}
                            containerStyle={{ paddingLeft: 8, borderBottomWidth: 0, paddingRight: 16 }}
                        />

                        <ListItem
                            leftIcon={(
                                <Image style={{ width: 30, marginRight: 16 }} resizeMode={'cover'} source={images.Menor} />
                            )}
                            subtitle={(
                                <Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#000040", marginTop: 8 }}>Menor : 5 - 11 años</Text>
                            )}
                            rightIcon={(
                                <InputNumber value={this.state.nro_childs} onChange={(nro_childs) => { this.setState({ nro_childs }) }} style={{ width: 100 }} />
                            )}
                            containerStyle={{ paddingLeft: 8, borderBottomWidth: 0, paddingRight: 16 }}
                        />

                        <ListItem
                            leftIcon={(
                                <Image style={{ width: 30, marginRight: 16 }} resizeMode={'cover'} source={images.Infante} />
                            )}
                            subtitle={(
                                <Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#000040", marginTop: 8 }}>Infante : 0 - 4 años</Text>
                            )}
                            rightIcon={(
                                <InputNumber value={this.state.nro_babys} onChange={(nro_babys) => { this.setState({ nro_babys }) }} style={{ width: 100 }} />
                            )}
                            containerStyle={{ paddingLeft: 8, borderBottomWidth: 0, paddingRight: 16 }}
                        />
                    </View>

                    <View style={{ paddingHorizontal: 16 }}>
                        <Button title="CONTINUAR" onPress={this.handleSelectButtonsTab.bind(this, 2)} buttonStyle={{ backgroundColor: "#00add2" }} />
                    </View>

                </Card>

            </View>

        } else if (this.state.selectedIndex == 2) {
            return <View style={{ flex: 1 }}>
                {/** CARD ORDER SELECTION */}
                <Card containerStyle={{ flex: 1, margin: 8, marginBottom: 8, shadowOpacity: 0.75, shadowRadius: 5, shadowColor: '#081257', elevation: 4, width: 'auto' }} >
                    {/*<View style={{ height: 240, width: 'auto', borderTopLeftRadius: 8, borderTopRightRadius: 8, backgroundColor: '#CCC' }}>
                    <Image source={{ uri: this.state.catalog.operator.poster }} style={{ height: '100%', width: '100%', borderTopLeftRadius: 8, borderTopRightRadius: 8 }} resizeMode={'cover'} />
                </View>*/}
                    <View style={{ padding: 8 }}>
                        <Text style={{ color: '#000', fontFamily: "RobotoCondensed-Bold", fontSize: 18, textAlign: 'center', marginBottom: 16 }} numberOfLines={1}>Operador :: {this.state.catalog.operator.name}</Text>

                        <View>
                            <Text bold style={{ fontFamily: "RobotoCondensed-Bold" }} >Ciudad</Text>
                            <TextInputForm
                                placeholder={'Seleccionar'} editable={false}
                                value={this.state.selectedCity == null ? "" : this.state.selectedCity.name}
                                style={{ backgroundColor: '#ccc' }}
                            />
                        </View>
                        <View>
                            <Text bold style={{ fontFamily: "RobotoCondensed-Bold" }} >Seleccionar Hotel</Text>
                            <TouchableOpacity onPress={this.handleAddHotel.bind(this)} >
                                <View style={styles.inputButton}>
                                    <Text>{this.state.hotel == null ? "Hotel" : this.state.hotel.name}</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View>
                            <Text bold style={{ fontFamily: "RobotoCondensed-Bold" }} >Numero de habitación</Text>
                            <TextInputForm
                                placeholder={'Número'} keyboardType={'numeric'}
                                value={this.state.nro_hab}
                                onChangeText={(nro_hab) => this.setState({ nro_hab })}
                            />
                        </View>
                        <View>
                            <Text bold style={{ fontFamily: "RobotoCondensed-Bold" }} >Otra Dirección (Opcional si no selecciona Hotel)</Text>
                            <TextInputForm
                                placeholder={'Dirección'} keyboardType={'default'}
                                value={this.state.address_client}
                                onChangeText={(address_client) => this.setState({ address_client })}
                            />
                        </View>
                        <Button
                            title="CONTINUAR"
                            onPress={this.handleSelectButtonsTab.bind(this, 3)}
                            buttonStyle={{ backgroundColor: "#00add2" }}
                        />
                    </View>
                </Card>
                {/** CARD ORDER SELECTION */}
            </View>
        } else if (this.state.selectedIndex == 3) {
            return <View style={{ flex: 1, padding: 8 }}>

                <Card containerStyle={{ flex: 1, margin: 8, marginBottom: 8, shadowOpacity: 0.75, shadowRadius: 5, shadowColor: '#081257', elevation: 4, width: 'auto' }} >
                    <View style={{ flexDirection: 'row', paddingTop: 16, paddingBottom: 16, borderRadius: 8 }}>
                        <View style={{ alignItems: 'center', flex: 1 }}>
                            <Text light style={{ color: "#000", fontSize: 13 }}>  TOTAL</Text>
                            <Text bold style={{ color: "#081257", fontSize: 17 }}> {this.state.moneySimbol} {this.state.total}</Text>
                        </View>
                        <View style={{ alignItems: 'center', flex: 1 }}>
                            <Text light style={{ color: "#000", fontSize: 13 }}>  BALANCE</Text>
                            <Text bold style={{ color: "#081257", fontSize: 17 }}> {this.state.moneySimbol}  {this.state.balance}</Text>
                        </View>
                        <View style={{ alignItems: 'center', flex: 1 }}>
                            <Text light style={{ color: "#000", fontSize: 13 }}>  SERVICIOS</Text>
                            <Text bold style={{ color: "#081257", fontSize: 17 }}> {this.state.services.length}</Text>
                        </View>
                    </View>
                </Card>
                <Card containerStyle={{ flex: 1, margin: 8, marginBottom: 8, paddingTop: 16, shadowOpacity: 0.75, shadowRadius: 5, shadowColor: '#081257', elevation: 4, width: 'auto' }} >

                    <ListItem
                        subtitle={(
                            <Text bold style={{ color: "#000040", fontFamily: "RobotoCondensed-Bold" }}> {this.state.nro_adults} {(this.state.nro_adults == 1) ? "Adulto" : "Adultos"} </Text>
                        )}
                        rightIcon={(
                            <Text bold style={{ color: "#000040", fontSize: 15, fontFamily: "RobotoCondensed-Bold" }}>  $ {this.state.total_adults} </Text>
                        )}
                        containerStyle={{ paddingLeft: 8, borderBottomWidth: 0, paddingRight: 16 }}
                    />

                    <ListItem
                        subtitle={(
                            <Text bold style={{ color: "#000040", fontFamily: "RobotoCondensed-Bold" }}> {this.state.nro_childs} {(this.state.nro_childs == 1) ? "Menor" : "Menores"} </Text>
                        )}
                        rightIcon={(
                            <Text bold style={{ color: "#000040", fontSize: 15, fontFamily: "RobotoCondensed-Bold" }}>  $ {this.state.total_childs}</Text>
                        )}
                        containerStyle={{ paddingLeft: 8, borderBottomWidth: 0, paddingRight: 16 }}
                    />

                    <View style={{ paddingHorizontal: 16 }}>

                        <Dropdown
                            containerStyle={{ padding: 0, margin: 0 }}
                            pickerStyle={{ padding: 0, margin: 0 }}
                            label='Seleccionar moneda'
                            labelTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            data={this.state.moneyList.map((item) => {
                                item.value = item.id
                                item.label = item.name
                                return item
                            })}
                            itemTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            affixTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            titleTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            style={{ fontFamily: "RobotoCondensed-Bold" }}
                            onChangeText={(money) => this.changeMoney(money)}
                            value={this.state.money}
                        />

                        <Dropdown
                            containerStyle={{ padding: 0, margin: 0 }}
                            pickerStyle={{ padding: 0, margin: 0 }}
                            label='Metódo de Pago'
                            labelTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            data={this.state.payments}
                            itemTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            affixTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            titleTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            style={{ fontFamily: "RobotoCondensed-Bold" }}
                            onChangeText={(selected_method_payments) => this.setState({ selected_method_payments })}
                            value={this.state.selected_method_payments}
                        />

                        <Dropdown
                            containerStyle={{ padding: 0, margin: 0 }}
                            pickerStyle={{ padding: 0, margin: 0 }}
                            label='Total Pago'
                            labelTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            data={this.state.type_payment}
                            itemTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            affixTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            titleTextStyle={{ fontFamily: "RobotoCondensed-Bold" }}
                            style={{ fontFamily: "RobotoCondensed-Bold" }}
                            onChangeText={(selected_type_payments) => this.setState({ selected_type_payments })}
                            value={this.state.selected_type_payments}
                        />

                        {this.state.selected_type_payments === "deposito" && (
                            <View style={{ marginTop: 8 }}>
                                <TextInputForm
                                    placeholder="Ingresa aqui el depósito"
                                    keyboardType='phone-pad'
                                    value={this.state.total_payment}
                                    onChangeText={(total_payment) => this.setState({ total_payment })}
                                    onEndEditing={this.getTotal.bind(this)}
                                />
                            </View>
                        )}

                        <View style={{ marginTop: 8 }}>
                            <Button title="CONTINUAR" onPress={this.handleSave.bind(this)} buttonStyle={{ backgroundColor: "#00add2" }} />
                        </View>

                    </View>
                </Card>

            </View>
        }
    }

    render() {
        const buttons = ['Servicios', 'Pax', 'Orden', 'Total']
        const { selectedIndex } = this.state

        return (

            <View style={{ flex: 1, backgroundColor: "#f8f8fc" }}>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />

                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>
                    <Header
                        transparent
                        leftComponent={(
                            <HeaderButton icon="md-arrow-back" color="white" typeicon="ionicon" onPress={this.handleClose.bind(this)} />
                        )}
                        centerComponent={<HeaderTitle>Orden de Servicio</HeaderTitle>}
                        rightComponent={(
                            <View style={{ width: 48 }} />
                        )}
                    />
                </ImageBackground>

                <View style={{ paddingLeft: 0, paddingRight: 0, marginLeft: 0 }}>
                    <ButtonGroup
                        selectedIndex={selectedIndex}
                        textStyle={{ fontFamily: "RobotoCondensed-Bold", color: "#000036", fontSize: 17 }}
                        buttons={buttons}
                        onPress={this.handleSelectButtonsTab.bind(this)}
                        containerStyle={{ height: 65, borderWidth: 0, backgroundColor: '#f8f8fc', borderColor: "#f8f8fc" }}
                        selectedButtonStyle={{ borderBottomColor: "#a3a3dc", borderBottomWidth: 2, backgroundColor: "#f8f8fc" }}
                        innerBorderStyle={{ borderWidth: 0, width: 0, color: '#f8f8fc' }}
                    />
                </View>
                <View style={{ flex: 1 }}>
                    <ScrollView>
                        {this.renderCards()}
                    </ScrollView>
                </View>

                <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                />

                <OrientationLoadingOverlay
                    visible={this.state.isLoading}
                    color="white"
                    indicatorSize="large"
                    messageFontSize={18}
                    message={this.state.messageLoading}
                />
                <Toast ref="toast" position='top' positionValue={200} />

                <Modal
                    animationType="fade"
                    visible={this.state.showPaypal}
                    onRequestClose={() => this.setState({ showPaypal: false })}
                    transparent={true}
                >

                    <WebView source={{ uri: this.paypalData.url }}></WebView>

                </Modal>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    row: {
        flex: 1,
        flexDirection: 'row',
    },
    cell: {
        flex: 1,
        borderWidth: StyleSheet.hairlineWidth,
    },
    scrollView: {
        flex: 1,
    },
    contentContainer: {
        height: 500,
        paddingVertical: 100,
        paddingLeft: 20,
    },
    textButton: {
        color: 'deepskyblue',
        borderWidth: StyleSheet.hairlineWidth,
        borderColor: 'deepskyblue',
        margin: 2,
    },

    dropdown_1: {
        flex: 1,
        top: 32,
        left: 8,
    },
    dropdown_2: {
        width: 110,
        height: 44,
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#081257'
    },
    dropdown_2_text: {
        marginVertical: 10,
        marginHorizontal: 6,
        fontSize: 14,
        color: 'black',
        textAlign: 'center',
        textAlignVertical: 'center',
        fontFamily: 'RobotoCondensed-Regular'
    },
    dropdown_2_dropdown: {
        width: 150,
        height: 300,
        borderColor: 'cornflowerblue',
        borderWidth: 2,
        borderRadius: 3,
    },
    dropdown_2_row: {
        flexDirection: 'row',
        height: 40,
        alignItems: 'center',
    },
    buttonPickUps: {
        height: 44,
        paddingHorizontal: 16,
        backgroundColor: '#FFF',
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#081257',
        marginBottom: 8,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1
    },
    inputButton: {
        height: 44,
        paddingHorizontal: 16,
        backgroundColor: '#FFF',
        marginBottom: 16,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#081257',
        justifyContent: 'center'
    },
    inputPrice: {
        width: 110
    },
    inputPriceError: {
        color: 'red',
        borderColor: 'red',
        borderWidth: 1,
    }
})

