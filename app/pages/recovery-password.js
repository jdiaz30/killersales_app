import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, StatusBar, Modal, Alert, TextInput } from 'react-native'
import { Button, FormLabel, FormInput, FormValidationMessage, Card } from 'react-native-elements'

import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'


import * as authService from '../services/auth-service'
import * as globalHelper from '../helpers/global-helper'

export default class RecoveryPasswordModal extends Component {
    constructor(props) {
        super(props)

        this.state = {
            correo: "",
            isLoading: false
        }
    }

    static navigationOptions = {
        title: '',
        header: null
    }

    handleReserPassword() {
        let validateMail = globalHelper.validateEmail(this.state.correo)
        if (validateMail) {
            this.setState({ isLoading: true }, () => {
                authService.recoveryPassword(this.state.correo).then((response) => {
                    this.setState({ isLoading: false })
                    Alert.alert(
                        'Éxito!',
                        'Revise su bandeja de entrada',
                        [
                            {
                                text: 'OK', onPress: () => {
                                    this.props.onClose()
                                }
                            }
                        ],
                        { cancelable: false }
                    )
                }).catch((error) => {
                    this.setState({ isLoading: false })
                    Alert.alert(
                        'Alerta!',
                        error.response.data.user_msg,
                        [

                            { text: 'OK', onPress: () => { } },
                        ],
                        { cancelable: false }
                    )
                })
            })
        } else {
            Alert.alert(
                'Alerta!',
                'Correo es incorrecto',
                [

                    { text: 'OK', onPress: () => { } },
                ],
                { cancelable: false }
            )
        }

    }

    render() {
        return (

            <Modal
                animationType={this.props.animationType}
                transparent={true}
                visible={this.props.visible}
                onRequestClose={() => {
                    this.props.onClose()
                }}
            >
                <View style={styles.container}>
                    <View style={styles.containterModal}>
                        <Text style={styles.title} >Recupera tu contraseña</Text>
                        <Text style={styles.text}>¿Cuál es tu correo electrónico, nombre de usuario, o nombre completo?</Text>

                        <TextInput
                            style={{ height: 40, color: '#fff', fontSize: 13, backgroundColor: "#ccc", borderRadius: 15, paddingLeft: 10, paddingRight: 10, marginBottom: 25 }}
                            InputProps={{ disableUnderline: true }}
                            keyboardType='email-address'
                            placeholderTextColor='#fff'
                            underlineColorAndroid="rgba(0,0,0,0)"
                            fontFamily="RobotoCondensed-Bold"
                            placeholder="Ingrese su correo"
                            value={this.state.correo}
                            onChangeText={(correo) => this.setState({ correo })}
                        />

                        <Button title='Recuperar' fontFamily='RobotoCondensed-Bold' rounded={true} backgroundColor="#00add2" onPress={this.handleReserPassword.bind(this)} />

                        <Button title='Volver' transparent={true} color="#ccc" fontFamily="RobotoCondensed-Bold" fontSize={14} textStyle={{ textDecorationLine: "underline" }} onPress={this.props.onClose} />
                    </View>

                </View>

                {/*  <OrientationLoadingOverlay
                    visible={this.state.isLoading}
                    color="white"
                    indicatorSize="large"
                    messageFontSize={18}
                    message="Cargando ..."
                /> */}

            </Modal>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "rgba(0,0,0,0.6)",
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row'

    },
    styleInput: {
        height: 39,
        color: '#4F4F4F',
        fontSize: 14,
        textAlign: 'center',
        textDecorationLine: "none",
    },
    containterModal: {
        backgroundColor: 'white',
        borderRadius: 15,
        padding: 15
    },
    title: {
        fontSize: 20,
        fontFamily: "RobotoCondensed-Bold",
        marginBottom: 16,
        color: "#000036",
        textAlign: "center"
    },
    text: {
        fontSize: 14,
        fontFamily: "RobotoCondensed-Bold",
        marginBottom: 16,
        textAlign: "center",
        color: "#CCC"
    }
})