import React, { Component } from 'react'
import { View, TouchableOpacity, Image, Text, FlatList, ImageBackground, ScrollView, StyleSheet, Dimensions, Alert, StatusBar } from 'react-native'
import { Icon, Avatar, FormInput } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'

import Config from "../../app.settings"
import images from '../../components/images'

import * as sessionHelper from '../../helpers/session-helper'

import * as clanService from '../../services/clan-service'
import * as promotorService from '../../services/promotor-service'

import { Header, HeaderButton, HeaderTitle, Button } from '../../components'

const size = Dimensions.get('window').width / 3
const styles = StyleSheet.create({
    itemContainer: {
        width: size,
        height: size,
        //backgroundColor: "#f9f9f9",
        alignItems: "center",
        justifyContent: 'center',
        margin: 8,
        flex: 1,
        borderRadius: 5,
        padding: 8,
        backgroundColor: '#FFF',
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowColor: '#081257',
        //shadowOffset: { height: 5, width: 5 },
        elevation: 4,
    },
    item: {
        flex: 1,
        margin: 3,
        backgroundColor: 'lightblue',
    }
})

export default class Clan extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            nameClan: "",
            userSession: this.props.navigation.getParam('userSession'),
            isEditClan: false,
            clan: [
                {
                    id: "0",
                    name: "Josue",
                    image: images.Wonder,
                    rank: images.CoupOne,
                    is_add: true
                }
            ]
        }
        this.input = React.createRef()
    }

    componentDidMount() {
        this.setClan()
        this.listClan()
    }

    setClan() {
        const userData = this.state.userSession
        const userClan = (userData.clan !== null) ? userData.clan.name : "Dale nombre a tu clan aquí"
        this.setState({ nameClan: userClan })
    }

    listClan() {
        if (this.state.userSession.clan !== null) {
            const promotorData = {
                clan_id: this.state.userSession.clan.id,
                page_size: 50,
                exclude_id: this.state.userSession.id
            }
            promotorService.search(promotorData)
                .then((response) => {
                    const clans = [...this.state.clan]

                    response.data.data.results.filter((clan) => {
                        clans.push(clan)
                    })

                    this.setState({ clan: clans })
                })
        }
    }

    handleClose() {
        this.props.navigation.pop()
    }

    handleShowEditClan() {
        this.setState({ isEditClan: true })
    }

    handleInviteKillers() {
        if (this.state.userSession.clan !== null) {
            this.props.navigation.push("ClanInvite", { userSession: this.state.userSession })
        } else {
            Alert.alert(
                'Error!',
                'Necesitas tener un clan',
                [

                    { text: 'OK', onPress: () => { } },
                ],
                { cancelable: false }
            )
        }
    }

    saveEditNameClan() {
        this.setState({ isEditClan: false })
        const clanData = {
            name: this.state.nameClan
        }
        const clan = this.state.userSession.clan

        if (clan !== null) {
            clanService.update(clan.id, clanData).then(
                (response) => {
                    EventRegister.emit("updateAvatar")
                }).catch((error) => {

                })
        } else {
            clanService.create(clanData).then(
                (response) => {
                    this.updateClanProfile(response.data.data.id)
                }).catch((error) => {

                })
        }
    }

    updateClanProfile(clanId) {
        const promotorData = {
            clan_id: clanId
        }
        promotorService.update(this.state.userSession.id, promotorData)
            .then((response) => {
                EventRegister.emit("updateAvatar")
            })
    }

    render() {

        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />

                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>

                    <Header
                        transparent menu
                        centerComponent={<HeaderTitle>Mi Clan</HeaderTitle>}
                        rightComponent={(<View style={{ width: 48 }} />)}
                    />

                    <View style={{ alignItems: 'center', marginTop: 15 }}>
                        <View>
                            <Avatar
                                rounded
                                source={images.ClanPoster}
                                onPress={() => console.log("Works!")}
                                activeOpacity={0.7}
                                avatarStyle={{ borderWidth: 3, borderColor: 'white' }}
                                containerStyle={{ borderStyle: 'dotted' }}
                                width={120}
                                height={120}
                            />
                        </View>

                        {this.state.isEditClan ? (
                            <View style={{ alignItems: 'center', justifyContent: "center", flexDirection: "row" }}>
                                <FormInput underlineColorAndroid="transparent" placeholder="Tu Clan" placeholderTextColor="white" fontFamily="RobotoCondensed-Bold" inputStyle={{ width: 150, color: "white", textAlign: "center" }}
                                    value={this.state.nameClan} onChangeText={(nameClan) => this.setState({ nameClan })} />
                                <Icon name="check" type="font-awesome" color="white" onPress={this.saveEditNameClan.bind(this)} underlayColor="transparent" />
                            </View>
                        ) : (
                                <View>
                                    <Button
                                        transparent={true}
                                        fontFamily="RobotoCondensed-Bold"
                                        fontSize={17}
                                        color="white"
                                        iconRight={{ name: 'pencil', type: 'font-awesome' }}
                                        title={this.state.nameClan}
                                        onPress={this.handleShowEditClan.bind(this)}
                                    />
                                </View>
                            )}

                    </View>


                </ImageBackground>


                <View style={{ flex: 1 }}>
                    {/*<View style={{ marginLeft: 15, marginRight: 15, paddingTop: 20, paddingBottom: 20 }}>
                        <Text style={{ color: '#b0b0b0', fontFamily: "RobotoCondensed-Light", fontSize: 18 }}>Aliados</Text>
                        </View>*/}

                    <FlatList
                        data={this.state.clan}
                        renderItem={({ item, index }) =>
                            <View style={styles.itemContainer}>

                                {
                                    item.is_add ? (
                                        <Icon name='plus' type="font-awesome" rounded color="#081257" containerStyle={{ backgroundColor: "white" }} reverse reverseColor="#081257" onPress={this.handleInviteKillers.bind(this)} />
                                    ) : (
                                            <View style={{ alignItems: "center" }}>
                                                <SVGImage
                                                    style={{ width: 55, height: 55, backgroundColor: "transparent" }}
                                                    source={{ uri: Config.API_MEDIA_URL + item.avatar.url }}
                                                />

                                                <View style={{ marginTop: 2, alignItems: "center" }}>
                                                    <Text style={{ color: '#121111', fontFamily: "RobotoCondensed-Bold", fontSize: 16 }} numberOfLines={1}>{item.name}</Text>
                                                    <Image style={{ height: 12, width: 12, marginTop: 3 }} source={images.Coin} />
                                                    <Text style={{ color: '#121111', fontFamily: "RobotoCondensed-Bold", fontSize: 10 }}>100 KCoins</Text>
                                                </View>
                                            </View>
                                        )
                                }

                            </View>

                        }
                        keyExtractor={(item, index) => item.id}
                        showsHorizontalScrollIndicator={false}
                        numColumns={3}

                    />
                </View>
            </View>
        )
    }
}

