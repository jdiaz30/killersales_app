/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { View, TouchableOpacity, Image, Text, FlatList, ImageBackground, ScrollView, StyleSheet, Dimensions, Alert, TextInput, StatusBar } from 'react-native'
import { Icon, Avatar, Card, Button, FormInput } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'

import Config from "../../app.settings"
import images from '../../components/images'

import * as sessionHelper from '../../helpers/session-helper'

import * as clanService from '../../services/clan-service'
import * as promotorService from '../../services/promotor-service'

import { SearchBar, Header, HeaderTitle, HeaderButton } from '../../components'

const size = Dimensions.get('window').width / 3
const styles = StyleSheet.create({
    itemContainer: {
        width: size,
        height: size,
        //backgroundColor: "#f9f9f9",
        alignItems: "center",
        justifyContent: 'center',
        margin: 8,
        borderRadius: 5,
        padding: 8,
        flex: 1,
        backgroundColor: '#FFF',
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowColor: '#081257',
        //shadowOffset: { height: 5, width: 5 },
        elevation: 4,
    },
    item: {
        flex: 1,
        margin: 3,
        backgroundColor: 'lightblue',
    }
})


export default class ClanInvite extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            userSession: this.props.navigation.getParam('userSession'),
            searchTxt: "",
            killers: []
        }
        this.input = React.createRef()
    }

    handleClose() {
        this.props.navigation.pop()
    }

    handleSearch() {
        const params = {
            name: this.state.searchTxt,
            page_size: 20,
            exclude_id: this.state.userSession.id
        }
        promotorService.search(params).then((response) => {
            this.setState({ killers: response.data.results })
        })
    }

    handleInviteKiller(killer) {
        const notifyData = {
            type_notify: "invite_clan",
            message: "Te han invitado a unirte al clan : " + this.state.userSession.clan.name,
            status: 0,
            to_promotor_id: killer.id,
            action_id: this.state.userSession.clan.id//clan_id
        }

        clanService.inviteClan(JSON.stringify(notifyData)).then(() => {
            Alert.alert(
                'Alerta!',
                'Invitación enviada',
                [
                    { text: 'OK', onPress: () => { } },
                ],
                { cancelable: false }
            )
        })
    }

    render() {

        return (

            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />

                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>
                    <Header
                        transparent
                        leftComponent={(
                            <HeaderButton icon="md-arrow-back" color="white" typeicon="ionicon" onPress={this.handleClose.bind(this)} />
                        )}
                        centerComponent={<HeaderTitle>Invitar Killers</HeaderTitle>}
                        rightComponent={(<View style={{ width: 48 }} />)}
                    />
                    <SearchBar
                        onSubmitEditing={this.handleSearch.bind(this)}
                        round
                        lightTheme
                        onChangeText={(searchTxt) => this.setState({ searchTxt })}
                        placeholder='Buscar ...' />

                </ImageBackground>


                <View style={{ flex: 1 }}>
                    {/*<View style={{ marginLeft: 15, marginRight: 15, paddingTop: 20, paddingBottom: 20 }}>
                        <Text style={{ color: '#b0b0b0', fontFamily: "RobotoCondensed-Light", fontSize: 18 }}>Killers</Text>
                </View>*/}


                    <FlatList
                        data={this.state.killers}
                        renderItem={({ item, index }) =>

                            <TouchableOpacity style={styles.itemContainer} onPress={this.handleInviteKiller.bind(this, item)}  >
                                <View style={{ flex: 1, alignItems: "center" }}>
                                    <SVGImage
                                        style={{ width: 55, height: 55, backgroundColor: "transparent" }}
                                        source={{ uri: Config.API_MEDIA_URL + item.avatar.url }}
                                    />
                                    <View style={{ marginTop: 2, alignItems: "center" }}>
                                        <Text style={{ color: '#121111', fontFamily: "RobotoCondensed-Bold", fontSize: 15 }} ellipsizeMode="tail" numberOfLines={1}>{item.name}</Text>
                                        <Image style={{ height: 12, width: 12, marginTop: 3 }} source={images.Coin} />
                                        <Text style={{ color: '#121111', fontFamily: "RobotoCondensed-Bold", fontSize: 10 }}>{item.kcoins + " KCoins"}</Text>
                                    </View>
                                </View>
                            </TouchableOpacity>
                        }
                        keyExtractor={(item, index) => (item.id).toString()}
                        showsHorizontalScrollIndicator={false}
                        numColumns={3}
                    />

                </View>



            </View>

        )
    }
}

