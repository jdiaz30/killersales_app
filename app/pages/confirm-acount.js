/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React, { Component } from 'react'
import { Platform, StyleSheet, View, StatusBar, Alert, ImageBackground } from 'react-native'
import { Button, FormLabel, FormInput } from 'react-native-elements'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'

import moment from 'moment'

import images from '../components/images'
import registerStyle from '../styles/register.style'
import * as authService from '../services/auth-service'
import * as sessionHelper from '../helpers/session-helper'
import * as globalHelper from '../helpers/global-helper'
import * as appService from '../services/app-service'
import { Text, Header, HeaderTitle, HeaderButton, TextInputForm } from '../components'

export default class ConfirmAcount extends Component {
    static navigationOptions = {
        title: 'Crear cuenta',
        header: null
    }

    constructor(props) {
        super(props)

        this.state = {
            phone: this.props.navigation.getParam('phone'),
            numberArea: this.props.navigation.getParam('numberArea'),
            userTemp: this.props.navigation.getParam('userTemp'),
            code: '',
            showSendCode: true,
            isLoading: false
        }
    }

    componentDidMount() {

    }

    handleReSendCode = () => {
        const self = this
        const params = {
            userapp: this.state.userTemp,
            promotor: {
                country: { number_area: this.state.numberArea },
                phone: this.state.phone
            }
        }

        appService.sendValidatePhoneAccount(params).then((response) => {
            this.setState({ isLoading: false })
            console.log("joper30 handleReSendCode", response)
            globalHelper.showMessageAlert("Behlaak dice:", response.data.user_msg, null)
        }).catch((error) => {
            this.setState({ isLoading: false })
            console.log("joper30 handleReSendCode error ", error)
            globalHelper.showMessageAlert("Error!", error.response.data.user_msg, null)
        })
    }

    handleValidateCode() {
        let errorCode = (this.state.code == "") ? true : false
        this.setState({ errorCode: errorCode })

        if (errorCode) {
            globalHelper.showMessageAlert("Error!", "Codigo Incorrecto", null)
        } else {
            this.validateAccount(this.state.code)
        }
    }

    validateInput(name) {
        switch (name) {
            case "code":
                let errorName = (this.state.name == "") ? true : false
                this.setState({ errorName: errorName })
                break
        }
    }

    validateAccount(code) {
        const params = {
            "code": code,
            "date_add": moment().utc().format("YYYY-MM-DD H:mm:ss"),
            "userapp": this.state.userTemp.id
        }

        this.setState({ isLoading: true }, () => {
            appService.validatePhoneAccount(params)
                .then((response) => {
                    this.setState({ isLoading: false })
                    sessionHelper.clear('validateCode')
                    globalHelper.showMessageAlert("Exito", "Su correo se ha verificado", () => {
                        this.props.navigation.popToTop()
                    })
                })
                .catch((error) => {
                    globalHelper.showMessageAlert("Error", error.response.data.user_msg, null)
                    const statusResponseData = error.response.data.data
                    const showSendCode = (statusResponseData == 1) ? true : false

                    this.setState({ isLoading: false, showSendCode: showSendCode })
                })
        })
    }

    render() {
        return (

            <ImageBackground source={images.Menu} style={{ flex: 1 }} resizeMode='cover'>
                <View style={[registerStyle.boxRegister, { padding: 0 }]}>

                    <StatusBar
                        backgroundColor="#000036"
                        barStyle="light-content"
                    />
                    <Header
                        transparent
                        leftComponent={(
                            <HeaderButton icon="md-arrow-back" color="white" typeicon="ionicon" onPress={() => { this.props.navigation.goBack() }} />
                        )}
                    />
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', paddingHorizontal: 16 }}>
                        <View style={{ minWidth: 300, width: '100%', paddingHorizontal: 16 }}>
                            <Text bold color="#FFF" size={22} style={{ textAlign: 'center', marginBottom: 16 }} >Ingresar código</Text>

                            <Text color="#FFF" size={16} style={{ textAlign: 'center', marginBottom: 16 }} >Hemos enviado un mensaje a su correo</Text>

                            <TextInputForm secureTextEntry={true} style={[{ textAlign: 'center' }, (this.state.errorCode == true) ? registerStyle.input_error : {}]} placeholder="XXXX XXXX"
                                value={this.state.code} onChangeText={(code) => this.setState({ code })} onEndEditing={this.validateInput.bind(this, "code")} />

                            <View style={{ marginBottom: 0, marginTop: 16 }} >
                                <Button full title='VALIDAR' rounded={true} backgroundColor='#fdec5b' color="#2b2e34" fontFamily="RobotoCondensed-Bold" containerViewStyle={{ marginLeft: 0, marginRight: 0 }} buttonStyle={{ borderRadius: 4 }} loading={this.state.isLoading} onPress={this.handleValidateCode.bind(this)} />
                            </View>
                            {this.state.showSendCode && <View style={{ marginBottom: 0, marginTop: 16 }} >
                                <Button full title='REENVIAR CÓDIGO' rounded={true} containerViewStyle={{ marginLeft: 0, marginRight: 0 }} buttonStyle={{ backgroundColor: 'transparent', borderColor: '#fdec5b', borderWidth: 1, borderRadius: 4 }} color="#ffffff" fontFamily="RobotoCondensed-Bold" onPress={this.handleReSendCode.bind(this)} />
                            </View>}
                        </View>

                    </View>

                    <OrientationLoadingOverlay
                        visible={this.state.isLoading}
                        color="white"
                        indicatorSize="large"
                        messageFontSize={18}
                        message="Validando ..."
                    />

                </View>
            </ImageBackground>

        )
    }
}
