/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { Alert, Platform, StyleSheet, View, StatusBar, Image, ScrollView, FlatList, ActivityIndicator, TouchableOpacity, ImageBackground, Dimensions } from 'react-native'
//import { Button, FormLabel, FormInput, FormValidationMessage, Card, ListItem, Icon } from 'react-native-elements'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'

import Config from "../../app.settings"

import HeaderComponent from '../../components/header/header'
import catalogStyle from '../../styles/catalog.style'
import CatalogItem from './catalog-items'
import * as catalogService from '../../services/catalog-service'
import * as sessionHelper from '../../helpers/session-helper'

import images from '../../components/images'
import { Header, HeaderButton, HeaderTitle, Card, Text } from '../../components'

const size = Dimensions.get('window').width / 3

const CatItem = props => {
    const textActividad = (props.activities.length > 1) ? "Actividades" : "Actividad"
    return (
        <Card
            containerStyle={{
                flex: 1,
                width: size,
                margin: 8,
                marginBottom: 8,
                shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: '#081257',
                //shadowOffset: { height: 5, width: 5 },
                elevation: 4,
            }} >

            <TouchableOpacity
                onPress={props.onPress}
                style={{ height: 140, width: 'auto', borderTopLeftRadius: 8, borderTopRightRadius: 8, backgroundColor: '#CCC' }}>
                <Image source={{ uri: props.image }} style={{ height: '100%', width: '100%', borderTopLeftRadius: 8, borderTopRightRadius: 8 }} resizeMode={'cover'} />
            </TouchableOpacity>
            <View style={{ justifyContent: 'center', alignItems: 'center', padding: 8 }}>
                <Text style={{ color: '#000', fontFamily: "RobotoCondensed-Bold", fontSize: 18 }} numberOfLines={1}>{props.name}</Text>
                <Text style={{ color: '#CCC', fontFamily: "RobotoCondensed-Bold", fontSize: 14 }} numberOfLines={1}>{props.activities.length} {textActividad} </Text>
                <Text style={{ color: '#000', fontFamily: "RobotoCondensed-Bold", fontSize: 11 }} numberOfLines={1}>{props.operator.name}</Text>
            </View>
        </Card>
    )
}


export default class Catalog extends Component {
    static navigationOptions = {
        headerTitle: "Cátalogo",
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            catalogs: [],
            isRefresh: false,
            isLoading: false,
            userSession: null,
        }
    }

    getUserSession() {
        sessionHelper.getUser().then((user) => {
            if (user !== null) {
                console.log("joper30 getUserSession", user)
                this.setState({ userSession: user }, () => {
                    this.listCatalogs("init")
                })
            }
        })
    }

    componentDidMount() {
        this.getUserSession()
        sessionHelper.getCatalog().then((catalog) => {
            if (catalog) {
                Alert.alert(
                    'Compra Pendiente',
                    catalog.name,
                    [
                        {
                            text: 'Ir', onPress: () => {
                                this.handleActivities(catalog)
                            }
                        },
                        {
                            text: 'Cerrar', onPress: () => {

                            }
                        },
                    ],
                    { cancelable: true }
                )
            }
        })
    }

    listCatalogs(option) {
        const isLoading = (option == "refresh") ? false : true
        let params = { page_size: 100, status: 1, operator_id: this.state.userSession.operator_id }
        if (this.state.userSession.is_freelancer == true) {
            delete params.operator_id
        }
        this.setState({ isLoading: isLoading }, () => {
            catalogService.getAll(params).then((response) => {
                const catalogs = response.data.results
                catalogs.map((catalog) => {
                    catalog.poster = (catalog.poster !== null) ? Config.API_MEDIA_URL + catalog.poster_100 : "https://www.cultura.gal/sites/default/files/images/evento/ameixarock.png"
                    catalog.id = catalog.id.toString()
                    return catalog
                })
                this.setState({ catalogs: catalogs, isRefresh: false, isLoading: false })
            }).catch((error) => {
                this.setState({ isLoading: false })
            })
        })

    }

    handleRefreshCatalog() {
        this.setState({ isRefresh: true }, () => {
            this.listCatalogs("refresh")
        })
    }

    handleClose() {
        this.props.navigation.pop()
    }

    handleActivities = (catalog) => {
        this.props.navigation.push("ActivityCatalog", { catalog: catalog, userSession: this.state.userSession })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />

                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>

                    <Header
                        transparent menu
                        centerComponent={<HeaderTitle>Catálogos</HeaderTitle>}
                        /*   rightComponent={(<HeaderButton icon="md-funnel" color="white" typeicon="ionicon" />)} */
                        rightComponent={(<View style={{ width: 48 }} />)}
                    />

                </ImageBackground>

                <FlatList
                    data={this.state.catalogs.filter((item) => item.activities.length > 0)}
                    renderItem={({ item, index }) => <CatItem
                        image={item.poster}
                        name={item.name}
                        activities={item.activities.filter((item) => item.status == 1)}
                        operator={item.operator}
                        onPress={() => this.handleActivities(item)}
                    />}
                    keyExtractor={(item, index) => item.id}
                    showsHorizontalScrollIndicator={false}

                    onRefresh={this.handleRefreshCatalog.bind(this)}
                    refreshing={this.state.isRefresh}
                    numColumns={2}
                //style={{ paddingBottom: 16 }}
                />

                <OrientationLoadingOverlay
                    visible={this.state.isLoading}
                    color="white"
                    indicatorSize="large"
                    messageFontSize={18}
                    message="Cargando ..."
                />
            </View>
        )
    }
}

