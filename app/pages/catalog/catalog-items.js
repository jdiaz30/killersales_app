import React, { Component } from 'react'
import { Platform, StyleSheet, Text, View, StatusBar, ImageBackground, Image, Dimensions, TouchableOpacity } from 'react-native'

import { Icon } from 'react-native-elements'

const size = Dimensions.get('window').width / 2
const styles = StyleSheet.create({
    itemContainer: {
        width: size,
        height: size / 1.3,
        alignItems: "center",
        justifyContent: 'center',
        margin: 4,
        borderRadius: 10,
        //padding: 8,
        flex: 1,
        overflow: 'hidden',
    },
    item: {
        flex: 1,
        margin: 3,
        backgroundColor: 'lightblue',
    }
})

export default class CatalogItem extends Component {

    constructor(props) {
        super(props)
    }

    handleOrderService(catalog) {
        this.props.navigation.push("OrderService", { catalog: catalog, userSession: this.props.userSession })
    }

    render() {
        const { item } = this.props
        return (
            <TouchableOpacity style={styles.itemContainer} onPress={this.handleOrderService.bind(this, item)}>

                <Image style={{
                    height: size / 1.3,
                    width: size,
                    borderRadius: 10
                }} source={{ uri: item.poster }} borderRadius={10} />
                <View style={{ position: "absolute", top: 0, left: 0, borderRadius: 10, backgroundColor: 'rgba(43,46,52,0.5)', width: "100%", height: "100%" }}>
                    <View style={{ alignContent: 'center', alignItems: "center", position: 'relative', marginTop: 55 }}>
                        <Text style={{ color: 'white', fontFamily: "RobotoCondensed-Bold", fontSize: 18 }} numberOfLines={1}>{item.name}</Text>
                        <Text style={{ color: 'white', fontFamily: "RobotoCondensed-Bold", fontSize: 14 }} numberOfLines={1}>25 Pax Disponibles</Text>
                    </View>
                </View>
            </TouchableOpacity>
        )
    }
}