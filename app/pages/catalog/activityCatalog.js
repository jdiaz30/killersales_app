/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { Platform, StyleSheet, View, StatusBar, Image, Alert, FlatList, TouchableOpacity, ImageBackground, Dimensions } from 'react-native'
import { Icon } from 'react-native-elements'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'

import Config from "../../app.settings"

import HeaderComponent from '../../components/header/header'
import catalogStyle from '../../styles/catalog.style'
import CatalogItem from './catalog-items'
import * as catalogService from '../../services/catalog-service'
import * as cityService from '../../services/city-service'

import images from '../../components/images'
import { Header, HeaderButton, HeaderTitle, Card, Text } from '../../components'
import * as sessionHelper from '../../helpers/session-helper'
import serviceStyle from '../../styles/service.style'

const size = Dimensions.get('window').width / 3

const CatItem = props => {
    return (
        <Card
            containerStyle={{
                flex: 1,
                width: size,
                margin: 8,
                marginBottom: 8,
                shadowOpacity: 0.75,
                shadowRadius: 5,
                shadowColor: '#081257',
                //shadowOffset: { height: 5, width: 5 },
                elevation: 4,
                position: 'relative',
                width: 'auto'
            }} >

            <TouchableOpacity
                onPress={props.onPress}
                style={{ height: 140, width: 'auto', borderTopLeftRadius: 8, borderTopRightRadius: 8, backgroundColor: '#CCC' }}>
                <Image source={{ uri: props.image }} style={{ height: '100%', width: '100%', borderTopLeftRadius: 8, borderTopRightRadius: 8 }} resizeMode={'cover'} />
                {props.selected && <Image source={images.Shadow} style={{ height: '100%', width: '100%', borderTopLeftRadius: 8, borderTopRightRadius: 8, position: 'absolute' }} resizeMode={'cover'} />}
                {props.selected && <Image source={images.Check} style={{ position: 'absolute', top: 8, right: 8 }} />}
            </TouchableOpacity>
            <View style={{ justifyContent: 'center', alignItems: 'center', padding: 8, height: 80 }}>
                <Text style={{ color: '#000', fontFamily: "RobotoCondensed-Bold", fontSize: 18 }} numberOfLines={1}>{props.name}</Text>
                <View style={{ flexDirection: 'row' }}>
                    <Icon name="md-person" type="ionicon" size={14} color="#000040" />
                    <Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#000040" }}>  $ {props.price_rep_adult}</Text>
                </View>
                <View style={{ flexDirection: 'row' }}>
                    <Icon name="md-happy" type="ionicon" size={14} color="#000040" />
                    <Text style={{ fontFamily: "RobotoCondensed-Bold", color: "#000040" }}>  $ {props.price_rep_child}</Text>
                </View>
            </View>

        </Card>
    )
}


export default class ActivityCatalog extends Component {

    static navigationOptions = {
        headerTitle: "Actividades",
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            isRefresh: true,
            userSession: this.props.navigation.getParam('userSession'),
            catalog: this.props.navigation.getParam('catalog'),
            activities: [],
            cities: [],
            city: null,
            activitiesSelected: [],
            isLoading: false
        }
    }

    componentDidMount() {
        sessionHelper.getCities().then((cities) => {
            if (cities !== null) {
                this.setCities(cities)
            } else {
                this.listCities()
            }
        })
    }

    componentWillUnmount() {
        this.saveStorage()
    }

    saveStorage = () => {
        if (this.state.activitiesSelected.length > 0) {
            var save = sessionHelper.saveActivities(this.state.activitiesSelected)
            var save = sessionHelper.saveCatalog(this.state.catalog)
            var save = sessionHelper.saveCity(this.state.city)
        }
    }

    listActivities() {
        sessionHelper.getActivities().then((activitiesStorage) => {
            this.setState({ isLoading: true }, () => {
                catalogService.getActivities(this.state.catalog.id).then((response) => {
                    const activities = response.data
                    activities.map((item) => {
                        if (activitiesStorage) {
                            const act = activitiesStorage.find(v => v.id == item.id)
                            item.selected = (act && act.catalog_id == this.state.catalog.id)
                        } else {
                            item.selected = false
                        }
                        item.poster = (item.poster !== null) ? Config.API_MEDIA_URL + item.poster_500 : "https://www.cultura.gal/sites/default/files/images/evento/ameixarock.png"
                        item.id = item.id.toString()
                        return item
                    })

                    this.setState({ activitiesSelected: activitiesStorage ? activitiesStorage : [], activities: activities, isRefresh: false, isLoading: false })
                })
            })

        })
    }

    listCities() {
        cityService.getAll({ status: 1, page_size: 28 }).then((response) => {
            const data = [...response.data.data.results]
            sessionHelper.saveCities(data)
            this.setCities(data)
        })
    }

    setCities(cities) {
        sessionHelper.getCity().then((citySelected) => {
            let citiesFilter = cities.map((item) => {
                item.selected = (citySelected && citySelected.id == item.id)
                return item
            })
            this.setState({ cities: citiesFilter, city: citySelected }, () => {
                this.listActivities()
            })
        })
    }

    handleRefreshCatalog() {
        this.setState({ isRefresh: true }, () => {
            this.listActivities()
        })
    }

    handleClose() {
        this.props.navigation.pop()
    }

    handleOrderService(activity) {
        var activities = [...this.state.activities]
        var index = activities.findIndex((item) => item.id == activity.id)
        activities[index].selected = (activities[index].selected === true) ? false : true
        this.setState({ activities: activities }, () => {
            var activ = this.state.activities.filter(item => item.selected === true)
            this.setState({ activitiesSelected: activ })
        })
    }

    handleCarshop = () => {
        const services = this.state.city ? this.state.activities.filter((item) => (item.city_id == this.state.city.id && item.selected)) : []
        if (services.length > 0) {
            this.saveStorage()
            this.props.navigation.push("OrderService", { userSession: this.state.userSession, catalog: this.state.catalog, services: services, city: this.state.city })
        } else {
            Alert.alert(
                'Alerta',
                'Debes de seleccionar una o mas actividades',
                [
                    {
                        text: 'OK', onPress: () => {

                        }
                    },
                ],
                { cancelable: false }
            )
        }
    }

    handleSelectCity = (city) => {
        this.setState({ city: city }, () => {
            const cities = this.state.cities.map((item) => {
                item.selected = (city.id == item.id) ? true : false
                return item
            })
            this.setState({ cities: cities })
        })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />
                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>
                    <Header
                        transparent
                        leftComponent={(
                            <HeaderButton icon="md-arrow-back" color="white" typeicon="ionicon" onPress={this.handleClose.bind(this)} />
                        )}
                        centerComponent={<HeaderTitle>{this.state.catalog.name}</HeaderTitle>}
                        rightComponent={(
                            <HeaderButton icon="md-cart" badge={this.state.city ? this.state.activities.filter((item) => item.selected && item.city_id == this.state.city.id).length : 0} color="white" typeicon="ionicon" onPress={this.handleCarshop} />
                        )}
                    />
                </ImageBackground>

                <View style={{ marginBottom: 16, marginHorizontal: 8, marginTop: 16 }}>
                    <FlatList
                        data={this.state.cities}
                        renderItem={({ item, index }) =>
                            <TouchableOpacity onPress={() => this.handleSelectCity(item)} >
                                <View style={item.selected ? serviceStyle.city_selected : serviceStyle.city}>
                                    <Text style={item.selected ? serviceStyle.city_text_selected : serviceStyle.city_text}> {item.slug}</Text>
                                </View>
                            </TouchableOpacity>
                        }
                        keyExtractor={(item, index) => (item.id).toString()}
                        showsHorizontalScrollIndicator={false}
                        horizontal={true}
                    />
                </View>

                {!this.state.city && <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>
                    <Text bold size={24} style={{ textAlign: 'center' }} >Seleccionar Ciudad</Text>
                </View>}

                {this.state.city && this.state.activities.filter((item) => item.city.id == this.state.city.id).length == 0 && <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', alignContent: 'center', paddingHorizontal: 24 }}>
                    <Text bold size={24} style={{ textAlign: 'center' }} >No hay actividades registradas en {this.state.city.name}</Text>
                </View>}

                {this.state.city !== null && this.state.activities.length > 0 && <FlatList
                    //data={this.state.activities}
                    data={this.state.activities.filter((item) => item.city.id == this.state.city.id)}
                    renderItem={({ item, index }) => <CatItem
                        image={item.poster}
                        name={item.name}
                        price_rep_adult={item.price_rep_adult}
                        price_rep_child={item.price_rep_child}
                        selected={item.selected}
                        onPress={this.handleOrderService.bind(this, item)}
                    />}
                    keyExtractor={(item, index) => item.id}
                    showsHorizontalScrollIndicator={false}

                    onRefresh={this.handleRefreshCatalog.bind(this)}
                    refreshing={this.state.isRefresh}
                    numColumns={2}
                //style={{ paddingBottom: 16 }}
                />}

                <OrientationLoadingOverlay
                    visible={this.state.isLoading}
                    color="white"
                    indicatorSize="large"
                    messageFontSize={18}
                    message="Cargando actividades"
                />

            </View>
        )
    }
}

