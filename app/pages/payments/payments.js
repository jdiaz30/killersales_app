/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react'
import { View, TouchableOpacity, Image, Text, FlatList, ImageBackground, ScrollView, StyleSheet, Dimensions, Alert, TextInput, StatusBar } from 'react-native'
import { Icon, Avatar, Card, Button, FormInput, SearchBar } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'
import OrientationLoadingOverlay from 'react-native-orientation-loading-overlay'

import Config from "../../app.settings"
import images from '../../components/images'
import { Header, HeaderTitle } from '../../components'
import * as sessionHelper from '../../helpers/session-helper'
import * as dateHelper from '../../helpers/date-helper'
import * as socketHelper from '../../helpers/socket-helper'

import * as orderCobratorService from '../../services/order-cobrate-service'

export default class Payments extends Component {
    static navigationOptions = {
        header: null
    }

    constructor(props) {
        super(props)
        this.state = {
            isLoading: false,
            payments: [],
            userSession: null
        }
    }

    componentDidMount() {
        this.getUserSession()
    }

    getUserSession() {
        sessionHelper.getUser().then((user) => {
            this.setState({ userSession: user, isLoading: true }, () => {
                this.listPayments(user.id)
            })

        })
    }

    listPayments(cobratorId) {
        orderCobratorService.getAll({ cobrator_id: cobratorId, page_size: 20 })
            .then((response) => {
                this.setState({ payments: response.data.data.results, isLoading: false })
            })
            .catch((error) => {
                this.setState({ isLoading: false })
            })
    }

    render() {
        return (
            <View style={{ backgroundColor: 'white', flex: 1 }}>
                <StatusBar
                    backgroundColor="#000036"
                    barStyle="light-content"
                />
                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>
                    <Header
                        transparent menu
                        centerComponent={<HeaderTitle>Mis cobros</HeaderTitle>}
                        rightComponent={(<View style={{ width: 48 }} />)}
                    />
                </ImageBackground>

                <View style={{ flex: 1 }}>
                    {(this.state.isLoading == false && this.state.payments.length == 0) &&
                        <View style={{ padding: 25, alignItems: 'center' }}>
                            <Text style={{ color: '#000036', fontFamily: "RobotoCondensed-Bold", fontSize: 16 }}>No se encontraron cobranzas</Text>
                        </View>
                    }

                    <FlatList
                        data={this.state.payments}
                        renderItem={({ item, index }) =>

                            <TouchableOpacity>
                                <View style={{ marginBottom: 8, borderBottomColor: "#f7f7f7", borderBottomWidth: 1 }}>
                                    <View style={{ flexDirection: "row" }}>
                                        <View style={{ padding: 15, flex: 1 }}>
                                            <Text style={{ color: '#000036', fontFamily: "RobotoCondensed-Bold", fontSize: 16 }}>{item.order.total}</Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>

                        }
                        keyExtractor={(item, index) => (item.id).toString()}
                        showsHorizontalScrollIndicator={false}
                    />
                </View>
                <OrientationLoadingOverlay
                    visible={this.state.isLoading}
                    color="white"
                    indicatorSize="large"
                    messageFontSize={18}
                    message="Cargando ..."
                />
            </View>

        )
    }
}

