

import React, { Component } from 'react'
import { View, StatusBar, ImageBackground, FlatList, Image, StyleSheet } from 'react-native'
import { Header, HeaderTitle, HeaderButton, HeaderLeft, HeaderRight, Text } from '../../components'
import images from '../../components/images'

export default class Ranking extends Component {
    static navigationOptions = {
        drawerLabel: 'Ranking',
        header: null
    }

    render() {
        return (

            <View style={{ flex: 1 }}>

                <StatusBar backgroundColor="#000036" barStyle="light-content" />

                <ImageBackground source={images.ProfileBack} style={{ width: 'auto', height: 'auto' }} resizeMode='cover'>

                    <Header
                        transparent menu
                        centerComponent={<HeaderTitle>RANKING</HeaderTitle>}
                        rightComponent={(<HeaderButton icon="md-share" color="white" typeicon="ionicon" />)}
                    />


                </ImageBackground>
                <View>

                </View>

            </View>



        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    map: {
        width: "100%",
        height: "100%"
    }
});
