export default {
    image: {
        width: 160,
        height: 160
    },
    buttonIcon: {
        position: 'absolute',
        bottom: 0,
        right: 0

    }

};