
export default {
    boxLogin: {
        flex: 1,
        backgroundColor: 'rgba(43,46,52,0.6)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logo: {
        //flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 16,
    },
    input: {
        color: 'white',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        height: 40,
        width: "100%",
        borderRadius: 20,
        marginBottom: 16,
        fontSize: 13,
        padding: 15
    }
}

