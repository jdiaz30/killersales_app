
export default {
    boxRegister: {
        flex: 1,
        //backgroundColor: '#d0d565',
        padding: 20
    },
    logo: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    input: {
        color: 'white',
        height: 40,
        width: "98%",
        borderColor: '#4f5565',
        borderWidth: 1,
        borderRadius: 20,
        marginBottom: 20,
        fontSize: 13,
        padding: 15,
        fontFamily: "RobotoCondensed-Bold"
    },
    input_error: {
        color: 'red',
        //height: 40,
        //width: "98%",
        borderColor: 'red',
        borderWidth: 1,
        //borderRadius: 20,
        //marginBottom: 20,
        fontSize: 13,
        padding: 15,
        fontFamily: "RobotoCondensed-Bold"
    },
    inputButton: {
        height: 44,
        paddingHorizontal: 16,
        backgroundColor: '#FFF',
        marginBottom: 16,
        borderWidth: 1,
        borderRadius: 4,
        borderColor: '#081257',
        justifyContent: 'center'
    }
}

