
export default {
    input: {
        color: '#000036',
        height: 40,
        fontSize: 13,
    },
    city: {
        padding: 6,
        backgroundColor: 'white',
        marginRight: 6,
        borderRadius: 5,
        borderColor: "#d0d0e6",
        borderWidth: 1
    },
    city_selected: {
        padding: 6,
        backgroundColor: '#00add2',
        marginRight: 6,
        borderRadius: 5,
        borderColor: "white",
        borderWidth: 1
    },
    city_text: {
        color: "#6e6e92"
    },
    city_text_selected: {
        color: "white"
    },
    textShadowDark: {
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: { width: -1, height: 1 },
        textShadowRadius: 10
    }
}

