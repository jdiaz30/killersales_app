import jwt_decode from 'jwt-decode'
import { AsyncStorage } from 'react-native'

export async function decodeToken() {
    const token = await AsyncStorage.getItem("userToken")
    const tokenData = jwt_decode(token)

    return new Promise((resolve, reject) => {
        resolve(tokenData)
    })
}

export async function saveToken(token) {
    await AsyncStorage.setItem('userToken', token)
}

export async function saveUser(user) {
    await AsyncStorage.setItem('userSession', user)
}

export async function getUser() {
    const user = await AsyncStorage.getItem("userSession")

    return new Promise((resolve, reject) => {
        console.log("joper30 async ", user)
        resolve(JSON.parse(user))
    })
}

export async function saveClan(clan) {
    await AsyncStorage.setItem('userClan', clan)
}

export async function getClan() {
    const clan = await AsyncStorage.getItem("userClan")

    return new Promise((resolve, reject) => {
        resolve(clan)
    })
}

export async function saveWelcome() {
    await AsyncStorage.setItem('welcomePage', 'welcomePage')
}

export async function getWelcome() {
    const welcome = await AsyncStorage.getItem('welcomePage')

    return new Promise((resolve, reject) => {
        resolve(welcome)
    })
}

export async function saveNotification(token) {
    await AsyncStorage.setItem('notificationToken', token)
}

export async function getNotification() {
    const notification = await AsyncStorage.getItem('notificationToken')

    return new Promise((resolve, reject) => {
        resolve(notification)
    })
}

export async function saveOrder(order) {
    await AsyncStorage.setItem('order', JSON.stringify(order))
}

export async function removeOrder() {
    await AsyncStorage.removeItem('order')
}

export async function getOrder() {
    const order = await AsyncStorage.getItem('order')
    return new Promise((resolve, reject) => {
        resolve(order ? JSON.parse(order) : null)
    })
}

export async function saveActivities(activities) {
    await AsyncStorage.setItem('activities', JSON.stringify(activities))
}

export async function removeActivities() {
    await AsyncStorage.removeItem('activities')
}

export async function getActivities() {
    const activities = await AsyncStorage.getItem('activities')
    return new Promise((resolve, reject) => {
        resolve(activities ? JSON.parse(activities) : null)
    })
}

export async function saveCatalog(catalog) {
    await AsyncStorage.setItem('catalog', JSON.stringify(catalog))
}

export async function removeCatalog() {
    await AsyncStorage.removeItem('catalog')
}

export async function getCatalog() {
    const catalog = await AsyncStorage.getItem('catalog')
    return new Promise((resolve, reject) => {
        resolve(catalog ? JSON.parse(catalog) : null)
    })
}

export async function saveCity(city) {
    await AsyncStorage.setItem('city', JSON.stringify(city))
}

export async function removeCity() {
    await AsyncStorage.removeItem('city')
}

export async function getCity() {
    const city = await AsyncStorage.getItem('city')
    return new Promise((resolve, reject) => {
        resolve(city ? JSON.parse(city) : null)
    })
}

export async function saveCities(cities) {
    await AsyncStorage.setItem('cities', JSON.stringify(cities))
}

export async function getCities() {
    let cities = await AsyncStorage.getItem('cities')
    return new Promise((resolve, reject) => {
        resolve(cities !== null ? JSON.parse(cities) : null)
    })
}
//Obtenemos la lista segun su identificador
export async function getDataList(name) {
    let data = await AsyncStorage.getItem(name)
    return new Promise((resolve, reject) => {
        resolve(data !== null ? JSON.parse(data) : null)
    })
}
//Guardamos listas con un identificador
export async function saveDataList(data, name) {
    await AsyncStorage.setItem(name, JSON.stringify(data))
}

export async function saveTempValidateCodeUser(data) {
    await AsyncStorage.setItem('validateCode', JSON.stringify(data))
}

export async function clear(name) {
    await AsyncStorage.removeItem(name)
}
