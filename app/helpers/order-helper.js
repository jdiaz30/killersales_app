import moment from 'moment'

export function getStatusBackground(status) {
    let statusColor = "red"

    switch (status) {
        case 0://pendiente
            statusColor = "#06b6f7"
            break
        case 1://reservado
            statusColor = "#05ba4c"
            break
        case 2://Pagado
            statusColor = "#142974"
            break
        case 3:
            statusColor = "#fc131e"
            break
    }
    return statusColor
}

export function getStatusText(status) {
    let statusText = ""
    switch (status) {
        case 0:
            statusText = "Pendiente"
            break
        case 1:
            statusText = "Reservado"
            break
        case 2:
            statusText = "Pagado"
            break
        case 3:
            statusText = "Cancelado"
            break
    }

    return statusText
}

export function getTotalText(order) {
    let simbol = (order.money !== undefined && order.money !== null) ? order.money.simbol : ""
    let totalText = simbol + " " + parseFloat(order.total).toFixed(2)
    return totalText
}

export function getTotalBalanceText(order) {
    let simbol = (order.money !== undefined && order.money !== null) ? order.money.simbol : ""
    let total = parseFloat(order.total - order.total_payment)
    let totalText = ""
    let prevText = (order.type_payment == "deposito") ? "C.C" : ""

    if (total > 0 && (order.status == 0 || order.status == 1)) {
        totalText = simbol + " " + (total).toFixed(2) + " " + prevText
    }
    return totalText
}

export function getTotalBalance(order) {
    let simbol = (order.money !== undefined && order.money !== null) ? order.money.simbol : ""
    let total = parseFloat(order.total) - parseFloat(order.total_payment)
    let totalText = ""
    if (total > 0) {
        totalText = simbol + " " + (total).toFixed(2)
    }
    return totalText
}

export function parsePickUps(pickups, activityPickupDate) {
    pickups.map((pickup) => {
        const date = moment(activityPickupDate).day()
        pickup.address = pickup.lu_address
        switch (date) {
            case 0:
                pickup.time = pickup.do_time
                break
            case 1:
                pickup.time = pickup.lu_time
                break
            case 2:
                pickup.time = pickup.ma_time
                break
            case 3:
                pickup.time = pickup.mi_time
                break
            case 4:
                pickup.time = pickup.ju_time
                break
            case 5:
                pickup.time = pickup.vi_time
                break
            case 6:
                pickup.time = pickup.sa_time
                break
        }
        return pickup
    })

    return pickups
}

export function parsePhone(phone) {
    let phoneParse = { phone: "", numberArea: "" }
    if (phone !== "") {
        let index = phone.indexOf(" ")
        phoneParse.phone = phone.substr(index)
        phoneParse.numberArea = phone.substr(0, index)
    }
    return phoneParse
}