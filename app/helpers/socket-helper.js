import SocketIOClient from 'socket.io-client'
import Config from "../app.settings"

let socket = null
let roomId = null

export function init(roomId) {
    this.roomId = roomId
    this.socket = SocketIOClient(Config.SOCKET_URL)
    this.socket.emit("killer_sales:create-room", roomId)

    this.socket.on('reconnect_attempt', () => {
        this.socket.emit("killer_sales:create-room", this.roomId)
    })
}

export function getSocket() {
    return this.socket
}