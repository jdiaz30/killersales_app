import moment from 'moment-timezone'
import localization from 'moment/locale/es'

export function dateTranscurred(date) {
    let days = moment().diff(moment(date).tz(moment.tz.guess()), 'days')
    let years = moment().diff(moment(date).tz(moment.tz.guess()), 'years')
    let months = moment().diff(moment(date).tz(moment.tz.guess()), 'months')

    //console.log("DateTranscurreds", days, months, years, moment().format("YYYY-MM-DD"), moment(date).format("YYYY-MM-DD"))
    let textDate = ""

    if (years > 0) {
        textDate = years + " " + ((years > 1) ? "años" : "año")
    }

    if (months >= 1) {
        textDate = months + " " + ((months > 1) ? "meses" : "mes")
    } else {
        if (days > 0 && days <= 31) {
            textDate = days + " " + ((days > 1) ? "dias" : "dia")
        } else {
            let hours = moment().diff(moment(date).tz(moment.tz.guess()), 'hour')
            let mins = moment().diff(moment(date).tz(moment.tz.guess()), 'minutes')

            if (hours == 0) {
                textDate = mins + " " + ((mins > 1) ? "minutos" : "minuto")
            } else {
                textDate = hours + " " + ((hours > 1) ? "horas" : "hora")
            }
        }
    }
    return textDate
}

export function calculateAge(dateNac) {
    let age = moment().diff(moment(dateNac).format("YYYY-MM-DD"), 'years', true);
    return parseInt(age)
}

export function dateNow() {
    console.log("dateNow")
}

export function utcToLocal(dateUtc, format) {
    let date = moment(dateUtc).locale("es", localization).tz(moment.tz.guess()).format(format)
    return date
}

//para extraer las fechas de los formatos utc
export function extractDateOnly(date) {
    let dateFormat = date.substr(0, 10)
    return dateFormat
}


