
import { Alert } from 'react-native'

export function parseParamsUrlGet(data) {
    return Object.entries(data).map(([key, val]) => `${key}=${val}`).join('&')
}

export function validateEmail(email) {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(email)
}

export function showMessageAlert(title, message, callback) {
    Alert.alert(
        title,
        message,
        [
            {
                text: 'OK', onPress: () => {
                    if (callback !== null) {
                        callback()
                    }
                }
            },
        ],
        { cancelable: false }
    )
}

export function showMessageConfimrAlert(title, message, titleConfirm, titleCancel, callbackSuccess, callbackCancel) {
    Alert.alert(
        title,
        message,
        [
            {
                text: titleConfirm, onPress: () => {
                    if (callbackSuccess !== null) {
                        callbackSuccess()
                    }
                }
            },
            {
                text: titleCancel, onPress: () => {
                    if (callbackCancel !== null) {
                        callbackCancel()
                    }
                }
            },
        ]
    )
}

export function convertParamsUrlByObject(object) {
    return Object.keys(object).map(key => key + '=' + object[key]).join('&')
}