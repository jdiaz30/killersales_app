import PushNotification from 'react-native-push-notification'
import { Platform } from 'react-native'

import * as sessionHelper from './session-helper'
import * as authService from '../services/auth-service'

export function init(user) {
    //PushNotification.setApplicationIconBadgeNumber(2)
    PushNotification.configure({

        // (optional) Called when Token is generated (iOS and Android)
        onRegister: function (token) {
            if (user == null) {
                existsToken(token.token)
            } else {
                registerToken(user, token.token)
            }
        },

        // (required) Called when a remote or local notification is opened or received
        onNotification: function (notification) {
            console.warn('NOTIFICATION:', notification)

            // process the notification

            // required on iOS only (see fetchCompletionHandler docs: https://facebook.github.io/react-native/docs/pushnotificationios.html)
            //notification.finish(PushNotificationIOS.FetchResult.NoData)
        },

        // ANDROID ONLY: GCM or FCM Sender ID (product_number) (optional - not required for local notifications, but is need to receive remote push notifications)
        senderID: "887760931733",

        // IOS ONLY (optional): default: all - Permissions to register.
        permissions: {
            alert: true,
            badge: true,
            sound: true
        },

        // Should the initial notification be popped automatically
        // default: true
        popInitialNotification: true,

        /**
          * (optional) default: true
          * - Specified if permissions (ios) and token (android and ios) will requested or not,
          * - if not, you must call PushNotificationsHandler.requestPermissions() later
          */
        requestPermissions: true,
    })
}

function existsToken(token) {
    sessionHelper.decodeToken().then((user) => {
        sessionHelper.getNotification().then((notification) => {
            if (!notification) {
                registerToken({ id: user.user_id }, token)
            }
        })
    })
}

function registerToken(user, token) {
    let dispositive = { so: Platform.OS, uuid: "", token: token, status: 1, userapp_id: user.id }
    authService.registerDispositive(user.id, dispositive).then((response) => {
        sessionHelper.saveNotification(token)
    }).catch((error) => {
        console.warn("error register", error.response.data)
    })
}