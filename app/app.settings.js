let Config = {

  "API_URL": "https://api.behlaaktours.xyz/api",
  "API_MEDIA_URL": "https://api.behlaaktours.xyz",
  "SOCKET_URL": "https://behlaaktours.xyz:3000",
  "URL_PAYMENTS": "https://behlaaktours.xyz:3030"

  /* "API_URL": "http://192.168.91.130:8000/api",
  "API_MEDIA_URL": "http://192.168.91.130:8000",
  "SOCKET_URL": "http://192.168.91.130:3000",
  "URL_PAYMENTS": "http://192.168.91.130:3030" */
}

export default Config