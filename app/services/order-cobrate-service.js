import Config from "../app.settings"
import { AsyncStorage } from 'react-native'

import * as GlobalHelper from "../helpers/global-helper"
import * as httpService from '../services/http-service'

let http = httpService.http

export function getAll(params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/orders-cobrate/?" + paramsParse)
}

export function update(orderCobrateId, data) {
    return http.put("/orders-cobrate/" + orderCobrateId + "/", data)
}

export function register(orderId, data) {
    return http.post("/orders/" + orderId + "/cobrate/", data)
}