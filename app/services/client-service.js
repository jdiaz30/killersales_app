import * as GlobalHelper from "../helpers/global-helper"
import * as httpService from '../services/http-service'

const http = httpService.http

export function verifyPhone(data) {
    return http.post("/clients/verify-phone/", data)
}