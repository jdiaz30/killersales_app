import Config from "../app.settings"
import { AsyncStorage } from 'react-native'

import * as GlobalHelper from "../helpers/global-helper"
import * as httpService from '../services/http-service'

let http = httpService.http

export function register(data) {
    return http.post("/orders/", data)
}

export function update(orderId, data) {
    return http.put("/orders/" + orderId + "/", data)
}

export function get(orderId) {
    return http.get("/orders/" + orderId + "/")
}

export function searchGeo(params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/orders/search-geo/?" + paramsParse)
}

export function search(params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/orders/?" + paramsParse)
}

export function listOrderCobrate(orderId) {
    return http.get("/orders/" + orderId + "/cobrate/")
}

export function getTokenPaypal() {
    return http.get("/orders/paypal/")
}