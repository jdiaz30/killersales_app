import Config from "../app.settings"
import { AsyncStorage } from 'react-native'

import * as GlobalHelper from "../helpers/global-helper"
import * as httpService from '../services/http-service'

const http = httpService.http

export function getAllCountries(params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/app/countries/?" + paramsParse)
}

export function getAllOperators(params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/app/operators/?" + paramsParse)
}

export function registerPromotor(params) {
    return http.post("/app/promotors/", params)
}

export function validatePhoneAccount(params) {
    return http.post("/app/verify-code/", params)
}

export function sendValidatePhoneAccount(params) {
    return http.post("/app/send-code/", params)
}