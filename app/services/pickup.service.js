import Config from "../app.settings"
import { AsyncStorage } from 'react-native'

import * as GlobalHelper from "../helpers/global-helper"
import * as httpService from '../services/http-service'

const http = httpService.http

export function get(id, params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/pickups/" + id + "/?" + paramsParse)
}

export function getAll(params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/pickups/?" + paramsParse)
}