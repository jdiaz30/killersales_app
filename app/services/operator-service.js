import Config from "../app.settings"
import { AsyncStorage } from 'react-native'

import * as GlobalHelper from "../helpers/global-helper"
import * as httpService from '../services/http-service'

const http = httpService.http

export function getAll(params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/operators/?" + paramsParse)
}
