import Config from "../app.settings"

import * as httpService from '../services/http-service'
import * as GlobalHelper from "../helpers/global-helper"

const http = httpService.http

export function login(data, params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.post("/auth/?" + paramsParse, data)
}

export function recoveryPassword(userApp) {
    return http.get("/users/reset-password/" + userApp + "/")
}

export function registerDispositive(userId, dispositive) {
    return http.post("/users/" + userId + "/dispositives/", dispositive)
}