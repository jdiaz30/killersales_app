
import * as GlobalHelper from "../helpers/global-helper"
import * as httpService from '../services/http-service'

const http = httpService.http

export function getAll(params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/clans/?" + paramsParse)
}

export function create(params) {
    return http.post("/clans/", params)
}

export function update(clanId, params) {
    return http.put("/clans/" + clanId + "/", params)
}

export function inviteClan(params) {
    return http.post("/notifications/", params)
}