import Config from "../app.settings"
import { AsyncStorage } from 'react-native'
import * as GlobalHelper from "../helpers/global-helper";


const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
}

const headersAuth = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': 'JWT'
}

export async function getAll(params) {
    const token = await AsyncStorage.getItem("userToken")
    headersAuth.Authorization = 'JWT ' + token

    let paramsParse = GlobalHelper.parseParamsUrlGet(params)

    return fetch(Config.API_URL + "/catalogs/?" + paramsParse, {
        headers: headersAuth,
    })
        .then(handleErrors)
        .then((results) => {
            return results.json()
        })
}

export async function getActivities(catalogId) {
    const token = await AsyncStorage.getItem("userToken")
    headersAuth.Authorization = 'JWT ' + token

    return fetch(Config.API_URL + "/catalogs/" + catalogId + "/activities/", {
        headers: headersAuth,
    })
        .then(handleErrors)
        .then((results) => {
            return results.json()
        })
}

function handleErrors(response) {
    if (!response.ok) {
        throw Error(response.statusText)
    }
    return response
}