import Config from "../app.settings"

import axios from 'axios'
import { AsyncStorage } from 'react-native'
import * as globalHelper from '../helpers/global-helper'

export const http = axios.create({
    baseURL: Config.API_URL,
    //timeout: 5000,
    headers: { 'Accept': 'application/json', 'Content-Type': 'application/json' }
})

let tokenAuth = ""

export function initAuth() {
    AsyncStorage.getItem("userToken").then((token) => {
        tokenAuth = token
        http.defaults.headers.common['Authorization'] = 'JWT ' + tokenAuth
        http.defaults.headers.post['Authorization'] = 'JWT ' + tokenAuth
        http.defaults.headers = { 'Accept': 'application/json', 'Content-Type': 'application/json', 'Authorization': 'JWT ' + tokenAuth }
    })

    //axios.defaults.headers.common['Authorization'] = 'JWT ' + token
}

export function getHttp() {
    return http
}

export function showError(error) {
    if (error.response) {
        globalHelper.showMessageAlert('Alerta!', error.response.data.user_msg, null)
    } else if (error.request) {
        globalHelper.showMessageAlert('Alerta!', "Hubo un error al procesar la solicitud", null)
    } else {
        globalHelper.showMessageAlert('Alerta!', "Hubo un error insperado intentelo luego", null)
    }
}

export function parseMessageError(error) {
    let message = ""

    if (error.response) {
        messsage = error.response.data.user_msg
    } else if (error.request) {
        message = "Hubo un error al procesar la solicitud"
    } else {
        message = "Hubo un error insperado intentelo luego"
    }

    return message
}