import Config from "../app.settings"
import { AsyncStorage } from 'react-native'

import * as GlobalHelper from "../helpers/global-helper"
import * as httpService from '../services/http-service'

const http = httpService.http

export function update(userId, params) {
    return http.put("/promotors/" + userId + "/", params)
}

export function get(promotorId, params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/promotors/" + promotorId + "/?" + paramsParse)
}

export function getAllDeals(promotorId, params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/promotors/" + promotorId + "/orders/?" + paramsParse)
}

export function search(params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/promotors/?" + paramsParse)
}

export function stats(promotorId) {
    return http.get("/promotors/" + promotorId + "/stats/")
}