
import * as GlobalHelper from "../helpers/global-helper"
import * as httpService from '../services/http-service'

const http = httpService.http

export function getAll(params) {
    let paramsParse = GlobalHelper.parseParamsUrlGet(params)
    return http.get("/hotels/?" + paramsParse)
}

export function register(data) {
    return http.post("/hotels/", data)
}
