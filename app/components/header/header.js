import React, { Component } from 'react';
import { TouchableOpacity, Platform, View } from 'react-native';
import { Button, FormLabel, FormInput, FormValidationMessage, Header as HeaderElement, Icon } from 'react-native-elements';
import { DrawerActions, withNavigation, HeaderTitle as HeaderTitleRN } from 'react-navigation';
import { Text } from '../index'

import NavigationService from '../navigation/navigation.service';

const HEADER_BGCOLOR = '#010044';
const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : 0;
const HEADER_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
const HEADER_TXTCOLOR = '#FFF'
const HEADER_FONTSIZE = 20
const HEADER_BODERCOLOR = '#010044'
const HEADER_BODERWIDTH = 1

export default class HeaderComponent extends Component {

    onMenu() {
        NavigationService.drawer()
    }

    render() {

        return (
            <HeaderElement statusBarProps={{ backgroundColor: "#000036", barStyle: "light-content" }}
                leftComponent={{ icon: 'menu', color: '#fff', onPress: this.onMenu, underlayColor: '#010044' }}
                centerComponent={{ text: this.props.title, style: { color: '#fff', fontFamily: "RobotoCondensed-Bold", fontSize: 18 } }}
                outerContainerStyles={{
                    backgroundColor: '#010044',
                    shadowColor: '#010044',
                    shadowOpacity: 0.1,
                    shadowRadius: 0.333333333,
                    shadowOffset: { "height": 0.3333333333 },
                    elevation: 2,
                    height: 56,
                    paddingHorizontal: 16,
                    borderBottomColor: HEADER_BODERCOLOR,
                    borderBottomWidth: HEADER_BODERWIDTH,
                }}
            />
        )
    }
}


export const Header = (props) => {
    let {
        leftComponent,
        centerComponent,
        rightComponent,
        backgroundColor,
        outerContainerStyles,
        innerContainerStyles,
        statusBarProps,
    } = props;
    var styleTransparent = {}
    if (props.transparent) {
        styleTransparent = {
            backgroundColor: 'transparent',
            borderBottomColor: 'transparent',
            borderBottomWidth: 0,
        }
    }


    return (
        <HeaderElement
            leftComponent={
                <HeaderLeft>
                    {props.menu ? (<HeaderButton icon="md-menu" color="white" typeicon="ionicon" onPress={() => { NavigationService.drawer() }} />) : (<View />)}
                    {leftComponent}
                </HeaderLeft>
            }
            centerComponent={<HeaderCenter>{centerComponent}</HeaderCenter>}
            rightComponent={<HeaderRight>{rightComponent}</HeaderRight>}
            backgroundColor={backgroundColor}
            outerContainerStyles={[styles.STYLES_HEADER, styleTransparent, outerContainerStyles]}
            innerContainerStyles={innerContainerStyles}
            statusBarProps={statusBarProps}
        />
    );
}

export const HeaderTitle = (props) => {
    var { size, fontFamily } = props;
    return <HeaderTitleRN style={[
        { fontFamily: "RobotoCondensed-Bold", color: HEADER_TXTCOLOR, fontSize: HEADER_FONTSIZE },
        size && { fontSize: normalize(size) },
        fontFamily && { fontFamily: fontFamily },
        props.style
    ]}>{props.children}</HeaderTitleRN>
}

export const HeaderButton = (props) => {
    if (props.onPress) {
        return (<TouchableOpacity style={[{ alignContent: 'center', justifyContent: 'center', height: '100%', paddingHorizontal: 16, position: 'relative' }, props.style]} onPress={props.onPress} >
            {props.icon ? <Icon name={props.icon} type={props.typeicon ? props.typeicon : 'ionicon'} size={props.size ? props.size : 24} color={props.color ? props.color : "#FFF"} /> : null}
            {props.badge ? <Text style={[{ color: '#fff', backgroundColor: '#FF2726', borderRadius: 8, paddingHorizontal: 4, paddingVertical: 2, minWidth: 18, height: 18, top: 8, right: 12, zIndex: 4, alignContent: 'center', justifyContent: 'center', textAlign: 'center', fontSize: 11, position: 'absolute' }, props.styleBadge]} >{props.badge}</Text> : null}
            {props.title ? <Text >{props.title}</Text> : null}
        </TouchableOpacity>)
    }

    return (<View style={[{ alignContent: 'center', justifyContent: 'center', height: '100%', paddingHorizontal: 16, position: 'relative' }, props.style]} >
        {props.icon ? <Icon name={props.icon} type={props.typeicon ? props.typeicon : 'ionicon'} size={props.size ? props.size : 24} color={props.color ? props.color : "#FFF"} /> : null}
        {props.badge ? <Text style={[{ color: '#fff', backgroundColor: '#FF2726', borderRadius: 8, paddingHorizontal: 4, paddingVertical: 2, minWidth: 18, height: 18, top: 8, right: 12, zIndex: 4, alignContent: 'center', justifyContent: 'center', textAlign: 'center', fontSize: 11, position: 'absolute' }, props.styleBadge]} >{props.badge}</Text> : null}
        {props.title ? <Text >{props.title}</Text> : null}
    </View>)
}

export const HeaderLeft = (props) => {
    return (
        <View style={{
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            height: '100%'
        }}>
            {props.children}
        </View>
    );
}

export const HeaderCenter = (props) => {
    return (
        <View style={[{
            flexDirection: 'row',
            justifyContent: 'center',
            alignItems: 'center',
            height: '100%'
        }, props.style]}>
            {props.children}
        </View>
    );
}

export const HeaderRight = (props) => {
    return (
        <View style={{
            flexDirection: 'row',
            justifyContent: 'flex-end',
            alignItems: 'center',
            height: '100%'
        }}>
            {props.children}
        </View>
    );
}


const styles = {
    STYLES_HEADER: {
        backgroundColor: HEADER_BGCOLOR,
        shadowColor: '#010044',
        shadowOpacity: 0.1,
        shadowRadius: 0.333333333,
        shadowOffset: { "height": 0.3333333333 },
        elevation: 2,
        //paddingHorizontal: 16,
        //elevation: 0,
        padding: 0,
        //paddingLeft: 0,
        //paddingRight: 0,
        height: HEADER_HEIGHT,
        marginTop: STATUSBAR_HEIGHT,
        borderBottomColor: HEADER_BODERCOLOR,
        borderBottomWidth: HEADER_BODERWIDTH,
    },
}