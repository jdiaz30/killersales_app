import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { NavigationActions } from 'react-navigation'
import { ScrollView, Text, View, StatusBar, AsyncStorage, Image, TouchableOpacity, TouchableHighlight, ImageBackground } from 'react-native'
import { Avatar, Icon, Badge } from 'react-native-elements'
import SVGImage from 'react-native-svg-image'
import { EventRegister } from 'react-native-event-listeners'


import styles from './side-menu.style'
import * as sessionHelper from '../../helpers/session-helper'
import * as socketHelper from '../../helpers/socket-helper'

import * as promotorService from '../../services/promotor-service'
import * as notifyService from '../../services/notification.service'

import images from '../images'
import Config from '../../app.settings';

export default class SideMenuComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            user: {},
            avatarProfile: '',
            screenSelect: '',
            notifyList: []
        }

        this.menuPromotor = [
            { title: "Perfil", label: "Profile", typeMenu: 'root', icon: 'user', isBadge: false },
            { title: "Catálogo", label: "Catalog", typeMenu: 'root', icon: 'book', isBadge: false },
            { title: "Clan", label: "Clan", typeMenu: 'root', icon: 'users', isBadge: false },
            { title: "Notificaciones", label: "Notification", typeMenu: 'root', icon: 'bell', isBadge: true },
            //{ title: "Batalla", label: "Battle", typeMenu: 'root', icon: 'bomb', isBadge: false },
            // { title: "Ranking", label: "Ranking", typeMenu: 'root', icon: 'star', isBadge: false },
            { title: "Mis Deals", label: "Deals", typeMenu: 'root', icon: 'shopping-bag', isBadge: false },
            // { title: "Pagos", label: "Payments", typeMenu: 'root', icon: 'credit-card', isBadge: false },
        ]

        this.menuCobrator = [
            { title: "Perfil", label: "Profile", typeMenu: 'root', icon: 'user', isBadge: false },
            { title: "Buscar promotors", label: "SearchPromotors", typeMenu: 'root', icon: 'street-view', isBadge: false },
            { title: "Notificaciones", label: "Notification", typeMenu: 'root', icon: 'bell', isBadge: true },
            { title: "Mis cobros", label: "Payments", typeMenu: 'root', icon: 'credit-card', isBadge: false },
        ]
    }

    componentDidMount() {
        this.getUserSession()
        this.listenEvents()
    }

    listenNotifications(user) {
        this.socket = socketHelper.getSocket()
        if (this.socket == undefined || this.socket == null) {
            socketHelper.init("killer-" + user.id)
        }
        this.socket.on('killer_sales:new-notification', (data) => {
            let notifications = [...this.state.notifyList]
            notifications.push({ id: 2, name: "dd" })

            this.setState({ notifyList: notifications })
        })
    }

    getNotifications(user) {
        notifyService.getAll({ to_promotor_id: user.id, status: 0, page_size: 30 })
            .then((response) => {
                let notifyList = response.data.data.results
                this.setState({ notifyList: notifyList })
            })
            .catch((error) => {

            })
    }

    listenEvents() {
        this.litenerAvatar = EventRegister.addEventListener('updateAvatar', (data) => {
            this.getProfile(this.state.user.email)
        })

        this.listenerNotify = EventRegister.addEventListener('seeNotify', (notifyId) => {
            this.seeNotify(notifyId)
        })
    }

    seeNotify(notifyId) {
        let notifyList = [... this.state.notifyList]
        let notifyListId = notifyList.map((notify) => notify.id)
        let indexNotify = notifyListId.indexOf(notifyId)

        if (indexNotify >= 0) {
            notifyList.splice(indexNotify, 1)
        }
        this.setState({ notifyList: notifyList })
    }

    getUserSession() {
        sessionHelper.getUser().then((user) => {
            if (user !== null) {
                this.setState({ user: user })
                this.setAvatarProfile(this.state.user)
                this.getNotifications(user)
            } else {
                sessionHelper.decodeToken().then((token) => {
                    this.getProfile(token.related_id)
                })
            }
        })
    }

    getProfile(userName) {
        promotorService.get(userName, {}).then((response) => {
            let data = response.data.data
            sessionHelper.saveUser(JSON.stringify(data))
            this.setState({ user: data })
            this.getNotifications(data)
            this.setAvatarProfile(data)
            this.listenNotifications(data)
        })
    }

    setAvatarProfile(user) {
        const avatarProfile = (user.avatar !== null) ? Config.API_MEDIA_URL + user.avatar.url : this.state.avatarProfile
        this.setState({ avatarProfile: avatarProfile })
    }

    clearSession = async () => {
        this.props.navigation.navigate("Auth")
        await AsyncStorage.clear()
    }

    navigateToScreen = (route, option) => () => {
        this.setState({ screenSelect: route });
        const navigateActionPush = NavigationActions.navigate({
            routeName: route,
            params: { userSession: this.state.user }
        })

        this.props.navigation.dispatch(navigateActionPush)
    }

    styleItem = (screen) => {
        if (this.state.screenSelect === screen) {
            return { backgroundColor: '#00add2' }
            //return { backgroundColor: 'rgba(0, 45, 107, 0.89)' }
        }
        return {}
    }

    loadMenu() {
        let menu = (this.state.user.type_promotor == "promotor") ? [...this.menuPromotor] : [...this.menuCobrator]
        return menu.map((menu) => {
            return (
                <TouchableHighlight onPress={this.navigateToScreen(menu.label, menu.typeMenu)} key={menu.label}>
                    <View style={[styles.navSectionStyle, this.styleItem(menu.label)]}>
                        <View style={{ padding: 5 }}><Icon name={menu.icon} color='#ffffff' type='font-awesome' /></View>
                        <View style={{ flexDirection: 'row' }}><Text style={styles.sectionHeadingStyle}>{menu.title}</Text>
                            {(menu.isBadge && this.state.notifyList.length > 0) && <Badge
                                value={this.state.notifyList.length}
                                textStyle={{ color: 'orange' }}
                                containerStyle={{ marginTop: 6 }}
                            />
                            }
                        </View>
                    </View>
                </TouchableHighlight>
            )
        })
    }

    render() {
        return (

            <ImageBackground source={images.Menu} style={{ width: '100%', height: '100%', flex: 1, justifyContent: 'center' }} resizeMode='cover'>

                <View style={styles.container}>
                    <StatusBar
                        backgroundColor="#282828"
                        barStyle="light-content"
                    />

                    <ScrollView>
                        <View style={{
                            alignItems: 'center',
                            paddingTop: 15,
                            paddingBottom: 15
                        }}>
                            <View style={{ flex: 1 }}>
                                <TouchableHighlight onPress={this.navigateToScreen('Profile', 'root')}>
                                    <SVGImage
                                        style={{ width: 140, height: 140, backgroundColor: "transparent", flex: 1 }}
                                        source={{ uri: this.state.avatarProfile }}
                                    />
                                </TouchableHighlight>
                            </View>

                            <Text style={{ color: "#fff", fontFamily: "RobotoCondensed-Bold", fontSize: 18 }}> {this.state.user.name}</Text>

                            <Image style={{ height: 20, width: 20, marginTop: 5 }} source={images.Coin} />
                            <Text style={{ color: "#fff", fontFamily: "RobotoCondensed-Bold", fontSize: 12 }}> {this.state.user.kcoins + " KCOINS"}</Text>

                        </View>
                        {
                            this.loadMenu()
                        }

                        <TouchableHighlight onPress={this.clearSession.bind(this)}>
                            <View style={[styles.navSectionStyle]}>
                                <View style={{ padding: 5 }}><Icon name='close' color='#ffffff' /></View>
                                <View><Text style={styles.sectionHeadingStyle} onPress={this.clearSession.bind(this)}> Salir</Text></View>
                            </View>
                        </TouchableHighlight>

                    </ScrollView>
                </View>
            </ImageBackground>
        )
    }

}

SideMenuComponent.propTypes = {
    navigation: PropTypes.object
}