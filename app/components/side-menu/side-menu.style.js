export default {
    container: {
        flex: 1,
        //backgroundColor: '#3d3f4b'
    },
    navItemStyle: {
        padding: 10
    },
    navSectionSelectStyle: {
        backgroundColor: '#00add2',
        flex: 1,
        flexDirection: 'row',
        //flexWrap: 'wrap'
    },
    navSectionStyle: {
        flex: 1,
        flexDirection: 'row',
        //flexWrap: 'wrap'
    },
    sectionHeadingStyle: {
        paddingVertical: 8,
        paddingHorizontal: 5,
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 16,
        color: '#ffffff'
    },
    footerContainer: {
        padding: 20,
        backgroundColor: 'lightgrey'
    },
    avatarContent: {
        borderColor: "white",
        borderWidth: 3,
        borderStyle: "dashed",
        borderRadius: 50,
        width: 90,
        height: 90,
        alignItems: "center",
        justifyContent: 'center'
    }
};