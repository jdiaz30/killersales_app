import React from 'react';
import { StyleSheet, View, Image } from 'react-native';
import SVGImage from 'react-native-svg-image'

const AvatarCircle = (props) => {

    const { size, selected, source, type } = props;

    var imageCircle = {};
    var styleContainer = {};
    var imageAvatar = {};
    if (size) {
        const sizeAvatar = size * (72 / 72);
        styleContainer.height = size;
        styleContainer.width = size;
        imageCircle.height = size;
        imageCircle.width = size;
        imageAvatar.height = sizeAvatar;
        imageAvatar.width = sizeAvatar;
        imageAvatar.borderRadius = sizeAvatar / 2;
    }

    return (
        <View style={[styles.container, styleContainer, props.containerStyle]}>
            {type !== 'svg' && <Image source={source ? source : imagesAvatar.Perfil} style={[styles.imageAvatar, imageAvatar]} />}
            {type === 'svg' && <SVGImage source={source} style={[styles.imageAvatar, imageAvatar]} />}
        </View>
    );

}
export default AvatarCircle;

const styles = StyleSheet.create({
    container: {
        height: 72,
        width: 72,
        margin: 0,
        padding: 0,
        backgroundColor: 'transparent',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 36
        //position: 'relative'
    },
    imageAvatar: {
        height: 62,
        width: 62,
        //borderRadius: 31,
        //position: 'absolute',
    }

});