import PropTypes from 'prop-types';
import React from 'react';
import { Text, StyleSheet, Platform } from 'react-native';
import { normalize } from 'react-native-elements';
export const HEAD_FONTSIZE = Platform.OS === 'ios' ? 20 : 20;

const fonts = {
    ios: {},
    android: {
        regular: {
            fontFamily: 'RobotoCondensed-Regular',
        },
        light: {
            fontFamily: 'RobotoCondensed-Light',
        },
        condensed: {
            fontFamily: 'RobotoCondensed-Condensed',
        },
        black: {
            fontFamily: 'RobotoCondensed-Black',
        },
        thin: {
            fontFamily: 'RobotoCondensed-Thin',
        },
        medium: {
            fontFamily: 'RobotoCondensed-Medium',
        },
        bold: {
            fontFamily: 'RobotoCondensed-Bold',
        },
        boldItalic: {
            fontFamily: 'RobotoCondensed-BoldItalic',
        },
        roman: {
            fontFamily: 'RobotoCondensed-Roman',
        },
    },
};


const styles = StyleSheet.create({
    text: {
        ...Platform.select({
            android: {
                ...fonts.android.regular,
            },
        }),
    },
    bold: {
        ...Platform.select({
            android: {
                ...fonts.android.bold,
            },
        }),
    },
    boldItalic: {
        ...Platform.select({
            android: {
                ...fonts.android.boldItalic,
            },
        }),
    },
    black: {
        ...Platform.select({
            android: {
                ...fonts.android.black,
            },
        }),
    },
    light: {
        ...Platform.select({
            android: {
                ...fonts.android.light,
            },
        }),
    },
    condensed: {
        ...Platform.select({
            android: {
                ...fonts.android.condensed,
            },
        }),
    },
    thin: {
        ...Platform.select({
            android: {
                ...fonts.android.thin,
            },
        }),
    },
    medium: {
        ...Platform.select({
            android: {
                ...fonts.android.medium,
            },
        }),
    },
    roman: {
        ...Platform.select({
            android: {
                ...fonts.android.roman,
            },
        }),
    },
});

const TextElement = props => {
    const { style, size, color, children, h1, h2, h3, h4, header, bold, boldItalic, black, light, condensed, thin, medium, roman, fontFamily, ...rest } = props;

    var stylesLocal = [
        { color: '#262627' },
        styles.text,
        h1 && { fontSize: normalize(40), color: '#4B4B4B' },
        h2 && { fontSize: normalize(34), color: '#4B4B4B' },
        h3 && { fontSize: normalize(28), color: '#4B4B4B' },
        h4 && { fontSize: normalize(22), color: '#4B4B4B' },
        header && { fontSize: normalize(HEAD_FONTSIZE), color: '#4B4B4B' },
        h1 && styles.bold,
        h2 && styles.bold,
        h3 && styles.bold,
        h4 && styles.bold,
        header && styles.bold,
        bold && styles.bold,
        boldItalic && styles.boldItalic,
        black && styles.black,
        light && styles.light,
        condensed && styles.condensed,
        thin && styles.thin,
        medium && styles.medium,
        roman && styles.roman,
        fontFamily && { fontFamily },
        !size && { fontSize: normalize(14) },
        size && { fontSize: size },
        color && { color: color },
        style && style,
    ];

    return (
        <Text
            style={stylesLocal}
            {...rest}
        >
            {children}
        </Text>
    );
};


TextElement.propTypes = {
    style: PropTypes.any,
    h1: PropTypes.bool,
    h2: PropTypes.bool,
    h3: PropTypes.bool,
    h4: PropTypes.bool,
    fontFamily: PropTypes.string,
    children: PropTypes.any,
};

export default TextElement;
