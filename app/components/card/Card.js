
import PropTypes from 'prop-types';
import React from 'react';
import {
    View,
    StyleSheet,
    Platform,
    Image,
    Text as NativeText,
} from 'react-native';
import { Card as CardElement, Header as HeaderElement } from 'react-native-elements';
const HEADER_HEIGHT = Platform.OS === 'ios' ? 44 : 56;
import { HeaderLeft, HeaderCenter, HeaderRight } from '../header/header';

export const Card = props => {
    const {
        children,
        flexDirection,
        containerStyle,
        wrapperStyle,
        imageWrapperStyle,
        title,
        titleStyle,
        titleNumberOfLines,
        featuredTitle,
        featuredTitleStyle,
        featuredSubtitle,
        featuredSubtitleStyle,
        dividerStyle,
        image,
        imageStyle,
        fontFamily,
        imageProps,
        ...attributes
    } = props;

    return (
        <CardElement
            flexDirection={flexDirection}
            containerStyle={[{ backgroundColor: '#FFF', borderRadius: 8, borderWidth: 1, padding: 0, paddingTop: 0 }, containerStyle]}
            dividerStyle={{ backgroundColor: '#CCC' }}

            title={title}
            titleStyle={[title && { fontFamily: "RobotoCondensed-Bold", color: "#06003a", fontSize: 17, paddingTop: 15 }, titleStyle]}
            image={image}
            imageStyle={[{ borderRadius: 8 }, imageStyle]}
            imageWrapperStyle={[{ borderRadius: 0 }, imageWrapperStyle]}
        >
            {children}
        </CardElement>
    )
}


export const CardHeader = (props) => {
    let {
        leftComponent,
        centerComponent,
        rightComponent,
        backgroundColor,
        outerContainerStyles,
        innerContainerStyles,
        statusBarProps,
    } = props;
    return (
        <HeaderElement
            leftComponent={<HeaderLeft>{leftComponent}</HeaderLeft>}
            centerComponent={<HeaderCenter>{centerComponent}</HeaderCenter>}
            rightComponent={<HeaderRight>{rightComponent}</HeaderRight>}
            backgroundColor={backgroundColor}
            outerContainerStyles={[{ height: 'auto', minHeight: HEADER_HEIGHT, backgroundColor: 'transparent', borderBottomColor: '#e1e8ee', padding: 0 }, outerContainerStyles]}
            innerContainerStyles={[innerContainerStyles]}
            statusBarProps={statusBarProps}
        />
    );
}

