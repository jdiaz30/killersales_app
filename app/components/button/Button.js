import React from 'react';
import {
    View,
    StyleSheet,
    Platform,
} from 'react-native';
import { Button as ButtonElement } from 'react-native-elements';

export const Button = (props) => {

    const { square, round, nomargin, ...rest } = props;

    var borderRadius = 6
    if (square) {
        borderRadius = 0
    } else if (round) {
        borderRadius = 24
    }

    const marginBottom = nomargin ? 0 : 16
    const buttonStyle = props.transparent ? {} : styles.button
    return (
        <ButtonElement
            transparent={props.transparent}
            title={props.title}
            icon={props.icon}
            iconRight={props.iconRight}
            {...rest}
            buttonStyle={[buttonStyle, { borderRadius: borderRadius }, props.buttonStyle]}
            containerViewStyle={[{ marginLeft: 0, marginRight: 0, marginBottom: marginBottom }, props.containerViewStyle]}
            textStyle={[{ fontFamily: 'RobotoCondensed-Bold', color: "white", fontSize: 16 }, props.textStyle]} onPress={props.onPress} />
    )
}

const styles = StyleSheet.create({
    button: {
        height: 48,
        backgroundColor: "#081257",
        //borderRadius: 7,
        shadowOpacity: 0.75,
        shadowRadius: 5,
        shadowColor: '#081257',
        shadowOffset: { height: 5, width: 5 },
        elevation: 4,
    },
});
