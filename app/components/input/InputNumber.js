import React, { Component } from 'react'
import { View, Image, TouchableOpacity } from 'react-native'
import { Text } from '../index'
import { Icon } from 'react-native-elements'

export class InputNumber extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: this.parseInput(this.props.value)
        };
    }

    parseInput = (value) => {
        if (typeof value === 'number') {
            return value;
        }
        return 0;
    }

    increment = () => {
        const value = this.state.value + 1;
        this.onChangeQuantity(value);
        this.setState({
            value: value
        });
    }

    decrement = () => {
        if (this.state.value > 0) {
            const value = this.state.value - 1;
            this.onChangeQuantity(value);
            this.setState({
                value: value
            });
        }
    }

    onChangeQuantity = (value) => {
        if (this.props.onChange) {
            this.props.onChange(value);
        }
    }

    render() {
        const { style, styleText } = this.props;
        const CONTROL_WIDTH = 24;

        return (
            <View style={[{ flexDirection: 'row', height: 44, borderColor: '#081257', borderWidth: 1, borderRadius: 4 }, style]}>
                <View style={[{ width: '100%', height: 44, paddingLeft: CONTROL_WIDTH, paddingRight: CONTROL_WIDTH, justifyContent: 'center' }]}>
                    <Text style={[{ textAlign: 'center' }, styleText]} >{this.props.value}</Text>
                </View>
                <View style={{ width: CONTROL_WIDTH, height: 44, borderRightWidth: 1, borderRightColor: '#081257', position: 'absolute', top: 0, left: 0, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={this.decrement}
                        style={{ flex: 1, width: CONTROL_WIDTH, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}
                    >
                        <Icon size={12} name={'md-remove'} type='ionicon' />
                    </TouchableOpacity>
                </View>
                <View style={{ width: CONTROL_WIDTH, height: 44, borderLeftWidth: 1, borderLeftColor: '#081257', position: 'absolute', top: 0, right: 0, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}>
                    <TouchableOpacity
                        onPress={this.increment}
                        style={{ flex: 1, width: CONTROL_WIDTH, justifyContent: 'center', alignContent: 'center', alignItems: 'center' }}
                    >
                        <Icon size={12} name={'md-add'} type='ionicon' />
                    </TouchableOpacity>
                </View>
            </View>
        );

    }

}