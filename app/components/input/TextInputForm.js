import React from 'react';
import { StyleSheet, TextInput, View, Text } from 'react-native';
import { SearchBar as SearchBarElement } from 'react-native-elements'

/** backgroundColor: 'rgba(185, 185, 185, 0.6)' */
export const TextInputForm = (props) => {
    const { InputPropss } = props
    return (

        <TextInput
            style={[{ height: 44, paddingHorizontal: 16, color: '#4B4B4B', fontFamily: 'RobotoCondensed-Regular', backgroundColor: '#FFF', marginBottom: 16, borderWidth: 1, borderRadius: 4, borderColor: '#081257' }, props.style]}
            value={props.value}
            InputProps={props.InputProps}
            secureTextEntry={props.secureTextEntry}
            keyboardType={props.keyboardType}
            textContentType={props.textContentType}
            placeholderTextColor={props.placeholderTextColor ? props.placeholderTextColor : '#CCC'}
            underlineColorAndroid={props.underlineColorAndroid ? props.underlineColorAndroid : "rgba(0,0,0,0)"}
            fontFamily={props.fontFamily ? props.fontFamily : "RobotoCondensed-Regular"} //HelveticaLT66MediumItalic
            placeholder={props.placeholder}
            onChangeText={props.onChangeText}
            selectionColor={'#081257'}
            editable={props.editable}
            onEndEditing={props.onEndEditing}
            returnKeyType={'next'}
            fontSize={props.fontSize}
        />

    )
}

export const LabelInputForm = props => {
    return (
        <Text style={{ color: '#081257', fontFamily: 'RobotoCondensed-Bold', marginBottom: 4 }}>{props.children}</Text>
    )
}

export const SearchBar = props => {

    const { ...rest } = props
    return (
        <SearchBarElement
            icon={{ type: 'font-awesome', name: 'search', style: { fontSize: 16, width: 24, marginTop: -2, marginLeft: 4 } }}
            ref={props.ref}
            onChangeText={props.onChangeText}
            onClearText={props.onClearText}
            value={props.value}
            placeholder={props.placeholder}
            noIcon={props.noIcon}
            /*clearIcon={{
                color: '#86939e',
                name: 'close',
                style: {
                    fontSize: 20,
                    height: 24,
                    width: 24,
                    //backgroundColor: '#ccc',
                    borderRadius: 12,
                    marginTop: -4
                },
            }}*/
            containerStyle={{
                backgroundColor: '#081257',
                borderTopWidth: 0
            }}
            inputStyle={{
                //backgroundColor: 'rgba(0, 0, 0, 0.3)',
                backgroundColor: '#FFF',
                fontSize: 16,
                fontFamily: 'RobotoCondensed-Bold',
                paddingLeft: 30,
                //paddingRight: 36
            }}
            selectionColor={'#081257'}

            {...rest}
        />
    )
}
