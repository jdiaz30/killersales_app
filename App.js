/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

/* import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, ActivityIndicator, StatusBar } from 'react-native';
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import { createStackNavigator, createSwitchNavigator } from 'react-navigation';

import Login from './app/pages/login';
import Register from './app/pages/register'
import Home from './app/pages/home'

import { YellowBox } from 'react-native'

 */

import React, { Component } from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  Button,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native'
import SplashScreen from 'react-native-splash-screen';
import { createStackNavigator, createSwitchNavigator, createDrawerNavigator, DrawerNavigator } from 'react-navigation'

import Login from './app/pages/login'
import Register from './app/pages/register/register'
import ConfirmAcount from './app/pages/confirm-acount'
import Welcome from './app/pages/welcome/welcome'
import Home from './app/pages/home'
import Catalog from './app/pages/catalog/catalog'
import ActivityCatalog from './app/pages/catalog/activityCatalog'
import Profile from './app/pages/profile/profile'
import ProfileUpdate from './app/pages/profile/profile-update'
import Clan from './app/pages/clan/clan'
import ProfileAvatar from './app/pages/profile/profile-avatar'
import Battle from './app/pages/battle/battle'

import SideMenuComponent from './app/components/side-menu/side-menu'

import { YellowBox } from 'react-native'
import NavigationService from './app/components/navigation/navigation.service'
import ClanInvite from './app/pages/clan/clan-invite'
import Notifications from './app/pages/notification/notification'
import OrderService from './app/pages/service/service'
import ServiceHotel from './app/pages/service/service-hotel'
import ServiceHotelAdd from './app/pages/service/service-hotel-add'
import Deals from './app/pages/deals/deals'
import Ranking from './app/pages/ranking/ranking'
import ServiceDetail from './app/pages/service/service-detail'
import Payments from './app/pages/payments/payments'

import * as httpService from './app/services/http-service'
import * as sessionHelper from './app/helpers/session-helper'
import * as socketHelper from './app/helpers/socket-helper'
import { SelectPickupModal } from './app/modals'

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated'])

class AuthLoadingScreen extends React.Component {

  constructor(props) {
    super(props)
    this._bootstrapAsync()
  }


  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('userToken')
    if (userToken) {
      httpService.initAuth()
    }

    const welcomeScreenCheck = sessionHelper.getWelcome().then((data) => {
      const initPage = data ? 'Catalog' : 'Welcome'
      this.props.navigation.navigate(userToken ? initPage : 'Auth')
    })

  }

  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
})


const CONFIG_VAVIGATION = {
  navigationOptions: {
    headerStyle: {
      backgroundColor: '#010044',
    },
    headerTintColor: '#fff',
  },
};

const AppStackWelcome = createStackNavigator(
  {
    Welcome: Welcome
  },
  CONFIG_VAVIGATION
)

const AppStack = createStackNavigator(
  {
    Home: Home,
  },
  CONFIG_VAVIGATION
)

const AppStackProfile = createStackNavigator(
  {
    Profile: Profile,
    ProfileUpdate: ProfileUpdate,
    ProfileAvatar: ProfileAvatar
  },
  CONFIG_VAVIGATION
)

const AppStackCatalogo = createStackNavigator(
  {
    Catalog: Catalog,
    ActivityCatalog: ActivityCatalog,
    OrderService: OrderService,
    ServiceHotel: ServiceHotel,
    ServiceHotelAdd: ServiceHotelAdd,
    PickUp: SelectPickupModal
  },
  CONFIG_VAVIGATION
)

const AppStackDeals = createStackNavigator(
  {
    Deals: Deals,
    DealsOrderService: OrderService,
    DealsServiceHotel: ServiceHotel,
    DealsServiceHotelAdd: ServiceHotelAdd,
    DealsPickUp: SelectPickupModal
  },
  CONFIG_VAVIGATION
)

const AppStackClan = createStackNavigator(
  {
    Clan: Clan,
    ClanInvite: ClanInvite,
  },
  CONFIG_VAVIGATION
)

const AppStackNotification = createStackNavigator(
  {
    Notification: Notifications,
    ServiceDetail: ServiceDetail
  },
  CONFIG_VAVIGATION
)

const AppStackBattle = createStackNavigator(
  {
    Battle: Battle
  },
  CONFIG_VAVIGATION
)


const AppPayments = createStackNavigator(
  {
    Payments: Payments,
  },
  CONFIG_VAVIGATION
)


const AppRankingBattle = createStackNavigator(
  {
    Ranking: Ranking
  },
  CONFIG_VAVIGATION
)

/* const AppServiceDetail = createStackNavigator(
  {
    ServiceDetail: ServiceDetail
  },
  CONFIG_VAVIGATION
) */



const AppStackMain = createDrawerNavigator(
  {
    AppStack: AppStack,
    AppStackWelcome: AppStackWelcome,
    AppStackProfile: AppStackProfile,
    AppStackCatalogo: AppStackCatalogo,
    AppStackClan: AppStackClan,
    AppStackNotification: AppStackNotification,
    AppStackBattle: AppStackBattle,
    AppStackDeals: AppStackDeals,
    AppRankingBattle: AppRankingBattle,
    //AppSearchPromotors: AppSearchPromotors,
    AppPayments: AppPayments
  },
  {
    drawerPosition: 'left',
    contentComponent: SideMenuComponent
  }
)

const AuthStack = createStackNavigator(
  { SignIn: Login, Register: Register, ConfirmAcount: ConfirmAcount },
  {
    initialRouteName: 'SignIn',
    navigationOptions: {
      headerStyle: {
        backgroundColor: '#282828',
      },
      headerTintColor: '#fff',
      /*   headerTitleStyle: {
          fontWeight: 'bold',
        }, */
    },
  }
)

const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStackMain,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
);

//type Props = {}

export default class App extends Component {

  componentDidMount() {
    setTimeout(() => {
      SplashScreen.hide()
      sessionHelper.getUser().then((user) => {
        if (user !== null) {
          this.socketInit(user)
        }
      })
    }, 30)
  }

  socketInit(user) {
    socketHelper.init("killer-" + user.id)
  }

  render() {
    return <AppNavigator ref={navigatorRef => {
      NavigationService.setTopLevelNavigator(navigatorRef)
    }} />
  }
}